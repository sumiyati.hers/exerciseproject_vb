﻿#Region "Custom Namespace Imports"

Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Data.Configuration

#End Region

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.IO
Imports System.Collections
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

#End Region

Public Class ReportingService

#Region "Constructor"
    Public Sub New()
    End Sub
#End Region

#Region "Private Method"

    Private Function GetConnectionString() As String
        Try
            Return (DatabaseFactory.CreateDatabase("DBReport").CreateConnection().ConnectionString)
        Catch generatedExceptionVariable0 As Exception
            Throw New Exception("Error has occured when trying to get Data Configuration (DBReport)!")
        End Try
    End Function

    Private Sub SetConnection(ByVal report As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim connection As ConnectionInfo = New ConnectionInfo()
        Dim dataConfig As String = GetConnectionString()

        connection.DatabaseName = (dataConfig.Split(";")(0)).Split("=")(1)
        connection.ServerName = (dataConfig.Split(";")(1)).Split("=")(1)
        connection.IntegratedSecurity = (dataConfig.Split(";")(2)).Split("=")(1)

        If connection.IntegratedSecurity = False Then
            connection.Password = (dataConfig.Split(";")(3)).Split("=")(1)
            connection.UserID = (dataConfig.Split(";")(4)).Split("=")(1)
        End If

        connection.AllowCustomConnection = True

        For Each table As CrystalDecisions.CrystalReports.Engine.Table In report.Database.Tables
            Dim logOnInfo As TableLogOnInfo = table.LogOnInfo
            logOnInfo.ConnectionInfo = connection
            table.ApplyLogOnInfo(logOnInfo)
            table.Location = connection.DatabaseName & ".dbo." & table.Location.Substring(table.Location.LastIndexOf(".") + 1)
        Next

    End Sub

    'first index for query parameter
    '2nd index for report parameter
    '3rd index for stored procedure
    Private Sub SetCrystalParameter( _
        ByVal report As ReportDocument, _
        ByVal ParaType As ParameterType, _
        ByVal ParamArray parameters As Object())
        If Not IsNothing(parameters) Then
            Select Case ParaType
                Case ParameterType.Index
                    SetParameterByIndex(report, parameters)
                Case ParameterType.Name
                    SetParameterByName(report, parameters)
            End Select
        End If
    End Sub

    Private Sub SetParameterByIndex(ByRef report As ReportDocument, ByVal ParamArray parameters As Object())
        Dim crystalValues As ParameterValues '= New ParameterValues
        Dim crystalValue As ParameterDiscreteValue

        Dim parameterIndex As Integer = 0
        Dim parameterValue As String
        Dim enumerator As IEnumerator = parameters.GetEnumerator

        While enumerator.MoveNext
            parameterValue = enumerator.Current.ToString

            crystalValue = New ParameterDiscreteValue
            crystalValue.Value = parameterValue

            'Bug Fix: Error Passing Parameter to Crystal
            crystalValues = report.DataDefinition.ParameterFields(parameterIndex).CurrentValues()
            'End Bug Fix
            crystalValues.Add(crystalValue)

            report.DataDefinition.ParameterFields(parameterIndex).ApplyCurrentValues(crystalValues)
            parameterIndex = parameterIndex + 1
        End While
    End Sub

    Private Sub SetParameterByName(ByRef report As ReportDocument, ByVal ParamArray parameters As Object())
        Try
            Dim crystalValues As ParameterValues '= New ParameterValues
            Dim crystalValue As ParameterDiscreteValue

            Dim parameter() As Object
            Dim enumerator As IEnumerator = parameters.GetEnumerator

            While enumerator.MoveNext
                Try
                    parameter = CType(enumerator.Current, Object())
                    crystalValue = New ParameterDiscreteValue
                    crystalValue.Value = parameter(1)

                    'Bug Fix: Error Passing Parameter to Crystal
                    crystalValues = report.DataDefinition.ParameterFields(parameter(0)).CurrentValues()
                    'End Bug Fix
                    crystalValues.Add(crystalValue)
                    report.DataDefinition.ParameterFields(parameter(0).ToString()).ApplyCurrentValues(crystalValues)
                Catch ex As Exception
                    'nothing
                End Try

            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetExportContentType(ByVal contentType As String) As ExportFormatType
        Select Case contentType.ToUpper()
            Case "PDF"
                Return ExportFormatType.PortableDocFormat
            Case "EXCEL"
                Return ExportFormatType.Excel
            Case "WORD"
                Return ExportFormatType.WordForWindows
            Case Else
                Throw New Exception("Undefined Export Format Type")
        End Select
    End Function

#End Region

#Region "Public Method"

    Public Function CreateReportDocument(ByVal reportName As String, ByVal ParaType As ParameterType, ByVal InitSubReportParam As Boolean, ByVal ParamArray parameters As Object()) As ReportDocument
        Dim report As ReportDocument = (ReportFactory.GetInstance()).GetReportClass(reportName)

        If Not (report Is Nothing) Then
            Me.SetConnection(report)

            If Not (parameters Is Nothing) Then
                Me.SetCrystalParameter(report, ParaType, parameters)
            End If
        End If

        'OKTA 14 OKT 2006
        'SUB REPORT HANDLE
        Dim crSections As Sections
        Dim crReportObjects As ReportObjects
        Dim subRepDoc As ReportDocument = New ReportDocument
        Dim crSubreportObject As SubreportObject

        crSections = report.ReportDefinition.Sections
        For Each crSection As Section In crSections
            crReportObjects = crSection.ReportObjects
            For Each crReportObject As ReportObject In crReportObjects
                If crReportObject.Kind = ReportObjectKind.SubreportObject Then
                    crSubreportObject = CType(crReportObject, SubreportObject)
                    subRepDoc = crSubreportObject.OpenSubreport(crSubreportObject.SubreportName)
                    Me.SetConnection(subRepDoc)

                    'Sebaiknya Gunakan Sub Report Link
                    If InitSubReportParam Then
                        If Not (parameters Is Nothing) Then
                            Me.SetCrystalParameter(subRepDoc, ParaType, parameters)
                        End If
                    End If

                End If
            Next
        Next
        Return report
    End Function

    Public Function ExportReport(ByVal reportName As String, ByVal contentType As String, ByVal parameterType As ReportService.ParameterType, ByVal InitSubReportParam As Boolean, ByVal ParamArray parameters As Object()) As Byte()
        Dim report As ReportDocument = CreateReportDocument(reportName, parameterType, InitSubReportParam, parameters)
        Dim oStream As MemoryStream = _
            CType(report.ExportToStream(GetExportContentType(contentType)), MemoryStream)
        Try
            Return oStream.ToArray()
        Finally
            CType(oStream, IDisposable).Dispose()
        End Try
    End Function

#End Region

End Class
