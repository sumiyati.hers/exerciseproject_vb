﻿Imports System
Imports System.Xml
Imports System.Collections

Public Class ReportFactory

#Region "Private Properties"

    Private Shared _instance As ReportFactory
    Private _reportAssembly As String
    Private _assembly As System.Reflection.Assembly
    Private _xmlReader As XmlTextReader
    Private _reportMapPath As String = AppDomain.CurrentDomain.BaseDirectory.ToString + "ReportMap.xml"
    Private _reportNameAttribute As String = "ReportName"
    Private _reportClassAttribute As String = "ReportClass"
    Private _mapTable As Hashtable

#End Region

#Region "Constructor"

    Private Sub New()
        Me._reportAssembly = System.Reflection.Assembly.GetAssembly(Me.GetType).GetName.ToString
        Me._assembly = System.Reflection.Assembly.Load(Me._reportAssembly)
    End Sub

#End Region

#Region "Private Method"

    Private Sub LoadMap()
        Try
            _xmlReader = New XmlTextReader(Me._reportMapPath)
        Catch ex As XmlException
            Throw ex
        End Try
    End Sub

    Private Sub PrepareMap()
        Me._mapTable = New Hashtable
        Dim reportName As String
        Dim reportClass As String
        Try
            While Me._xmlReader.Read
                If Me._xmlReader.NodeType = XmlNodeType.Element Then
                    If Me._xmlReader.AttributeCount > 0 Then
                        reportName = Me._xmlReader.GetAttribute(_reportNameAttribute)
                        reportClass = Me._xmlReader.GetAttribute(_reportClassAttribute)
                        Me._mapTable.Add(reportName, reportClass)
                    End If
                End If
            End While
        Catch ex As Exception
            Throw ex
        Finally
            Me._xmlReader.Close()
        End Try
    End Sub

    Private Function ReportClassName(ByVal reportName As String) As String
        Return "IMFI.Template.ReportService." + reportName.Trim
    End Function

#End Region

#Region "Public Method"

    Public Function GetReportList() As ArrayList
        Return CType(Me._mapTable.Keys, ArrayList)
    End Function

    Public Function GetReportClass(ByVal reportName As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim oReportType As Type = Me._assembly.GetType(ReportClassName(reportName))
        Return CType(Activator.CreateInstance(oReportType), CrystalDecisions.CrystalReports.Engine.ReportClass)
    End Function

    Public Shared Function GetInstance() As ReportFactory
        If System.Web.HttpContext.Current.Session("Instance") Is Nothing Then
            System.Web.HttpContext.Current.Session("Instance") = New ReportFactory
        End If
        Return System.Web.HttpContext.Current.Session("Instance")
    End Function

#End Region

End Class
