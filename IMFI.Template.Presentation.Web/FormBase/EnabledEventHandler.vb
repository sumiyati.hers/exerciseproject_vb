﻿Public Delegate Sub EnabledEventHandler(ByVal Sender As Object, ByVal e As EnabledEventArgs)

Public Class EnabledEventArgs
    Inherits System.EventArgs

#Region "Constructor"

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal enabled As Boolean)
        _enabled = enabled
    End Sub

#End Region

#Region "Private Variable"

    Private _enabled As Boolean

#End Region

#Region "Public Property"

    Public Property Enabled() As Boolean
        Get
            Return _enabled
        End Get
        Set(ByVal Value As Boolean)
            _enabled = Value
        End Set
    End Property

#End Region

End Class
