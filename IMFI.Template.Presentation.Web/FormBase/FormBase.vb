﻿#Region "header Info"

#Region "Code Disclaimer"

'************************************************************
'*															*
'*	             Copyright © 2015 IMFI                      *
'*															*
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*															*
'*   Author      : IMFI Development Team		            *
'*   Purpose     : Form Base.                               *
'*   Called By   :                                          *
'*															*
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Template.Common
Imports IMFI.Template.DomainObjects
Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports System.IO
Imports System.Reflection

#End Region

#End Region

Public Class FormBase
    Inherits System.Web.UI.Page

#Region "Event Handler"

    Public Event IMFI_SetControlToAddMode As EnabledEventHandler
    Public Event IMFI_SetControlToEditMode As EnabledEventHandler
    Public Event IMFI_SetControlToOtherMode As EnabledEventHandler
    Public Event IMFI_ClearControl As EventHandler

#End Region

#Region "Private Variable"

    Private _PageGroupId As String = ""
    Private m_AccessRight As IAccessRight

#End Region

#Region "Public Property"

    Public Property PageGroupId() As String
        Get
            Return _PageGroupId.Trim.ToUpper
        End Get
        Set(ByVal Value As String)
            _PageGroupId = Value
        End Set
    End Property

    Public Property PageGroupSession() As Object
        Get
            If IsNothing(Session(PAGE_GROUP_SESSION)) Then
                Session(PAGE_GROUP_SESSION) = Nothing
            End If
            Return Session(PAGE_GROUP_SESSION)
        End Get
        Set(ByVal Value As Object)
            Session(PAGE_GROUP_SESSION) = Value
        End Set
    End Property

    Public Property CurrentFormMode() As FormBaseEnum.FormMode
        Get
            If IsNothing(Session("CurrentFormMode" + PageURL.GetPageName)) Then
                Return Nothing
                'Throw New Exception("Current Form Mode not define!")
            End If
            Return Session("CurrentFormMode" + PageURL.GetPageName)
        End Get
        Set(ByVal Value As FormBaseEnum.FormMode)
            Session("CurrentFormMode" + PageURL.GetPageName) = Value
            PrepareControl(Value)
        End Set
    End Property

#End Region

#Region "Form Handler"

    Protected Overloads Overrides Sub OnLoad(ByVal e As System.EventArgs)
        If Not IsValidPageGroupId() Then
            Throw New Exception("[IMFI - INTERNAL ERROR] : PAGE GROUP ID ISN'T VALID!")
        End If

        m_AccessRight = (New UserIdentification).GetUserCredential.GetPageAccess(Me.PageGroupId)

        If IsNothing(m_AccessRight) OrElse Not m_AccessRight.FLAG_INQ Then
            Session(SessionConstant.NOT_AUTHORIZED) = m_AccessRight
            Response.Redirect("~/NotAuthorized/NotAuthorized/NotAuthorized.aspx")
        End If

        DestroyPreviousPageGroupSession()
        AssignCurrentPageGroupSession()

        If IsNothing(Server.GetLastError()) Then
            Dim oQueryStringBuilder As New QueryStringBuilder
            oQueryStringBuilder.ReadQueryString()

            Dim sFormMode As String = oQueryStringBuilder.GetQSFormModeItem

            If IsNothing(sFormMode) Then
                If IsNothing(Me.CurrentFormMode) Then
                    CurrentFormMode = FormBaseEnum.FormMode.ADD
                End If

                If Not IsPostBack Then
                    RaiseEvent IMFI_ClearControl(Me, New EventArgs)
                End If
            Else
                CurrentFormMode = CType(System.Enum.Parse(GetType(FormBaseEnum.FormMode), sFormMode.ToUpper), FormBaseEnum.FormMode)
            End If
        End If

        MyBase.OnLoad(e)
    End Sub

#End Region

#Region "Session Handler"

#Region "Private Constant"

    Private Const CURRENT_PAGE_GROUP_SESSION As String = "CURRENT_PAGE_GROUP_SESSION"
    Private Const PAGE_GROUP_SESSION As String = "PAGE_GROUP_SESSION"

#End Region

#Region "Private Property"

    Private Property CurrentPageGroupSession() As String
        Get
            If IsNothing(Session(CURRENT_PAGE_GROUP_SESSION)) Then
                Session(CURRENT_PAGE_GROUP_SESSION) = ""
            End If
            Return CType(Session(CURRENT_PAGE_GROUP_SESSION), String).Trim.ToUpper
        End Get
        Set(ByVal Value As String)
            Session(CURRENT_PAGE_GROUP_SESSION) = Value
        End Set
    End Property

#End Region

#Region "Private Method"

    Private Sub ClearSessionByPageGroupID(ByVal pageGroupId As String)
        Dim oEnumerator As IEnumerator = Session.GetEnumerator()
        Dim oList As New ArrayList

        While oEnumerator.MoveNext
            Dim currentSession As String = CType(oEnumerator.Current(), String)
            If currentSession.IndexOf(pageGroupId) > -1 Then
                oList.Add(currentSession)
            End If
        End While

        For index As Int16 = 0 To oList.Count - 1
            Session(oList(index)) = Nothing
            Session.Remove(oList(index))
        Next
    End Sub

    Private Sub ClearSession(ByVal pageName As String)
        Dim oEnumerator As IEnumerator = Session.GetEnumerator()
        Dim oList As New ArrayList

        While oEnumerator.MoveNext
            Dim currentSession As String = CType(oEnumerator.Current(), String)
            If currentSession.IndexOf(pageName) > -1 Then
                oList.Add(currentSession)
            End If
        End While

        For index As Int16 = 0 To oList.Count - 1
            Session(oList(index)) = Nothing
            Session.Remove(oList(index))
        Next
    End Sub

#End Region

#Region "Page Lavel Session"

    Public Sub ClearPageSession(ByVal pageName As String)
        ClearSession(pageName)
    End Sub

    Public Sub ClearPageSession()
        ClearSession(PageURL.GetPageName)
    End Sub

    Public Sub PageSession(ByVal sessionName As String, ByVal value As Object)
        Session(sessionName + PageURL.GetPageName) = value
    End Sub

    Public Function PageSession(ByVal sessionName As String) As Object
        Return Session(sessionName + PageURL.GetPageName)
    End Function
#End Region

#Region "Application Level Session"

#Region "Private Method"

    Private Sub AssignCurrentPageGroupSession()
        CurrentPageGroupSession = Me.PageGroupId
    End Sub

    Private Sub DestroyPreviousPageGroupSession()
        If CurrentPageGroupSession.Length > 0 AndAlso CurrentPageGroupSession <> Me.PageGroupId Then
            Session(PAGE_GROUP_SESSION) = Nothing
            Session.Remove(PAGE_GROUP_SESSION)
            ClearSessionByPageGroupID(CurrentPageGroupSession)
            ClearSession("LOOKUP")
            ClearSession("CurrentFormMode")
        End If
    End Sub

#End Region

#Region "Public Method"

    Public Sub ClearPageGroupSession()
        Session(PAGE_GROUP_SESSION) = Nothing
        Session.Remove(PAGE_GROUP_SESSION)
    End Sub

#End Region

#End Region

#End Region

#Region "Private Method"

    Private Sub PrepareControl(ByVal mode As FormBaseEnum.FormMode)
        'Saat Add saja set true
        RaiseEvent IMFI_SetControlToAddMode(Me, New EnabledEventArgs(mode = FormBaseEnum.FormMode.ADD))
        RaiseEvent IMFI_SetControlToEditMode(Me, New EnabledEventArgs(Not (mode = FormBaseEnum.FormMode.VIEW Or mode = FormBaseEnum.FormMode.DELETE)))
        RaiseEvent IMFI_SetControlToOtherMode(Me, New EnabledEventArgs(Not (mode = FormBaseEnum.FormMode.VIEW Or mode = FormBaseEnum.FormMode.DELETE)))

        If mode = FormBaseEnum.FormMode.ADD Then
            If Not IsPostBack Then
                RaiseEvent IMFI_ClearControl(Me, New EventArgs)
            End If
        End If
    End Sub

    Private Function IsValidPageGroupId() As Boolean
        Return _PageGroupId.Length > 0
    End Function

#End Region

End Class
