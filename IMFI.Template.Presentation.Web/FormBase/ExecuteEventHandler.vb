﻿Public Delegate Sub ExecuteEventHandler(ByVal Sender As Object, ByVal e As ExecuteEventArgs)

Public Class ExecuteEventArgs
    Inherits System.EventArgs

#Region "Constructor"

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal Result As Object)
        _result = Result
    End Sub

#End Region

#Region "Private Variable"

    Private _result As Object

#End Region

#Region "Public Property"

    Public Property Result() As Object
        Get
            Return _result
        End Get
        Set(ByVal Value As Object)
            _result = Value
        End Set
    End Property

#End Region

End Class
