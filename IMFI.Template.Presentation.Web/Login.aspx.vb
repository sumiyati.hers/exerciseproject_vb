﻿Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Template.BusinessFacade
Imports IMFI.Template.DomainObjects
Imports IMFI.Template.Common
Imports IMFI.SC.DomainObjects

Public Class Login
    Inherits System.Web.UI.Page

#Region "Private Constant"

    Private Const CURRENT_PAGE_GROUP_SESSION As String = "CURRENT_PAGE_GROUP_SESSION"
    Private Const PAGE_GROUP_SESSION As String = "PAGE_GROUP_SESSION"

#End Region

#Region "Private Property"

    Private Property CurrentPageGroupSession() As String
        Get
            If IsNothing(Session(CURRENT_PAGE_GROUP_SESSION)) Then
                Session(CURRENT_PAGE_GROUP_SESSION) = ""
            End If
            Return CType(Session(CURRENT_PAGE_GROUP_SESSION), String).Trim.ToUpper
        End Get
        Set(ByVal Value As String)
            Session(CURRENT_PAGE_GROUP_SESSION) = Value
        End Set
    End Property

#End Region

#Region "Page Handler"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtUserID.Focus()

        If Not IsPostBack Then
            FillBranch()
        End If

#If DEBUG Then
        Me.txtUserID.Text = "admind3v"
        Me.txtPassword.Text = "!2Imfi"
        Me.txtBranch.Items.FindByValue("100").Selected = True
        btLogin_Click(sender, Nothing)
#End If

        btLogin.CssPostfix = "Button True"
    End Sub

#End Region

#Region "Control Handler"

    Private Sub btLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btLogin.Click
        Try
            Dim oUserIdentification As UserIdentification = New UserIdentification
            Dim oIUserCredential As IUserCredential = CType((New SecurityWrapper).AuthenticateLogin(Me.txtUserID.Text, txtPassword.Text, txtBranch.SelectedItem.Value, System.Configuration.ConfigurationManager.AppSettings("PREFIX_PROG_ID"), System.Configuration.ConfigurationManager.AppSettings("PREFIX_GROUP_ID")), IUserCredential)

            If Not oIUserCredential.IsLoginError AndAlso Not IsNothing(txtBranch.SelectedItem.Value) Then
                oUserIdentification.Register(oIUserCredential)
                DestroyPreviousPageGroupSession()

                Response.Redirect("~\Transaction\Dashboard\Dashboard.aspx")
            Else
                tableNotification.Visible = True
                txtNotification.Text = oIUserCredential.LoginMessage
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtUserID_TextChanged(sender As Object, e As System.EventArgs) Handles txtUserID.TextChanged
        'Dim oRowCount As Int32
        'Dim oArray As ArrayList = (New vPPD_EmployeeBusinessFacade).RetrieveListPaging(-1, 0, "Nama ASC", New PPD.DomainObjects.ObjectTransporter(New vPPD_Employee, Nothing), "NoPeg = '" & txtUserID.Text.Trim & "' AND fKeluar = 0", oRowCount)

        'If oArray.Count = 1 Then
        '    txtBranch.Items.FindByValue(CType(oArray(0), vPPD_Employee).KdCabang).Selected = True
        '    txtPassword.Focus()
        'ElseIf txtUserID.Text = (New GlobalVariable).PPD_ADMIN_USERNAME Then
        '    txtBranch.Items.FindByValue("100").Selected = True
        '    txtPassword.Focus()
        'Else
        '    txtBranch.Items.FindByValue(Nothing).Selected = True
        '    txtUserID.Focus()
        'End If

        'tableNotification.Visible = False
        txtBranch.Focus()
    End Sub

#End Region

#Region "Private Method"

    Private Sub ClearSession(ByVal pageName As String)
        Dim oEnumerator As IEnumerator = Session.GetEnumerator()
        Dim oList As New ArrayList

        While oEnumerator.MoveNext
            Dim currentSession As String = CType(oEnumerator.Current(), String)
            If currentSession.IndexOf(pageName) > -1 Then
                oList.Add(currentSession)
            End If
        End While

        For index As Int16 = 0 To oList.Count - 1
            Session(oList(index)) = Nothing
            Session.Remove(oList(index))
        Next
    End Sub

    Private Sub ClearSessionByPageGroupID(ByVal pageGroupId As String)
        Dim oEnumerator As IEnumerator = Session.GetEnumerator()
        Dim oList As New ArrayList

        While oEnumerator.MoveNext
            Dim currentSession As String = CType(oEnumerator.Current(), String)
            If currentSession.IndexOf(pageGroupId) > -1 Then
                oList.Add(currentSession)
            End If
        End While

        For index As Int16 = 0 To oList.Count - 1
            Session(oList(index)) = Nothing
            Session.Remove(oList(index))
        Next
    End Sub

    Private Sub DestroyPreviousPageGroupSession()
        If CurrentPageGroupSession.Length > 0 Then
            Session(PAGE_GROUP_SESSION) = Nothing
            Session.Remove(PAGE_GROUP_SESSION)
            ClearSessionByPageGroupID(CurrentPageGroupSession)
            ClearSession("LOOKUP")
            ClearSession("CurrentFormMode")
        End If
    End Sub

    Private Sub PrepareUserIdentification(ByVal oIUserCredential As IUserCredential)
        Dim oUserIdentification As UserIdentification = New UserIdentification
        oUserIdentification.Register(oIUserCredential)
    End Sub

    Private Sub FillBranch()
        Dim oRowCount As Int16
        Dim oArray As ArrayList = (New BranchDataHelper).RetrievePagingList(-1, 0, "BRANCH_CODE_REP ASC", (New DomainObjects.ObjectTransporter(Nothing, Nothing)), Nothing, oRowCount)

        txtBranch.Items.Clear()
        txtBranch.Items.Add("", Nothing)

        For index As Int16 = 0 To oArray.Count - 1
            txtBranch.Items.Add(CType(oArray(index), DS_COMPANY_REP).BRANCH_CODE_REP.ToString & " - " & (New BranchFormat).BranchNameOnly(CType(oArray(index), DS_COMPANY_REP).BRANCH_NAME.ToString), CType(oArray(index), DS_COMPANY_REP).BRANCH_CODE_REP)
        Next
    End Sub

#End Region

End Class