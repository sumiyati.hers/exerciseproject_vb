﻿Imports IMFI.Template.ReportService
Imports IMFI.Template.Common
Imports System
Imports System.IO
Imports System.Text
Imports System.Collections.Specialized
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class ReportPage
    Inherits System.Web.UI.Page

    Private repDoc As ReportDocument = Nothing

#Region "Form Handler"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Clear()
        Response.Buffer = True

        Dim oStream As New MemoryStream
        Dim oService As New ReportingService
        Dim oReportParameterBuilder As New ReportParameterBuilder(Request.QueryString)

        repDoc = oService.CreateReportDocument( _
            oReportParameterBuilder.ReportName, _
            oReportParameterBuilder.ParameterType, False, _
            oReportParameterBuilder.Parameter.ToArray())

        Dim oFileName As String = oReportParameterBuilder.ReportName.ToString & DateTime.Now.ToString("yyyyMMdd")

        If oReportParameterBuilder.ReportType = 0 Then
            'PDF
            oStream = repDoc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-disposition", "filename=""" & oFileName & ".pdf" & """")

            repDoc.Close()
            repDoc.Dispose()

            Response.BinaryWrite(oStream.ToArray())
            Response.End()
        ElseIf oReportParameterBuilder.ReportType = 1 Then
            'Excel
            Dim oExcelFormatOptions As ExcelFormatOptions = ExportOptions.CreateExcelFormatOptions()
            Dim oExportOptions As New ExportOptions()

            'oExcelFormatOptions.ExcelUseConstantColumnWidth = True
            'oExcelFormatOptions.ExcelConstantColumnWidth = 5
            oExcelFormatOptions.ExportPageBreaksForEachPage = True
            oExcelFormatOptions.ExportPageHeadersAndFooters = ExportPageAreaKind.OnEachPage

            oExportOptions.ExportFormatType = ExportFormatType.Excel
            oExportOptions.FormatOptions = oExcelFormatOptions
            oExportOptions.ExportFormatOptions = oExcelFormatOptions

            repDoc.ExportToHttpResponse(oExportOptions, Response, True, oFileName & ".xls")

            repDoc.Close()
            repDoc.Dispose()
        End If

        'Try

        'Catch ex As Exception

        'Finally
        oStream.Flush()
        oStream.Close()
        oStream.Dispose()
        'End Try

        GC.Collect()
    End Sub

#End Region

    Private Sub ReportPage_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        If Not IsNothing(repDoc) Then
            repDoc.Close()
            repDoc.Dispose()
            GC.Collect()
        End If
    End Sub

End Class