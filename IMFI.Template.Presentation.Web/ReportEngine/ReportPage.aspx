﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportPage.aspx.vb" Inherits="IMFI.Template.Presentation.Web.ReportPage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="cr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Title</title>
    <link rel="Shortcut Icon" href="~/Images/icon.ico" />

    <link rel="stylesheet" type="text/css" href="~/Styles/IMFI.css" />
    <link rel="stylesheet" type="text/css" href="~/Styles/CRdefault.css" />
</head>
<body id="Body1" runat="server">

<form id="UserReportForm" method="post" runat="server">
	<cc1:toolkitscriptmanager id="ToolkitScriptManager1" runat="server"></cc1:toolkitscriptmanager>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <cr:CrystalReportViewer ID="rvUserReport"
                    runat="server"
                    AutoDataBind="true"
                    EnableTheming="false"
                    HasCrystalLogo="False"
                    HasDrillUpButton="False"
                    HasExportButton="True"
                    HasPrintButton="False"
                    HasRefreshButton="False"
                    HasToggleGroupTreeButton="False"
                    ToolbarImagesFolderUrl="../../Images/CRToolBar/"
                    BestFitPage="False"
                    Width="100%" />
            </td>
        </tr>
    </table>
</form>

</body>
</html>

<%--<asp:Content ID="Head" runat="server" ContentPlaceHolderID="HeadColumnContent">
    <link href="~/Styles/CRdefault.css" type="text/css" />
</asp:Content>

<asp:Content ID="Main" runat="server" ContentPlaceHolderID="MainColumnContent">
    <cr:crystalreportviewer ID="rvUserReport" runat="server" 
        
        AutoDataBind="true"
        EnableTheming="False" 
        HasCrystalLogo="False" 
        HasDrillUpButton="False" 
        HasExportButton="true" 
        HasPrintButton="False" 
        HasRefreshButton="True"
        HasToggleGroupTreeButton="False" 
        ToolbarImagesFolderUrl="~/Images/CRToolBar/" />
</asp:Content>--%>