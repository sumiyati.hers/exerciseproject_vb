﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChangePassword.aspx.vb" Inherits="IMFI.Template.Presentation.Web.ChangePassword" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Title</title>
    <link rel="Shortcut Icon" href="~/Images/icon.ico" />

    <link rel="stylesheet" type="text/css" href="~/Styles/IMFI.css" />
</head>
<body class="ZBColor">
    <form id="form1" runat="server" onkeypress="checkKey()" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
        <div style="position: relative; top: 350px;">
            <div id="WhiteContainer" class="WhiteContainer RadiusAll">
                <table cellpadding="1px" cellspacing="0px" border="0" width="400px">
                    <tr>
                        <td class="TextBoxPaddingLeft_50 RadiusTopLeft">
                            <dx:ASPxLabel ID="lblUsername" runat="server" Text="Username">
                            </dx:ASPxLabel>
                        </td>
                        <td class="TextBoxPaddingRight_50 RadiusTopRight">
                            <dx:ASPxLabel ID="txtUsername" runat="server" Text="">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="TextBoxPaddingLeft_50 RadiusBottomLeft">
                            <dx:ASPxLabel ID="lblOldPassword" runat="server" Text="Password Lama">
                            </dx:ASPxLabel>
                        </td>
                        <td class="TextBoxPaddingRight_50 RadiusBottomRight">
                            <dx:ASPxTextBox ID="txtOldPassword" runat="server" Width="100%" MaxLength="10" Password="true">
                                <Border BorderStyle="None" />
                                <DisabledStyle BackColor="Transparent" ForeColor="ControlDarkDark">
                                    <Border BorderStyle="None" />
                                </DisabledStyle>
                                <ValidationSettings CausesValidation="false" Display="Static" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="true" ErrorText="Password Lama tidak boleh dikosongkan."></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="TextBoxPaddingLeft_50 RadiusBottomLeft">
                            <dx:ASPxLabel ID="lblNewPassword" runat="server" Text="Password Baru">
                            </dx:ASPxLabel>
                        </td>
                        <td class="TextBoxPaddingRight_50 RadiusBottomRight">
                            <dx:ASPxTextBox ID="txtNewPassword" runat="server" Width="100%" MaxLength="10" Password="true">
                                <Border BorderStyle="None" />
                                <DisabledStyle BackColor="Transparent" ForeColor="ControlDarkDark">
                                    <Border BorderStyle="None" />
                                </DisabledStyle>
                                <ValidationSettings CausesValidation="true" Display="Static" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="true" ErrorText="Password Baru tidak boleh dikosongkan."></RequiredField>
                                    <RegularExpression ValidationExpression="^(.{0,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{4,})|(.{1,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{3,})|(.{2,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{2,})|(.{3,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{1,})|(.{4,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{0,})$" ErrorText="Masukkan Kombinasi Minimal 6 Huruf, Angka, dan Tanda Baca (Misal ! dan &)." />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="TextBoxPaddingLeft_50 RadiusBottomLeft">
                            <dx:ASPxLabel ID="lblResumeNewPassword" runat="server" Text="Ulangi Password Baru">
                            </dx:ASPxLabel>
                        </td>
                        <td class="TextBoxPaddingRight_50 RadiusBottomRight">
                            <dx:ASPxTextBox ID="txtResumeNewPassword" runat="server" Width="100%" MaxLength="10" Password="true">
                                <Border BorderStyle="None" />
                                <DisabledStyle BackColor="Transparent" ForeColor="ControlDarkDark">
                                    <Border BorderStyle="None" />
                                </DisabledStyle>
                                <ValidationSettings CausesValidation="true" Display="Static" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="true" ErrorText="Password Baru tidak boleh dikosongkan."></RequiredField>
                                    <RegularExpression ValidationExpression="^(.{0,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{4,})|(.{1,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{3,})|(.{2,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{2,})|(.{3,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{1,})|(.{4,}(([a-zA-Z][^a-zA-Z])|([^a-zA-Z][a-zA-Z])).{0,})$" ErrorText="Masukkan Kombinasi Minimal 6 Huruf, Angka, dan Tanda Baca (Misal ! dan &)." />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <br />
                            <dx:ASPxButton ID="btChange" Runat="server" CausesValidation="true" Text="CHANGE" tabIndex="100" CssClass="ButtonLogin True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <br />
                            <asp:LinkButton ID="btCancel" runat="server" OnClientClick="gridLoadingPanel.Show();" ForeColor="#FFFFFF">Cancel ...</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="height: 100px;">
                                <table id="tableNotification" visible="false" cellspacing="20" border="0" width="100%" runat="server">
                                    <tr>
                                        <td align="center" valign="top" class="triangle-right">
                                            <dx:ASPxLabel ID="txtExpiredSession" runat="server" Text="SOMETHING'S WRONG!" Font-Bold="true" ForeColor="#DD0000">
                                            </dx:ASPxLabel>
                                            
                                            <br /><br />
                                            
                                            <dx:ASPxLabel ID="txtNotification" runat="server" Text="" EncodeHtml="false">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <dx:ASPxImage ID="imgWarning" runat="server" ShowLoadingImage="true" ImageUrl="~/Images/userSmaller.png" Height="50px">
                                            </dx:ASPxImage>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    
        <div id="Footer" class="Footer">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <dx:ASPxLabel ID="txtCopyright" runat="server" Text="Copyright &copy 2017 PT. Indomobil Finance Indonesia" ForeColor="#FCFCFC" EncodeHtml="false">
                        </dx:ASPxLabel>
                    </td>
                    <td align="right">
                        <dx:ASPxLabel ID="txtBestViewed" runat="server" Text="Best viewed in Mozilla Firefox" ForeColor="#FCFCFC">
                        </dx:ASPxLabel>
                    </td>
                    <td width="20px">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>

    </form>
</body>
</html>
