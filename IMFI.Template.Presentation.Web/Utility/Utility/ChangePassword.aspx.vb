﻿Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Template.BusinessFacade
Imports IMFI.Template.DomainObjects
Imports IMFI.Template.Common
Imports IMFI.SC.DomainObjects

Public Class ChangePassword
    Inherits System.Web.UI.Page

#Region "PRIVATE"

    Private Const CURRENT_PAGE_GROUP_SESSION As String = "CURRENT_PAGE_GROUP_SESSION"
    Private Const PAGE_GROUP_SESSION As String = "PAGE_GROUP_SESSION"

#End Region

#Region "PAGE"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(New UserIdentification().CurrentUserInfo) Then
            txtUsername.Text = New UserIdentification().CurrentUserInfo.USER_NAME.Trim.ToUpper & " ( " & New UserIdentification().CurrentUserInfo.USER_ID & " )"
        Else
            txtUsername.Text = New UserIdentification().CurrentEmployeeInfo.Name.Trim.ToUpper & " ( " & New UserIdentification().CurrentEmployeeInfo.NIK & " )"
        End If

        'If Not IsPostBack Then
        '    Dim oUserIdentification As UserIdentification = New UserIdentification

        '    txtUsername.Text = oUserIdentification.CurrentEmployeeInfo.NIK & " - " & oUserIdentification.CurrentUserInfo.USER_NAME
        'End If

        txtOldPassword.Focus()

        btChange.CssPostfix = "ButtonLogin True"
    End Sub

#End Region

#Region "NAVIGATION"

    Private Sub btCancel_Click(sender As Object, e As System.EventArgs) Handles btCancel.Click
        Response.Redirect("~\Login.aspx")
    End Sub

    Private Sub btChange_Click(sender As Object, e As System.EventArgs) Handles btChange.Click
        Dim oValid As Boolean
        Dim oOldPassword As String = (New SecurityWrapper).DecodePassword((New UserIdentification).CurrentUserInfo.USER_PASSWORD)

        If txtOldPassword.Text = oOldPassword Then
            If txtNewPassword.Text = txtResumeNewPassword.Text Then
                oValid = True
            Else
                tableNotification.Visible = True
                txtNotification.Text = "Kedua Password Baru Anda Tidak Sama."

                txtResumeNewPassword.Focus()
            End If
        Else
            tableNotification.Visible = True
            txtNotification.Text = "Password Lama Anda Salah"

            txtOldPassword.Focus()
        End If

        If oValid Then
            Dim oSecurityWrapper As New SecurityWrapper
            Dim oUserCre As IUserCredential = oSecurityWrapper.ChangePassword((New UserIdentification).GetUserCredential, txtNewPassword.Text)

            If oUserCre.IsLoginError Then
                tableNotification.Visible = True
                txtNotification.Text = oUserCre.LoginMessage

                txtNewPassword.Focus()
            Else
                Response.Redirect("~\Login.aspx")
            End If
        End If
    End Sub

#End Region

End Class