﻿Imports IMFI.Template.Common
Imports IMFI.Template.DomainObjects
Imports IMFI.Template.BusinessFacade

Public Class IMFI
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(New UserIdentification().CurrentUserInfo) Then
            txtLogin.Text = "Selamat Datang, " & New UserIdentification().CurrentUserInfo.USER_NAME.Trim.ToUpper & " ( " & New UserIdentification().CurrentUserInfo.USER_ID & " )"
        Else
            txtLogin.Text = "Selamat Datang, " & New UserIdentification().CurrentEmployeeInfo.Name.Trim.ToUpper & " ( " & New UserIdentification().CurrentEmployeeInfo.NIK & " )"
        End If
    End Sub

End Class