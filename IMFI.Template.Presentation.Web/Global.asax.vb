﻿Imports System.Web.SessionState
Imports IMFI.Template.BusinessFacade
Imports IMFI.Template.Common
Imports System.Globalization

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
        FrameworkConfigurationWrapper.Configure()
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
        Dim lang As String = "id-ID"

        Try
            Dim newCulture = New System.Globalization.CultureInfo(lang)
            'System.Threading.Thread.CurrentThread.CurrentCulture = _
            'New System.Globalization.CultureInfo(lang)

            'Dim newCulture As CultureInfo = CultureInfo.CurrentCulture.Clone()

            newCulture.DateTimeFormat.AbbreviatedDayNames = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"}
            newCulture.DateTimeFormat.MonthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""}
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
        Catch
            ' NOOP
        End Try
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
        If Not IsNothing(HttpContext.Current.Session) Then
            Dim ex As Exception = CType(Server.GetLastError(), Exception)
            Session("LAST_ERROR_URL") = HttpContext.Current.Request.Url.ToString()

            If Not IsNothing(ex) Then
                Session("ERROR") = Server.GetLastError()
            End If

            If IsNothing(Session(UserIdentification.USER_AUTORIZATION_SESSION_NAME)) Then
                Server.Transfer("~\Login.aspx")
            Else
                'HttpContext.Current.Response.Redirect("~\ErrorHandler\ErrorHandler\ErrorPage.aspx")
            End If
            'Else
            'HttpContext.Current.Response.Redirect("~\ErrorHandler\ErrorHandler\ErrorPage.aspx")
        End If
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class