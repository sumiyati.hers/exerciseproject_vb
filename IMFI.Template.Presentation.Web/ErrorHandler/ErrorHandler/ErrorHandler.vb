﻿Imports System.Text

Public Class ErrorHandler

#Region "Private Variable"

    Private _headerMessage As StringBuilder
    Private _exception As Exception
    Private _requestPath As String

#End Region

#Region "Constructor"

    Public Sub New(ByVal Ex As Exception, ByVal RequestPath As String)
        _exception = Ex
        _headerMessage = New StringBuilder(GetShortTitle(GetInnerMessage(InnerMessage())))
        _requestPath = RequestPath
    End Sub

    Public Sub New(ByVal Ex As Exception, ByVal headerMessage As String, ByVal RequestPath As String)
        _exception = Ex
        _headerMessage = New StringBuilder(headerMessage)
        _requestPath = RequestPath
    End Sub

#End Region

#Region "Private Property"

    Private ReadOnly Property Source() As String
        Get
            Return GetShortTitle(_exception.Source)
        End Get
    End Property

    Private ReadOnly Property Message() As String
        Get
            Return GetShortTitle(_exception.Message)
        End Get
    End Property

    Private ReadOnly Property StackTrace() As String
        Get
            Return GetInnerException(_exception).StackTrace
        End Get
    End Property

    Private ReadOnly Property InnerMessage() As String
        Get
            Return GetInnerException(_exception).Message
        End Get
    End Property

#End Region

#Region "Public Function"

    Public Function Createhtml() As String
        If _exception.InnerException.GetType().Name = "CustomException" Or _exception.InnerException.GetType().Name = "ConstraintException" Or _exception.InnerException.GetType().Name = "InsertUpdateConstraintException" Or _exception.InnerException.GetType().Name = "UniqueRecordException" Then
            Return GetCustomErrorhtml()
        Else
            Return GetErrorhtml()
        End If
    End Function

#End Region

#Region "Private Method"

    Private Function GetErrorhtml() As String
        Dim ohtmlBuilder As New StringBuilder
        Dim oDetailBuilder As New StringBuilder

        ohtmlBuilder.Append("<table width=98% height=96% border=0 cellpadding=0 align=center cellspacing=0>")
        ohtmlBuilder.Append("<tr><td height=25%>")
        ohtmlBuilder.Append("<table width=100%  border=0 cellspacing=0 cellpadding=0>")
        ohtmlBuilder.Append("<tr><td class=errTitle>")
        ohtmlBuilder.AppendFormat("{0}", Me._headerMessage.ToString())
        ohtmlBuilder.Append(" </td></tr></table>")
        ohtmlBuilder.Append("</td></tr>")
        ohtmlBuilder.Append("<tr>")
        ohtmlBuilder.Append("<td align=left valign=top height=50%>")
        ohtmlBuilder.Append("<table width=100% border=0 cellspacing=0 cellpadding=4>")
        ohtmlBuilder.Append("<tr>")
        ohtmlBuilder.Append("<td id='panelheader' class=yellowheader>Error Detail : </td>")
        ohtmlBuilder.Append("</tr>")
        ohtmlBuilder.Append("<tr>")
        ohtmlBuilder.Append("<td>")

        ohtmlBuilder.AppendFormat("{0}", ErrorDetail)

        ohtmlBuilder.Append("</td>")
        ohtmlBuilder.Append("</tr>")
        ohtmlBuilder.Append("</table></td></tr></table>")

        Return ohtmlBuilder.ToString()
    End Function

    Private Function GetCustomErrorhtml() As String
        Dim ohtmlBuilder As New StringBuilder
        Dim oDetailBuilder As New StringBuilder

        ohtmlBuilder.Append("<table width=98% height=96% border=0 cellpadding=0 align=center cellspacing=0>")
        ohtmlBuilder.Append("<tr><td height=25%>")
        ohtmlBuilder.Append("<table width=100%  border=0 cellspacing=0 cellpadding=0>")
        ohtmlBuilder.Append("<tr><td class=errTitle>")
        ohtmlBuilder.AppendFormat("{0}", Me._headerMessage.ToString())
        ohtmlBuilder.Append(" </td></tr></table>")
        ohtmlBuilder.Append("</td></tr>")
        ohtmlBuilder.Append("<tr>")
        ohtmlBuilder.Append("<td align=left valign=top height=50%>")
        ohtmlBuilder.Append("<table width=100% border=0 cellspacing=0 cellpadding=4>")
        ohtmlBuilder.Append("<tr>")
        ohtmlBuilder.Append("<td align=left valign=top> <a href=""")
        ohtmlBuilder.Append(HttpContext.Current.Session("LAST_ERROR_URL"))
        ohtmlBuilder.Append(""" onMouseOver=""self.status=document.referrer;return false"")"">BACK</a> ")
        ohtmlBuilder.Append("</td>")
        ohtmlBuilder.Append("</tr>")
        ohtmlBuilder.Append("</table></td></tr></table>")

        Return ohtmlBuilder.ToString()
    End Function

    Private Function GetShortTitle(ByVal description As String) As String
        Dim messageBuilder As New StringBuilder
        Dim maxLength As Int32 = 300

        messageBuilder.Append(GetInnerMessage(description))

        If messageBuilder.ToString().Length > maxLength Then
            messageBuilder.Remove(maxLength - 3, _
                messageBuilder.ToString().Length - maxLength + 3)
            messageBuilder.Append("...")
        End If

        Return messageBuilder.ToString()
    End Function

    Private Function GetInnerMessage(ByVal description As String) As String
        Dim index As Integer = description.IndexOf("%%") + 2

        If index >= 2 Then
            description = description.Substring(index, description.Length - index)

            If description.IndexOf("%") >= 0 Then
                description = description.Substring(0, description.IndexOf("%%"))
            End If
        End If

        Return description
    End Function

    Private Function ErrorDetail() As String
        Dim oDetailBuilder As New StringBuilder

        oDetailBuilder.Append("<div id='panelDetail'><table width=100% height=100% border=0 cellspacing=0 cellpadding=4>")

        oDetailBuilder.Append("<tr>")
        oDetailBuilder.Append("<td width=150 valign=top>Inner Message</td>")
        oDetailBuilder.Append("<td width=20 valign=top>:</td>")
        oDetailBuilder.AppendFormat("<td>{0}</td>", GetShortTitle(GetInnerMessage(InnerMessage)))
        oDetailBuilder.Append("</tr>")

        oDetailBuilder.Append("<tr class=errDetailAlt>")
        oDetailBuilder.Append("<td valign=top>Page</td>")
        oDetailBuilder.Append("<td valign=top>:</td>")
        oDetailBuilder.AppendFormat("<td>{0}</td>", _requestPath)
        oDetailBuilder.Append("</tr>")

        oDetailBuilder.Append("<tr>")
        oDetailBuilder.Append("<td valign=top>Source</td>")
        oDetailBuilder.Append("<td valign=top>:</td>")
        oDetailBuilder.AppendFormat("<td>{0}</td>", Source)
        oDetailBuilder.Append("</tr>")

        oDetailBuilder.Append("<tr class=errDetailAlt>")
        oDetailBuilder.Append("<td valign=top>Message</td>")
        oDetailBuilder.Append("<td valign=top>:</td>")
        oDetailBuilder.AppendFormat("<td>{0}</td>", Message)
        oDetailBuilder.Append("</tr>")

        oDetailBuilder.Append("<tr>")
        oDetailBuilder.Append("<td valign=top>Stack Trace</td>")
        oDetailBuilder.Append("<td valign=top>:</td>")
        oDetailBuilder.AppendFormat("<td>{0}</td>", GetShortTitle(StackTrace))
        oDetailBuilder.Append("</tr>")

        oDetailBuilder.Append("</table></div>")

        Return oDetailBuilder.ToString()
    End Function

    Private Function GetInnerException(ByVal ex As Exception) As Exception
        If Not IsNothing(ex.InnerException) Then
            Return GetInnerException(ex.InnerException)
        Else
            Return ex
        End If
    End Function

#End Region
End Class
