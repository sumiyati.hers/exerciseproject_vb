﻿Public Class ErrorPage
    Inherits System.Web.UI.Page

    Private Const STR_CONCURRENT_MODIFICATION_ERROR As String = "CONCURRENT_MODIFICATION_ERROR"

    Private Sub ErrorPage_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        InitError()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#Region "Private Method"

    Private Sub InitError()
        Try
            If Not IsNothing(Session("ERROR")) Then
                Dim oEx As Exception = CType(Session("ERROR"), Exception)
                Dim oErrorBuilder As New ErrorHandler(oEx, Request.Path)

                txtHeader.Text = "Something's wrong!"
                errorDetail.Text = oErrorBuilder.Createhtml()
                'Session.Remove("ERROR")
            Else
                txtHeader.Text = "Halaman Tidak Ditemukan."
                btViewAll.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region

End Class