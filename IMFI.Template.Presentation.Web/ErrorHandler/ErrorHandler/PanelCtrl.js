﻿// ***************************************** //
// ***   Author : David Pakpahan         *** //
// ***************************************** //

var _panelheader;
var _panelDetail;
var _errorMessage;
var _defaultColor = '#FFFFFF';
var _hoverColor = '#FFDD99';
var _headerMessage = 'Click here to show detail';

function AddEvent(Element, Event, Func, UseCapture) {
    if (Element.addEventListener) {
        Element.addEventListener(Event, Func, UseCapture);
    }
    else
        if (Element.attachEvent) {
            return Element.attachEvent('on' + Event, Func);
        }
        else {
            Element['on' + Event] = Func;
        }
}

function InitPage() {
    _panelheader = document.getElementById('panelheader');
    _panelDetail = document.getElementById('panelDetail');

    if (_panelheader != null) {
        SetheaderDetail();

        AddEvent(_panelheader, 'mouseover', header_OnMouseOver, false);
        AddEvent(_panelheader, 'mouseout', header_OnMouseOut, false);
        AddEvent(_panelheader, 'click', header_OnClick, false);
    }

}

function header_OnMouseOut() {
    _panelheader.bgColor = _defaultColor;
}

function header_OnMouseOver() {
    _panelheader.bgColor = _hoverColor;
}

function header_OnClick() {
    if (_panelDetail.style.visibility == 'visible') {
        _panelDetail.style.visibility = 'hidden';
        _panelheader.innerhtml = _headerMessage;
    }
    else {
        _panelDetail.style.visibility = 'visible';
        _panelheader.innerhtml = 'Click here to hide detail';
    }
}

function SetheaderDetail() {
    _panelDetail.style.visibility = 'hidden';
    _panelheader.innerhtml = _headerMessage;
}

AddEvent(window, 'load', InitPage, false);