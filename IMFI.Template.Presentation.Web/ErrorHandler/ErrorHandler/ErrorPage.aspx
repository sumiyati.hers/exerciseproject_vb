﻿<%@ Page Language="vb" MasterPageFile="~/IMFI.Master" AutoEventWireup="false" CodeBehind="ErrorPage.aspx.vb" Inherits="IMFI.Template.Presentation.Web.ErrorPage" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Head" runat="server" ContentPlaceHolderID="HeadColumnContent">
    <script src="~/ErrorHandler/ErrorHandler/PanelCtrl.js" language="javascript" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Main" runat="server" ContentPlaceHolderID="MainColumnContent">

    <dx:ASPxPopupControl ID="popUpViewApprover" ClientInstanceName="popUpViewApprover" runat="server"
        Modal="True" PopupAnimationType="None"
        PopupElementID="btViewAll" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" PopupAction="LeftMouseClick" CloseAction="OuterMouseClick"
        AppearAfter="0" DisappearAfter="0" AllowDragging="false" AllowResize="false" HeaderText=" " CssClass="RadiusAll" BackColor="#D64F4F">
        <Border BorderStyle="None" />
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                <div style="vertical-align:middle; margin: 0px 30px 30px 30px;">
                    <table cellspacing="0" cellpadding="0" border="0" width="400px" align="center">
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="errorDetail" runat="server" Text="" EncodeHtml="false" ForeColor="#FFFFFF" Font-Bold="true">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    
    <table cellpadding="0" cellspacing="0" border="0" width="90%" align="center" style="margin-top: 20px;">
        <tr>
            <td valign="top" align="center" width="450px">
                <div style="margin-right:20px;">
                    <dx:ASPxImage ID="txtImageLogo" runat="server" ShowLoadingImage="true" ImageUrl="~/Images/maintenance.png" Width="450px">
                    </dx:ASPxImage>
                </div>
            </td>
            <td valign="top">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                    <tr>
                        <td class="triangle-right Dleft">
                            <div style="font-size:75px !Important; font-weight:bolder; color:var(--ZTheme) !Important; margin: 0px 20px 0px 20px;">
                                Oops!
                            </div>
                            <div style="font-size:25px !Important; font-weight:bolder; color:var(--ZTheme) !Important; margin: 0px 20px 0px 20px;">
                                <dx:ASPxLabel ID="txtHeader" runat="server" Text="" CssClass="NotFoundHeader" EncodeHtml="false">
                                </dx:ASPxLabel>
                            </div>
                            <div style="margin: 0px 20px 20px 20px;">
                                <asp:LinkButton ID="btViewAll" runat="server" CausesValidation="false" OnClientClick="gridLoadingPanel.Show();" ForeColor="#D64F4F">Show Detail ...</asp:LinkButton>
                            </div>

                            <div style="font-size:13px !Important; font-weight:normal; color:var(--ZTheme) !Important; margin: 0px 20px 20px 20px;">
                                Silahkan mencoba langkah-langkah berikut ini:
                                <ol style="color:var(--ZTheme) !Important; margin: 0px; padding: 0px 0px 0px 13px;">
                                    <li style="font-size:13px !Important;">Periksa jaringan dan pastikan terkoneksi dengan internet dan jaringan IMFI.</li>
                                    <li style="font-size:13px !Important;">Log Out dari aplikasi dan lakukan pembersihan terhadap cache dan cookies di browser.</li>
                                    <li style="font-size:13px !Important;">Log In ke Aplikasi dan coba kembali transaksi terakhir sebelum terjadi error.</li>
                                    <li style="font-size:13px !Important;">Jika masih terjadi error, silahkan menghubungi IT HelpDesk untuk informasi lebih lanjut dengan menunjukkan pesan error saat menekan link "Show Detail".</li>
                                </ol>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>