﻿Imports IMFI.Template.Common
Imports IMFI.Template.BusinessFacade
Imports IMFI.Template.DomainObjects

Public Class GlobalVariable

    Public FormatDateTime As String = "dd/MM/yyyy HH:mm"
    Public MaxFileSize As String = "2048000"

End Class
