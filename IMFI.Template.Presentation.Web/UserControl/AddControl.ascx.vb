﻿Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Template.Common

Public Class AddControl
    Inherits System.Web.UI.UserControl

#Region "Event Handler"

    Public Event AddButton As EventHandler

#End Region

#Region "Private Variable"

    Private m_AccessRight As IAccessRight

#End Region

#Region "Form Handler"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        m_AccessRight = (New UserIdentification).GetUserCredential.GetPageAccess(CType(Me.Parent.Page, FormBase).PageGroupId())
        SetButtonAccessRight()

        imgBtnAdd.CssPostfix = "Button True"
    End Sub

#End Region

#Region "Private Method"

    Private Sub SetButtonAccessRight()
        imgBtnAdd.Visible = m_AccessRight.FLAG_ADD
    End Sub

#End Region

#Region "Button Handler"

    Private Sub imgBtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnAdd.Click
        RaiseEvent AddButton(sender, e)
    End Sub

#End Region

End Class