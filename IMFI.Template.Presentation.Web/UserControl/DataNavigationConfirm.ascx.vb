﻿Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Template.Common

Public Class DataNavigationConfirm
    Inherits System.Web.UI.UserControl

#Region "Event Handler"

    Public Event ButtonYes As EventHandler
    Public Event ButtonNos As EventHandler

#End Region

#Region "Private Variable"

    Private m_AccessRight As IAccessRight

#End Region

#Region "Form Handler"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        m_AccessRight = (New UserIdentification).GetUserCredential.GetPageAccess(CType(Me.Parent.Page, FormBase).PageGroupId())
        SetButtonAccessRight()

        btYes.CssPostfix = "Button True"
        btNos.CssPostfix = "Button False"
    End Sub

#End Region

#Region "Private Method"

    Private Sub SetButtonAccessRight()
        btYes.Visible = m_AccessRight.FLAG_ADD
        btNos.Visible = m_AccessRight.FLAG_ADD
    End Sub

#End Region

#Region "Button Handler"

    Private Sub btYes_Click(sender As Object, e As System.EventArgs) Handles btYes.Click
        RaiseEvent ButtonYes(sender, e)
    End Sub

    Private Sub btNos_Click(sender As Object, e As System.EventArgs) Handles btNos.Click
        RaiseEvent ButtonNos(sender, e)
    End Sub

#End Region

End Class