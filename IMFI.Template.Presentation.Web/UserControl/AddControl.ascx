﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AddControl.ascx.vb" Inherits="IMFI.Template.Presentation.Web.AddControl" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<table cellspacing="0" cellpadding="0" width="100%" style="text-align:center; margin-top:15px;" border="0">
	<tr>
		<td align="right">
            <dx:ASPxButton ID="imgBtnAdd" Runat="server" CausesValidation="false" Text="ADD" tabIndex="100" CssClass="Button True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="OnClickTab"></ClientSideEvents>
            </dx:ASPxButton>
	    </td>
	</tr>
</table>