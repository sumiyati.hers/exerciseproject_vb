﻿Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Template.Common

Public Class DataNavigationControl
    Inherits System.Web.UI.UserControl

#Region "Event Handler"

    Public Event BackButton As EventHandler
    Public Event NewButton As EventHandler
    Public Event ClearButton As EventHandler
    Public Event SaveAddButton As ExecuteEventHandler
    Public Event SaveUpdateButton As ExecuteEventHandler
    Public Event AfterSaveButton As ExecuteEventHandler
    Public Event DeleteButton As EventHandler
    Public Event SubmitButton As EventHandler
    Public Event UnsubmitButton As EventHandler
    Public Event RejectButton As EventHandler
    Public Event UnrejectButton As EventHandler
    Public Event ReviseButton As EventHandler
    Public Event ApproveButton As EventHandler
    Public Event UnApproveButton As EventHandler

#End Region

#Region "Private Variable"

    Private m_AccessRight As IAccessRight = Nothing

#End Region

#Region "Private Property"

    Public ReadOnly Property AccessRight() As IAccessRight
        Get
            If IsNothing(m_AccessRight) Then
                m_AccessRight = (New UserIdentification).GetUserCredential.GetPageAccess(CType(Me.Parent.Page, FormBase).PageGroupId())
            End If
            Return m_AccessRight
        End Get
    End Property

#End Region

#Region "Public Property"

#Region "Show Hide Button"

    Public WriteOnly Property ShowBackButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnBack.Visible = Value
        End Set
    End Property

    Public WriteOnly Property ShowNewButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnNew.Visible = Value And AccessRight.FLAG_ADD And Not IsViewMode()
        End Set
    End Property

    Public WriteOnly Property ShowClearButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnClear.Visible = Value And AccessRight.FLAG_ADD And Not IsViewMode()
        End Set
    End Property

    Public WriteOnly Property ShowSaveButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnSave.Visible = Value And (AccessRight.FLAG_ADD Or AccessRight.FLAG_UPDATE) And Not IsViewMode()
        End Set
    End Property

    Public WriteOnly Property ShowDeleteButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnDelete.Visible = Value And AccessRight.FLAG_DELETE And Not IsViewMode()
        End Set
    End Property

    Public WriteOnly Property ShowSubmitButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnSubmit.Visible = Value
        End Set
    End Property

    Public WriteOnly Property ShowUnsubmitButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnUnsubmit.Visible = Value
        End Set
    End Property

    Public WriteOnly Property ShowApproveButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnApprove.Visible = Value
        End Set
    End Property

    Public WriteOnly Property ShowUnapproveButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnUnapprove.Visible = Value
        End Set
    End Property

    Public WriteOnly Property ShowRejectButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnReject.Visible = Value
        End Set
    End Property

    Public WriteOnly Property ShowUnrejectButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnUnreject.Visible = Value
        End Set
    End Property

    Public WriteOnly Property ShowReviseButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnRevise.Visible = Value
        End Set
    End Property

#End Region

#Region "Enable & Disable Button"

    Public WriteOnly Property EnabledBackButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnBack.Enabled = Value
        End Set
    End Property

    Public WriteOnly Property EnabledNewButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnNew.Enabled = Value And AccessRight.FLAG_ADD And Not IsViewMode()
        End Set
    End Property

    Public WriteOnly Property EnabledClearButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnClear.Enabled = Value And AccessRight.FLAG_ADD And Not IsViewMode()
        End Set
    End Property

    Public WriteOnly Property EnabledSaveButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnSave.Enabled = Value And (AccessRight.FLAG_ADD Or AccessRight.FLAG_UPDATE) And Not IsViewMode()
        End Set
    End Property

    Public WriteOnly Property EnabledDeleteButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnDelete.Enabled = Value And AccessRight.FLAG_DELETE And Not IsViewMode()
        End Set
    End Property

    Public WriteOnly Property EnabledSubmitButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnSubmit.Enabled = Value
        End Set
    End Property

    Public WriteOnly Property EnabledUnsubmitButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnUnsubmit.Enabled = Value
        End Set
    End Property

    Public WriteOnly Property EnabledApproveButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnApprove.Enabled = Value
        End Set
    End Property

    Public WriteOnly Property EnabledUnapproveButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnUnapprove.Enabled = Value
        End Set
    End Property

    Public WriteOnly Property EnabledRejectButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnReject.Enabled = Value
        End Set
    End Property

    Public WriteOnly Property EnabledUnrejectButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnUnreject.Enabled = Value
        End Set
    End Property

    Public WriteOnly Property EnabledReviseButton() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnRevise.Enabled = Value
        End Set
    End Property

#End Region

    Public WriteOnly Property EnableCauseValidation() As Boolean
        Set(ByVal Value As Boolean)
            imgBtnSave.CausesValidation = Value
        End Set
    End Property

    Public WriteOnly Property ValidationGroup() As String
        Set(ByVal Value As String)
            imgBtnSave.ValidationGroup = Value
        End Set
    End Property

    Public WriteOnly Property IsDefault() As Boolean
        Set(ByVal Value As Boolean)
            ShowSubmitButton = Not Value
            ShowUnsubmitButton = Not Value
            ShowRejectButton = Not Value
            ShowUnrejectButton = Not Value
            ShowReviseButton = Not Value
            ShowApproveButton = Not Value
            ShowUnapproveButton = Not Value
        End Set
    End Property

#End Region

#Region "Form Handler"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SetButtonAccessRight()

        If IsAddMode() Then
            ShowDeleteButton = False
        End If

        LoadCSS()
    End Sub

    Private Sub LoadCSS()
        imgBtnSubmit.CssPostfix = "Button True"
        imgBtnUnsubmit.CssPostfix = "Button False"
        imgBtnApprove.CssPostfix = "Button True"
        imgBtnUnapprove.CssPostfix = "Button False"
        imgBtnRevise.CssPostfix = "Button False"
        imgBtnReject.CssPostfix = "Button False"
        imgBtnUnreject.CssPostfix = "Button False"

        imgBtnSave.CssPostfix = "Button True"
        imgBtnDelete.CssPostfix = "Button False"
        imgBtnNew.CssPostfix = "Button True"
        imgBtnClear.CssPostfix = "Button True"
        imgBtnBack.CssPostfix = "Button True"
    End Sub

#End Region

#Region "Button Handler"

    Private Sub imgBtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnBack.Click
        RaiseEvent BackButton(sender, e)
    End Sub

    Private Sub imgBtnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnNew.Click
        RaiseEvent NewButton(sender, e)
    End Sub

    Private Sub imgBtnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnClear.Click
        RaiseEvent ClearButton(sender, e)
    End Sub

    Private Sub imgBtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnSave.Click
        Dim executeEventArgs As New ExecuteEventArgs(Nothing)

        If CType(Me.Parent.Page, FormBase).CurrentFormMode = FormBaseEnum.FormMode.ADD Then
            RaiseEvent SaveAddButton(sender, executeEventArgs)
        Else
            RaiseEvent SaveUpdateButton(sender, executeEventArgs)
        End If
        RaiseEvent AfterSaveButton(sender, executeEventArgs)
    End Sub

    Private Sub imgBtnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnDelete.Click
        RaiseEvent DeleteButton(sender, e)
    End Sub

    Private Sub imgBtnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnSubmit.Click
        RaiseEvent SubmitButton(sender, e)
    End Sub

    Private Sub imgBtnUnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnUnsubmit.Click
        RaiseEvent UnsubmitButton(sender, e)
    End Sub

    Private Sub imgBtnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnApprove.Click
        RaiseEvent ApproveButton(sender, e)
    End Sub

    Private Sub imgBtnUnapprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnUnapprove.Click
        RaiseEvent UnApproveButton(sender, e)
    End Sub

    Private Sub imgBtnReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnReject.Click
        RaiseEvent RejectButton(sender, e)
    End Sub

    Protected Sub imgBtnUnreject_Click(sender As Object, e As EventArgs) Handles imgBtnUnreject.Click
        RaiseEvent UnrejectButton(sender, e)
    End Sub

    Private Sub imgBtnRevise_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgBtnRevise.Click
        RaiseEvent ReviseButton(sender, e)
    End Sub

#End Region

#Region "Private Method"

    Private Function IsViewMode() As Boolean
        Return CType(Me.Parent.Page, FormBase).CurrentFormMode = FormBaseEnum.FormMode.VIEW
    End Function

    Private Function IsDeleteMode() As Boolean
        Return CType(Me.Parent.Page, FormBase).CurrentFormMode = FormBaseEnum.FormMode.DELETE
    End Function

    Private Function IsAddMode() As Boolean
        Return CType(Me.Parent.Page, FormBase).CurrentFormMode = FormBaseEnum.FormMode.ADD
    End Function

    Private Sub SetButtonAccessRight()
        imgBtnNew.Visible = imgBtnNew.Visible And AccessRight.FLAG_ADD
        imgBtnDelete.Visible = imgBtnDelete.Visible And AccessRight.FLAG_DELETE And Not IsViewMode()
        imgBtnSave.Visible = imgBtnSave.Visible And (AccessRight.FLAG_UPDATE Or AccessRight.FLAG_ADD) And Not IsViewMode() And Not IsDeleteMode()
    End Sub

#End Region

End Class