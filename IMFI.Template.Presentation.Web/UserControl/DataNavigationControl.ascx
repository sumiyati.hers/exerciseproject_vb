﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DataNavigationControl.ascx.vb" Inherits="IMFI.Template.Presentation.Web.DataNavigationControl" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>



<table cellspacing="0" cellpadding="0" width="100%" style="text-align:center; margin-top:15px;" border="0">
    <tr>
		<td align="right">
            <dx:ASPxButton ID="imgBtnSubmit" Runat="server" CausesValidation="false" Text="SUBMIT" tabIndex="100" CssClass="Button True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm('Submit data ini?'); if (e.processOnServer) gridLoadingPanel.Show(); }"/>
            </dx:ASPxButton>
            <dx:ASPxButton ID="imgBtnUnsubmit" Runat="server" CausesValidation="false" Text="UNSUBMIT" tabIndex="100" CssClass="Button False" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm('Unsubmit data ini?'); if (e.processOnServer) gridLoadingPanel.Show(); }"/>
            </dx:ASPxButton>
            <dx:ASPxButton ID="imgBtnApprove" Runat="server" CausesValidation="false" Text="APPROVE" tabIndex="100" CssClass="Button True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm('Approve data ini?'); if (e.processOnServer) gridLoadingPanel.Show(); }"/>
            </dx:ASPxButton>
            <dx:ASPxButton ID="imgBtnUnapprove" Runat="server" CausesValidation="false" Text="UNAPPROVED" tabIndex="100" CssClass="Button False" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm('Unapprove data ini?'); if (e.processOnServer) gridLoadingPanel.Show(); }"/>
            </dx:ASPxButton>

            <dx:ASPxButton ID="imgBtnRevise" Runat="server" CausesValidation="false" Text="REVISE" tabIndex="100" CssClass="Button False" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="OnClickTab"></ClientSideEvents>
            </dx:ASPxButton>
            <dx:ASPxButton ID="imgBtnReject" Runat="server" CausesValidation="false" Text="REJECT" tabIndex="100" CssClass="Button False" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="OnClickTab"></ClientSideEvents>
            </dx:ASPxButton>
            <dx:ASPxButton ID="imgBtnUnreject" Runat="server" CausesValidation="false" Text="UNREJECT" tabIndex="100" CssClass="Button False" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm('Unreject data ini?'); if (e.processOnServer) gridLoadingPanel.Show(); }"/>
            </dx:ASPxButton>

            <dx:ASPxButton ID="imgBtnSave" Runat="server" CausesValidation="true" Text="SAVE" tabIndex="100" CssClass="Button True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <Border BorderWidth="0" />
                <ClientSideEvents Click="OnClick"></ClientSideEvents>
            </dx:ASPxButton>
            <dx:ASPxButton ID="imgBtnDelete" Runat="server" CausesValidation="false" Text="DELETE" tabIndex="100" CssClass="Button False" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <Border BorderWidth="0" />
                <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm('Delete data ini?'); if (e.processOnServer) gridLoadingPanel.Show(); }"/>
            </dx:ASPxButton>
            
            <dx:ASPxButton ID="imgBtnNew" Runat="server" CausesValidation="false" Text="NEW" tabIndex="100" CssClass="Button True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="OnClickTab"></ClientSideEvents>
            </dx:ASPxButton>
            <dx:ASPxButton ID="imgBtnClear" Runat="server" CausesValidation="false" Text="CLEAR" tabIndex="100" CssClass="Button True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="OnClickTab"></ClientSideEvents>
            </dx:ASPxButton>
            <dx:ASPxButton ID="imgBtnBack" Runat="server" CausesValidation="false" Text="BACK" tabIndex="100" CssClass="Button True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                <ClientSideEvents Click="OnClickTab"></ClientSideEvents>
            </dx:ASPxButton>
		</td>
	</tr>
</table>