﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SessionExpired.aspx.vb" Inherits="IMFI.Template.Presentation.Web.SessionExpired" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Title</title>
    <link rel="Shortcut Icon" href="~/Images/icon.ico" />

    <link rel="stylesheet" type="text/css" href="~/Styles/IMFI.css" />
</head>
<body class="ZBColor">
    <form id="form1" runat="server" onkeypress="checkKey()" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div style="position: relative; top: 300px;">
            <div id="WhiteContainer" class="WhiteContainer RadiusAll">
                <table id="tableNotification" cellspacing="20" border="0" width="100%" runat="server">
                    <tr>
                        <td align="center" valign="top" class="triangle-right">
                            <dx:ASPxLabel ID="txtExpiredSession" runat="server" Text="EXPIRED SESSION!" Font-Bold="true" ForeColor="#DD0000">
                            </dx:ASPxLabel>
                                            
                            <br /><br />
                                            
                            <dx:ASPxLabel ID="txtNotification" runat="server" Text="Session Anda telah habis!<br />Silahkan melakukan <b>login</b> kembali." EncodeHtml="false">
                            </dx:ASPxLabel>

                            <br /><br />

                            <dx:ASPxButton ID="imgBtnReLogin" Runat="server" CausesValidation="false" Text="LOGIN" tabIndex="100" CssClass="Button True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <dx:ASPxImage ID="imgWarning" runat="server" ShowLoadingImage="true" ImageUrl="~/Images/userSmaller.png" Height="50px">
                            </dx:ASPxImage>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div id="Footer" class="Footer">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <dx:ASPxLabel ID="txtCopyright" runat="server" Text="Copyright &copy 2017 PT. Indomobil Finance Indonesia" ForeColor="#FCFCFC" EncodeHtml="false">
                        </dx:ASPxLabel>
                    </td>
                    <td align="right">
                        <dx:ASPxLabel ID="txtBestViewed" runat="server" Text="Best viewed in Mozilla Firefox" ForeColor="#FCFCFC">
                        </dx:ASPxLabel>
                    </td>
                    <td width="20px">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>

    </form>
</body>
</html>