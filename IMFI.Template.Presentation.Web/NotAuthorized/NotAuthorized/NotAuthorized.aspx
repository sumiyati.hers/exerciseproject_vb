﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/IMFI.Master" CodeBehind="NotAuthorized.aspx.vb" Inherits="IMFI.Template.Presentation.Web.NotAuthorized1" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Main" runat="server" ContentPlaceHolderID="MainColumnContent">

    <div style="position: relative; top: 250px;">
        <div id="WhiteContainer" class="WhiteContainer RadiusAll">
            <table id="tableNotification" cellspacing="20" border="0" width="100%" runat="server">
                <tr>
                    <td align="center" valign="top" class="triangle-right">
                        <dx:ASPxLabel ID="txtExpiredSession" runat="server" Text="ACCESS DENIED!" Font-Bold="true" ForeColor="#DD0000">
                        </dx:ASPxLabel>
                                            
                        <br /><br />

                        <dx:ASPxLabel ID="txtNotification" runat="server" Text="Anda tidak memiliki hak untuk mengakses halaman ini!<br />Untuk informasi lebih lanjut, silahkan menghubungi <b>IT HelpDesk</b>." EncodeHtml="false">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <dx:ASPxImage ID="imgWarning" runat="server" ShowLoadingImage="true" ImageUrl="~/Images/userSmaller.png" Height="50px">
                        </dx:ASPxImage>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
</asp:Content>