﻿Public Class SessionExpired
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        imgBtnReLogin.CssPostfix = "Button True"
    End Sub
    
    Protected Sub imgBtnReLogin_Click(sender As Object, e As System.EventArgs) Handles imgBtnReLogin.Click
        Response.Redirect("~\Login.aspx")
    End Sub
End Class