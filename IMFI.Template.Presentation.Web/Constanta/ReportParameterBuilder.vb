﻿Imports IMFI.Template.ReportService

Public Class ReportParameterBuilder

#Region "Private Variable"

    Private _contentType As String
    Private _reportName As String
    Private _reportType As ReportType
    Private _parameterType As ParameterType
    Private _parameter As ArrayList

#End Region

#Region "Public Property"

    Public Property ContentType() As String
        Set(ByVal Value As String)
            _contentType = Value
        End Set
        Get
            Return _contentType
        End Get
    End Property

    Public ReadOnly Property ReportName() As String
        Get
            Return _reportName
        End Get
    End Property

    Public ReadOnly Property ReportType() As ReportType
        Get
            Return _reportType
        End Get
    End Property

    Public Property Parameter() As ArrayList
        Get
            Return _parameter
        End Get
        Set(ByVal Value As ArrayList)
            _parameter = Value
        End Set
    End Property

    Public ReadOnly Property ParameterType() As ParameterType
        Get
            Return _parameterType
        End Get
    End Property

#End Region

#Region "Constructor"

    Public Sub New(ByVal QueryString As NameValueCollection)
        _parameter = New ArrayList
        ConstructFromQueryString(QueryString)
    End Sub

    Public Sub New(ByVal ReportName As String, ByVal oParameterType As ParameterType)
        _contentType = String.Empty
        _reportName = ReportName
        _parameterType = oParameterType
        _parameter = New ArrayList
    End Sub

    'kia.imoet.2017.02.21
    Public Sub New(ByVal ReportName As String, ByVal oParameterType As ParameterType, ByVal ReportType As ReportType)
        _reportName = ReportName
        _reportType = ReportType
        _parameterType = oParameterType
        _parameter = New ArrayList
    End Sub

    Public Sub New(ByVal ReportName As String, ByVal ContentType As String, ByVal oParameterType As ParameterType)
        _contentType = ContentType
        _reportName = ReportName
        _parameterType = oParameterType
        _parameter = New ArrayList
    End Sub

#End Region

#Region "Public Function"

    Public Sub AddReportParameter(ByVal Value As Object)
        _parameter.Add(New Object() {Nothing, Value})
    End Sub

    Public Sub AddReportParameter(ByVal ParameterName As String, ByVal Value As Object)
        _parameter.Add(New Object() {ParameterName, Value})
    End Sub

    Public Function GetExportQueryString() As String
        Dim queryBuilder As New StringBuilder

        queryBuilder.AppendFormat("CT={0}", _contentType)
        queryBuilder.AppendFormat("&{0}", GetQueryString)

        Return queryBuilder.ToString()
    End Function

    'Public Function GetQueryString() As String
    '    Dim queryBuilder As New StringBuilder

    '    queryBuilder.AppendFormat("RN={0}", _reportName)
    '    If _parameterType = ParameterType.Index Then
    '        queryBuilder.Append("&PT=Index")
    '    Else
    '        queryBuilder.Append("&PT=Name")
    '    End If

    '    queryBuilder.AppendFormat("{0}", GetQueryParameter)

    '    Return queryBuilder.ToString()
    'End Function

    'kia.imoet.2017.02.21
    Public Function GetQueryString() As String
        Dim queryBuilder As New StringBuilder

        queryBuilder.AppendFormat("RN={0}", _reportName)
        If _parameterType = ParameterType.Index Then
            queryBuilder.Append("&PT=Index")
        Else
            queryBuilder.Append("&PT=Name")
        End If

        If _reportType = ReportType.PDF Then
            queryBuilder.Append("&RT=0")
        Else
            queryBuilder.Append("&RT=1")
        End If

        queryBuilder.AppendFormat("{0}", GetQueryParameter)

        Return queryBuilder.ToString()
    End Function

#End Region

#Region "Private method"

    Private Function GetQueryParameter() As String

        If _parameterType = ParameterType.Index Then
            Return GetQueryParameterByIndex()
        Else
            Return GetQueryParameterByName()
        End If
    End Function

    Private Function GetQueryParameterByIndex() As String
        Dim queryBuilder As New StringBuilder
        Dim index As Integer

        queryBuilder.Append("&PM=")

        For index = 0 To _parameter.Count() - 1
            queryBuilder.AppendFormat("{0};", _parameter(index)(1))
        Next

        Return queryBuilder.ToString()
    End Function

    Private Function GetQueryParameterByName() As String
        Dim queryBuilder As New StringBuilder
        Dim index As Integer

        For index = 0 To _parameter.Count() - 1
            queryBuilder.AppendFormat("&{0}={1}", _parameter(index)(0), _parameter(index)(1))
        Next

        Return queryBuilder.ToString()
    End Function

    'Private Function ConstructFromQueryString(ByVal QueryString As NameValueCollection) As Boolean
    '    If (IsUrlValid(QueryString)) Then
    '        _reportName = QueryString("RN")

    '        If QueryString("PT") = "Index" Then
    '            _parameterType = ParameterType.Index
    '        Else
    '            _parameterType = ParameterType.Name
    '        End If

    '        If Not IsNothing(QueryString("CT")) Then
    '            _contentType = QueryString("CT")
    '        End If

    '        If _parameterType = ParameterType.Index Then
    '            ConstructByIndex(QueryString)
    '        Else
    '            ConstructByName(QueryString)
    '        End If
    '    End If

    'End Function

    'Private Sub ConstructFromQueryString(ByVal QueryString As NameValueCollection)
    '    If (IsUrlValid(QueryString)) Then
    '        _reportName = QueryString("RN")

    '        If QueryString("PT") = "Index" Then
    '            _parameterType = ParameterType.Index
    '        Else
    '            _parameterType = ParameterType.Name
    '        End If

    '        If Not IsNothing(QueryString("CT")) Then
    '            _contentType = QueryString("CT")
    '        End If

    '        If _parameterType = ParameterType.Index Then
    '            ConstructByIndex(QueryString)
    '        Else
    '            ConstructByName(QueryString)
    '        End If
    '    End If

    'End Sub

    'kia.imoet.2017.02.21
    Private Sub ConstructFromQueryString(ByVal QueryString As NameValueCollection)
        If (IsUrlValid(QueryString)) Then
            _reportName = QueryString("RN")

            If QueryString("PT") = "Index" Then
                _parameterType = ParameterType.Index
            Else
                _parameterType = ParameterType.Name
            End If

            If Not IsNothing(QueryString("RT")) Then
                _reportType = QueryString("RT")
            End If

            If Not IsNothing(QueryString("CT")) Then
                _contentType = QueryString("CT")
            End If

            If _parameterType = ParameterType.Index Then
                ConstructByIndex(QueryString)
            Else
                ConstructByName(QueryString)
            End If
        End If
    End Sub

    Private Sub ConstructByIndex(ByVal QueryString As NameValueCollection)
        Dim strParam As String = QueryString("PM")
        Dim arrParam As Object() = strParam.Split(";")

        Dim oEnumerator As IEnumerator = arrParam.GetEnumerator

        While (oEnumerator.MoveNext)
            Me.AddReportParameter(oEnumerator.Current())
        End While
    End Sub

    Private Sub ConstructByName(ByVal QueryString As NameValueCollection)
        Dim indexParameter As Integer

        For indexParameter = 0 To QueryString.Count - 1
            If QueryString.GetKey(indexParameter) <> "PT" AndAlso _
                QueryString.GetKey(indexParameter) <> "CT" AndAlso _
                QueryString.GetKey(indexParameter) <> "RN" _
                Then

                Me.AddReportParameter( _
                    QueryString.GetKey(indexParameter), _
                    QueryString.GetValues(indexParameter)(0))
            End If
        Next
    End Sub

    Private Function IsUrlValid(ByVal QueryString As NameValueCollection) As Boolean
        If IsNothing(QueryString("RN")) Then
            Throw New Exception("Report Name Parameter is not provided.")
        End If

        'kia.imoet.2017.02.21
        If IsNothing(QueryString("RT")) Then
            Throw New Exception("Report Type is not provided.")
        End If

        If IsNothing(QueryString("PT")) Then
            Throw New Exception("Parameter Type is not provided.")
        End If

        Return True
    End Function

#End Region

End Class
