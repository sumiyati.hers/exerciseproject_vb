﻿Imports IMFI.Template.ReportService

Public Class ReportHelper

#Region "Private Variable & Constant"

    Private _reportBuilder As ReportParameterBuilder
    Private Const _reportPageLocation As String = "ReportPage.aspx"
    Private Const _reportExportLocation As String = "ReportExportContent.aspx"

#End Region

#Region "Public Properties"
    Public ReadOnly Property ReportParameter() As ReportParameterBuilder
        Get
            Return _reportBuilder
        End Get
    End Property

#End Region

#Region "Constructor"

    Public Sub New(ByVal QueryString As NameValueCollection)
        _reportBuilder = New ReportParameterBuilder(QueryString)
    End Sub

    Public Sub New(ByVal ReportName As String, ByVal oParameterType As ParameterType)
        _reportBuilder = New ReportParameterBuilder(ReportName, oParameterType)
    End Sub

    'kia.imoet.2017.02.21
    Public Sub New(ByVal ReportName As String, ByVal oParameterType As ParameterType, ByVal ReportType As ReportType)
        _reportBuilder = New ReportParameterBuilder(ReportName, oParameterType, ReportType)
    End Sub

    Public Sub New(ByVal ReportName As String, ByVal ContentType As String, ByVal oParameterType As ParameterType)
        _reportBuilder = New ReportParameterBuilder(ReportName, ContentType, oParameterType)
    End Sub

#End Region

#Region "Public Method"

    Public Function ReportLocation() As String
        Dim targetUrl As New StringBuilder

        targetUrl.Append("../../ReportEngine/ReportPage.aspx?")
        targetUrl.AppendFormat("{0}", _reportBuilder.GetQueryString)

        Return targetUrl.ToString()
    End Function

    Public Function ReportLocationUserControl() As String
        Dim targetUrl As New StringBuilder

        targetUrl.Append("../ReportEngine/ReportPage.aspx?")
        targetUrl.AppendFormat("{0}", _reportBuilder.GetQueryString)

        Return targetUrl.ToString()
    End Function

    Public Function ExportReportLocation() As String
        Dim targetUrl As New StringBuilder

        targetUrl.Append("ReportExportContent.aspx?")
        targetUrl.AppendFormat("{0}", _reportBuilder.GetExportQueryString)

        Return targetUrl.ToString()
    End Function

#End Region

End Class
