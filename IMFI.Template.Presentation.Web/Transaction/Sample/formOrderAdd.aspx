﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/IMFI.Master"
    CodeBehind="formOrderAdd.aspx.vb" Inherits="IMFI.Template.Presentation.Web.formOrderAdd" %>

<%@ Register Src="~/UserControl/AddControl.ascx" TagName="AddControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadColumnContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 85%;
        }
        .style4
        {
            width: 34%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainColumnContent" runat="server">
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td class="Header" colspan="6" align="center">
                TAMBAH DATA
            </td>
        </tr>
        <tr>
            <td class="FormCellLeftQ">
                Tanggal Order
            </td>
            <td class="FormCellLeftQ">
                <dx:ASPxDateEdit ID="TGL_ORDER" runat="server" Date="" DisplayFormatString="D" Width="">
                    <CalendarProperties>
                    </CalendarProperties>
                </dx:ASPxDateEdit>
            </td>
            <td class="FormCellLeftQ">
                Customer Id
            </td>
            <td class="FormCellLeftQ">
                <dx:ASPxComboBox ID="CUST_ID" runat="server" EnableCallbackMode="true" CallbackPageSize="10"
                    CssClass="combobox" ValueType="System.Int32" ValueField="custid" TextFormatString="{0} {1}"
                    Width="" DropDownStyle="DropDown">
                    <Columns>
                        <dx:ListBoxColumn FieldName="custid" />
                        <dx:ListBoxColumn FieldName="custname" />
                    </Columns>
                </dx:ASPxComboBox>
            </td>
            <td class="FormCellLeft">
                Keterangan
            </td>
            <td class="style1 FormCellRight">
                <dx:ASPxTextBox runat="server" ID="KETERANGAN" MaxLength="100" Style="margin-left: 0px">
                    <DisabledStyle BackColor="LightGray" ForeColor="Black">
                    </DisabledStyle>
                    <ReadOnlyStyle BackColor="LightGray">
                    </ReadOnlyStyle>
                </dx:ASPxTextBox>
            </td>
        </tr>
        <%--<tr>
            
           </tr>--%>
        <%--<tr>
                  
           </tr> --%>
    </table>
    <br />
    <dx:ASPxGridView ID="GridRow" ClientInstanceName="GridRow" runat="server" Width="100%"
        AutoGenerateColumns="False" KeyFieldName="ORDER_ID;ProductID">
        <Columns>
            <%--<dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="PILIH" VisibleIndex="1"
                Width="5%">
                <HeaderStyle HorizontalAlign="Center" />
                <FilterTemplate>
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center">
                                <dx:ASPxCheckBox ID="btSelectAll" runat="server">
                                    <ClientSideEvents CheckedChanged="function(s,e){ if(s.GetValue() == true ) { GridRow.SelectRows(); } else { GridRow.UnselectRows(); }}" />
                                </dx:ASPxCheckBox>
                            </td>
                        </tr>
                    </table>
                </FilterTemplate>
                <CellStyle>
                    <Paddings Padding="0" />
                </CellStyle>
            </dx:GridViewCommandColumn>--%>
            <dx:GridViewDataTextColumn FieldName="ORDER_ID" VisibleIndex="2" Caption="Order ID"
                Width="10%">
                <EditFormSettings Visible="False" />
                <CellStyle HorizontalAlign="left">
                </CellStyle>
                <HeaderStyle HorizontalAlign="center"></HeaderStyle>
                <Settings AutoFilterCondition="Contains" FilterMode="DisplayText"></Settings>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ProductID" VisibleIndex="2" Caption="Product"
                Width="10%">
                <EditFormSettings Visible="False" />
                <CellStyle HorizontalAlign="left">
                </CellStyle>
                <HeaderStyle HorizontalAlign="center"></HeaderStyle>
                <Settings AutoFilterCondition="Contains" FilterMode="DisplayText"></Settings>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="QTY" VisibleIndex="2" Caption="QTY" Width="10%">
                <EditFormSettings Visible="False" />
                <CellStyle HorizontalAlign="left">
                </CellStyle>
                <HeaderStyle HorizontalAlign="center"></HeaderStyle>
                <Settings AutoFilterCondition="Contains" FilterMode="DisplayText"></Settings>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="PRICE" VisibleIndex="2" Caption="PRICE" Width="10%">
                <EditFormSettings Visible="False" />
                <CellStyle HorizontalAlign="left">
                </CellStyle>
                <HeaderStyle HorizontalAlign="center"></HeaderStyle>
                <Settings AutoFilterCondition="Contains" FilterMode="DisplayText"></Settings>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Templates>
        </Templates>
        <Settings ShowFilterRow="True" />
        <SettingsPager PageSize="10" Position="Bottom" />
        <Styles>
            <AlternatingRow Enabled="True">
            </AlternatingRow>
        </Styles>
        <StylesPager>
            <CurrentPageNumber BackColor="#57452d" ForeColor="White" CssClass="MyPageNumber">
                <Paddings PaddingTop="5px" PaddingBottom="5px" PaddingLeft="9px" PaddingRight="9px" />
            </CurrentPageNumber>
        </StylesPager>
    </dx:ASPxGridView>
    <br />

    <table cellspacing="0" cellpadding="0" border="0" width="50%">
        <tr>
            <td class="FormCellLeftQ">
                Product Id
            </td>
            <td class="FormCellLeftQ">
                <dx:ASPxComboBox ID="ProductID" runat="server" EnableCallbackMode="true" CallbackPageSize="10"
                    CssClass="combobox" ValueType="System.String" ValueField="ProductID" TextFormatString="{0} {1}"
                    Width="" DropDownStyle="DropDown">
                   <%-- <Columns>
                        <dx:ListBoxColumn FieldName="ProductID" />
                        <dx:ListBoxColumn FieldName="ProductName" />
                    </Columns>--%>
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td class="FormCellLeftQ">
                QTY
            </td>
            <td class="FormCellLeftQ">
                <dx:ASPxTextBox runat="server" ID="QTY" MaxLength="100" Style="margin-left: 0px">
                    <DisabledStyle BackColor="LightGray" ForeColor="Black">
                    </DisabledStyle>
                    <ReadOnlyStyle BackColor="LightGray">
                    </ReadOnlyStyle>
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="FormCellLeftQ">
                PRICE
            </td>
            <td class="FormCellLeftQ">
                <dx:ASPxTextBox runat="server" ID="PRICE" MaxLength="100" Style="margin-left: 0px">
                    <DisabledStyle BackColor="LightGray" ForeColor="Black">
                    </DisabledStyle>
                    <ReadOnlyStyle BackColor="LightGray">
                    </ReadOnlyStyle>
                </dx:ASPxTextBox>
            </td>
        </tr>
    </table>
    <%--<ucdatanav:DataNavigationControl ID="DataNavigationControl1" IsDefault ="true" runat="server" />--%>
    <%-- <tr>
        <td>
            <uc1:AddControl ID="AddControl" runat="server" />
        </td>
    </tr>--%>
    <ucdatanav:DataNavigationControl ID="DataNavigationControl1" IsDefault="true" runat="server" />
</asp:Content>
