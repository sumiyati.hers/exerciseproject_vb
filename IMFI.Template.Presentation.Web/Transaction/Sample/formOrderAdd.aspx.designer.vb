﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class formOrderAdd

    '''<summary>
    '''TGL_ORDER control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TGL_ORDER As Global.DevExpress.Web.ASPxDateEdit

    '''<summary>
    '''CUST_ID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CUST_ID As Global.DevExpress.Web.ASPxComboBox

    '''<summary>
    '''KETERANGAN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents KETERANGAN As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''GridRow control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridRow As Global.DevExpress.Web.ASPxGridView

    '''<summary>
    '''ProductID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ProductID As Global.DevExpress.Web.ASPxComboBox

    '''<summary>
    '''QTY control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents QTY As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''PRICE control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PRICE As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''DataNavigationControl1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DataNavigationControl1 As Global.IMFI.Template.Presentation.Web.DataNavigationControl
End Class
