﻿Imports IMFI.Template.Common
Imports IMFI.Template.DomainObjects
Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports IMFI.Template.BusinessFacade


Partial Class frmOrder
    Inherits FormBase

    Private Property ses_ddltest As List(Of MST_KONSUMEN)
        Get
            Return PageSession("ses_ddltest")
        End Get
        Set(value As List(Of MST_KONSUMEN))
            PageSession("ses_ddltest", value)
        End Set
    End Property

    Private Property dataOrderHDR As List(Of ORDER_HDR)
        Get
            Return PageSession("dataOrderHDR")
        End Get
        Set(value As List(Of ORDER_HDR))
            PageSession("dataOrderHDR", value)
        End Set
    End Property

#Region "PAGE"

    Private Sub frmOrder_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Me.PageGroupId = ScreenGroupId.DASHBOARD

        Try
            Dim linkOrder As ASPxHyperLink = sender
            Dim grid As ASPxGridView = DirectCast(linkOrder.NamingContainer, DevExpress.Web.GridViewDataItemTemplateContainer).Grid
            Dim rowIndex As Integer = DirectCast(linkOrder.NamingContainer, DevExpress.Web.GridViewDataItemTemplateContainer).VisibleIndex
            Dim ORDER_ID As Int64 = DirectCast(linkOrder.NamingContainer, DevExpress.Web.GridViewDataItemTemplateContainer).KeyValue
            Dim id_order As String = grid.GetRowValues(rowIndex, "id_order")

            linkOrder.Text = id_order

            Dim oQueryStringBuilder As New QueryStringBuilder

            'oQueryStringBuilder.AddQS(QueryStringConstant.SAVE_MODE, ORDER_ID)
            oQueryStringBuilder.AddQS(QueryStringConstant.CLEAR_SESSION, "TRUE")
            'oQueryStringBuilder.AddQS(QueryStringConstant.SAVE_MODE, "~/Transaction/Sample/frmOrder.aspx")

            linkOrder.NavigateUrl = oQueryStringBuilder.ToQueryString("formOrderAdd.aspx")





        Catch ex As Exception

        End Try


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then


            LoadData()

            LoadDataddltest()
            BindDataddtest()

        End If
        BindData()
        'BindDataKonsumen()


    End Sub

#End Region


#Region "METHOD"
    Private Sub BindDataddtest()
        'GridRow.DataSource = (New SPService.GL_ACC_DESC).RetrieveWithCondition(Nothing, "ACC_NO ASC", (New ObjectTransporter(New TGL_ACC_DESC, (New UserIdentification).GetUserCredential)))
        ddltest.DataSource = ses_ddltest

        ddltest.DataBind()
    End Sub

    'Public Sub BindDataKonsumen()
    '    Dim a As ArrayList = (New MST_KONSUMENBusinessFacade).RetrieveListPaging(-1, 0, "CustId ASC", (New DomainObjects.ObjectTransporter(Nothing, Nothing)), Nothing, 0)
    '    CustID.Item.Clear()


    '    For i As Integer = 0 To a.Count - 1
    '        CustID.Items.Add(New ListEditItem(CType(a(i), MST_KONSUMEN).CustId.ToString & " - " & CType(a(i), MST_KONSUMEN).CustName.ToString, CType(a(i), MST_KONSUMEN).CustId.ToString))
    '    Next

    'End Sub

    Private Sub LoadDataddltest()
        ses_ddltest = Nothing
        ses_ddltest = (New MST_KONSUMENBusinessFacade).RetrieveListPaging(-1, 0, "custid ASC", (New DomainObjects.ObjectTransporter(New MST_KONSUMEN, (New UserIdentification).GetUserCredential)), Nothing, 0).Cast(Of MST_KONSUMEN).ToList()

    End Sub

    Private Sub LoadData()


        dataOrderHDR = (New ORDER_HDRBusinessFacade).RetrieveListPaging(-1, 0, "ORDER_ID ASC", (New DomainObjects.ObjectTransporter(New ORDER_HDR, (New UserIdentification).GetUserCredential)), Nothing, 0).Cast(Of ORDER_HDR).ToList()

       

    End Sub

    Private Sub BindData()
        'GridRow.DataSource = (New SPService.GL_ACC_DESC).RetrieveWithCondition(Nothing, "ACC_NO ASC", (New ObjectTransporter(New TGL_ACC_DESC, (New UserIdentification).GetUserCredential)))
        GridRow.DataSource = dataOrderHDR

        GridRow.DataBind()
    End Sub

#End Region

    Private Sub GridRow_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridViewEditorEventArgs) Handles GridRow.CellEditorInitialize
        e.Editor.ReadOnly = False
        e.Editor.ReadOnly = False
        CType(e.Editor, ASPxEdit).ValidationSettings.Display = Display.Dynamic

        Select Case e.Column.FieldName
            Case "CustId"
                Dim cmb As ASPxComboBox = TryCast(e.Editor, ASPxComboBox)

                e.Editor.ReadOnly = False

                For i As Integer = 0 To ses_ddltest.Count - 1
                    cmb.Items.Add(CType(ses_ddltest(i), MST_KONSUMEN).CustName, CType(ses_ddltest(i), MST_KONSUMEN).CustId)
                Next


        End Select


    End Sub

    Private Sub GridRow_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles GridRow.CustomColumnDisplayText
        If e.Column.FieldName = "CustId" Then

            Try
                Dim oResult As ArrayList

                oResult = (New MST_KONSUMENBusinessFacade).RetrieveListPaging(-1, 0, "CUSTID ASC", (New DomainObjects.ObjectTransporter(New MST_KONSUMEN, (New UserIdentification).GetUserCredential)), "CUSTID='" & e.Value & "'", 0)

                e.DisplayText = CType(oResult(0), MST_KONSUMEN).CustName

            Catch ex As Exception

            End Try

        End If
    End Sub

    Private Sub GridRow_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles GridRow.RowDeleting
        Try
            Dim oRow = New ORDER_HDR
            oRow.ORDER_ID = e.Keys("ORDER_ID")
            Dim oTDelete As ObjectTransporter = New ObjectTransporter(oRow, (New UserIdentification).GetUserCredential)
            Dim oResult As Object = (New ORDER_HDRBusinessFacade).Delete(oTDelete)

            e.Cancel = True
            CType(sender, ASPxGridView).CancelEdit()

            LoadData()
            BindData()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub GridRow_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles GridRow.RowInserting
        Try
            Dim oResult As Object
            Dim oRow = New ORDER_HDR

            oRow.ORDER_ID = e.NewValues("ORDER_ID")
            oRow.ORDER_DATE = e.NewValues("ORDER_DATE")
            oRow.CustId = e.NewValues("CustId")
            oRow.ALASAN_ORDER = e.NewValues("ALASAN_ORDER")

            oResult = (New ORDER_HDRBusinessFacade).Create(New ObjectTransporter(oRow, New UserIdentification().GetUserCredential()))

            e.Cancel = True
            CType(sender, ASPxGridView).CancelEdit()

            LoadData()
            BindData()
        Catch ex As Exception

        End Try
    End Sub

    'ADD Button
    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim oResult As Object
        Dim oRow = New MST_KONSUMEN

        oRow.CustId = 222
        oRow.CustName = "asih"


        oResult = (New MST_KONSUMENBusinessFacade).Create(New ObjectTransporter(oRow, New UserIdentification().GetUserCredential()))

        'ses_ddltest = LoadDataddltest()
        BindDataddtest()
    End Sub

    'EDIT BUTTON
    'Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
    '    Dim oRow_Retrieve As New MST_KONSUMEN
    '    Dim oRow As New MST_KONSUMEN
    '    Dim oResult As Object
    '    oRow_Retrieve = (New MST_KONSUMENBusinessFacade).Retrieve(New ObjectTransporter(New MST_KONSUMEN(5), New UserIdentification().GetUserCredential()))
    '    oRow.CustId = oRow_Retrieve.CustId

    '    oRow.CustName = "ccccc"


    '    oResult = (New MST_KONSUMENBusinessFacade).Update(New ObjectTransporter(oRow, New UserIdentification().GetUserCredential()))

    '    ses_ddltest = LoadDataddltest()
    '    BindDataddtest()
    'End Sub

    'DELETE BUTTON
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim oResult As Object
        Dim oRow As MST_KONSUMEN = (New MST_KONSUMENBusinessFacade).Retrieve(New ObjectTransporter(New MST_KONSUMEN(4), New UserIdentification().GetUserCredential()))

        oResult = (New MST_KONSUMENBusinessFacade).Delete(New ObjectTransporter(oRow, New UserIdentification().GetUserCredential()))

        If oResult.Tag = -1 Then
            Throw New Exception(oResult.Tag)
        Else
            'ses_ddltest = LoadDataddltest()
            BindDataddtest()
        End If

    End Sub

    Private Sub AddControl_AddButton(ByVal sender As Object, ByVal e As System.EventArgs) Handles AddControl.AddButton
        Dim oQueryStringBuilder As New QueryStringBuilder

        oQueryStringBuilder.AddQSFormMode(FormBaseEnum.FormMode.ADD)
        oQueryStringBuilder.AddQS(QueryStringConstant.CLEAR_SESSION, "TRUE")

        Response.Redirect(oQueryStringBuilder.ToQueryString("formOrderAdd.aspx"))
    End Sub

    Private Sub GridRow_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridRow.RowUpdating
        Try
            Dim oResult As Object
            Dim oRow As New ORDER_HDR

            oRow.ORDER_ID = e.Keys("ORDER_ID")
            oRow.ORDER_DATE = e.NewValues("ORDER_DATE")
            oRow.CustId = e.NewValues("CustId")
            oRow.ALASAN_ORDER = e.NewValues("ALASAN_ORDER")

            oResult = (New ORDER_HDRBusinessFacade).Update(New ObjectTransporter(oRow, New UserIdentification().GetUserCredential()))

            e.Cancel = True
            CType(sender, ASPxGridView).CancelEdit()

            LoadData()
            BindData()

        Catch ex As Exception

        End Try
    End Sub
End Class