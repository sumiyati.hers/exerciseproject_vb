﻿Imports IMFI.Template.Common
Imports IMFI.Template.DomainObjects
Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports IMFI.Template.BusinessFacade


Public Class formOrderAdd
    Inherits FormBase

    Private m_orderDtl As List(Of ORDER_DTL)
    Private m_konsumen As List(Of MST_KONSUMEN)
    Private m_product As List(Of MST_PRODUCT)
    Private m_ORDER_HDR As String

    Public Property ses_orderDtl As List(Of ORDER_DTL)
        Get
            Return m_orderDtl
        End Get

        Set(ByVal ses_orderDtl As List(Of ORDER_DTL))

            m_orderDtl = ses_orderDtl

        End Set
    End Property

    Public Property ses_konsumen As List(Of MST_KONSUMEN)
        Get
            Return m_konsumen
        End Get

        Set(ByVal ses_konsumen As List(Of MST_KONSUMEN))

            m_konsumen = ses_konsumen
        End Set
    End Property

    Public Property ses_produk As List(Of MST_PRODUCT)
        Get
            Return m_product
        End Get

        Set(ByVal ses_produk As List(Of MST_PRODUCT))
            m_product = ses_produk
        End Set
    End Property

    'Private ReadOnly Property ORDER_HDR_OBJECT_HOLDER() As ORDER_HDR
    '    Get
    '        If CurrentFormMode = FormBaseEnum.FormMode.ADD Then
    '            If IsNothing(Me.PageGroupSession) Then
    '                Me.PageGroupSession = New ORDER_HDR
    '            End If
    '        Else
    '            If IsNothing(Me.PageGroupSession) Then
    '                Me.PageGroupSession = CType((New ORDER_HDRBusinessFacade).Retrieve(New DomainObjects.ObjectTransporter(New ORDER_HDR((New UserIdentification).CurrentBranchCode, m_ORDER_HDR), New UserIdentification().GetUserCredential())), ORDER_HDR)
    '            End If
    '        End If
    '        Return CType(Me.PageGroupSession, ORDER_HDR)
    '    End Get
    'End Property

    'Private ReadOnly Property ORDER_DTL_OBJECT_HOLDER() As ORDER_DTL
    '    Get
    '        If CurrentFormMode = FormBaseEnum.FormMode.ADD Then
    '            If IsNothing(Me.PageGroupSession) Then
    '                Me.PageGroupSession = New ORDER_DTL
    '            End If
    '        Else
    '            If IsNothing(Me.PageGroupSession) Then
    '                Me.PageGroupSession = CType((New ORDER_DTLBusinessFacade).Retrieve(New DomainObjects.ObjectTransporter(New ORDER_DTL((New UserIdentification).CurrentBranchCode, m_ORDER_HDR), New UserIdentification().GetUserCredential())), ORDER_DTL)
    '            End If
    '        End If
    '        Return CType(Me.PageGroupSession, ORDER_DTL)
    '    End Get
    'End Property

    'Private ReadOnly Property SCREEN_OBJECT(ByVal status As String, ByVal Switch As Boolean) As ORDER_HDR
    '    Get
    '        Dim oOrderHeader As ORDER_HDR = ORDER_HDR_OBJECT_HOLDER
    '        'Dim oOrderDtl As ORDER_DTL = ORDER_DTL_OBJECT_HOLDER


    '        oOrderHeader.ORDER_DATE = ORDER_DATE.Text
    '        oOrderHeader.CustId = custId.Text
    '        oOrderHeader.ALASAN_ORDER = ALASAN_ORDER.Text

    '        'oOrderDtl.ProductID = ProductID.Text
    '        'oOrderDtl.QTY = QTY.Text
    '        'oOrderDtl.PRICE = PRICE.Text

    '        Return oOrderHeader
    '    End Get
    'End Property

#Region "PAGE"

    Private Sub formOrderAdd_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Me.PageGroupId = ScreenGroupId.DASHBOARD
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ClearPageGroupSession()
            ses_orderDtl = Nothing
            ses_orderDtl = LoadData()

            ses_konsumen = Nothing
            ses_konsumen = LoadDataKonsumen()
            BindDataKonsumen()

            'ses_produk = Nothing
            'ses_produk = LoadDataProduk()
            'BindDataProduk()

            BindDataProdukV2()
        End If
        BindData()
        SetControlVisible()
    End Sub

#End Region

#Region "METHOD"

    Private Sub BindDataProduk()
        ProductID.DataSource = ses_produk
        ProductID.DataBind()
    End Sub

    Public Sub BindDataProdukV2()
        Dim a As ArrayList = (New MST_PRODUCTBusinessFacade).RetrieveListPaging(-1, 0, "ProductID ASC", (New DomainObjects.ObjectTransporter(Nothing, Nothing)), Nothing, 0)
        ProductID.Items.Clear()

        For i As Integer = 0 To a.Count - 1
            ProductID.Items.Add(New ListEditItem(CType(a(i), MST_PRODUCT).ProductID.ToString & " - " & CType(a(i), MST_PRODUCT).ProductName.ToString, CType(a(i), MST_PRODUCT).ProductID.ToString))
        Next

    End Sub

    'Private Function LoadDataProduk() As List(Of MST_PRODUCT)
    '    Dim oArray As New List(Of MST_PRODUCT)
    '    Dim oResult As ArrayList

    '    oResult = (New MST_PRODUCTBusinessFacade).RetrieveListPaging(-1, 0, "ProductID ASC", (New DomainObjects.ObjectTransporter(New MST_PRODUCT, (New UserIdentification).GetUserCredential)), Nothing, 0)

    '    For index = 0 To oResult.Count - 1
    '        oArray.Add(oResult(index))
    '    Next

    '    Return oArray
    'End Function

    Private Sub BindDataKonsumen()

        CUST_ID.DataSource = ses_konsumen

        CUST_ID.DataBind()
    End Sub

    Private Function LoadDataKonsumen() As List(Of MST_KONSUMEN)
        Dim oArray As New List(Of MST_KONSUMEN)
        Dim oResult As ArrayList

        oResult = (New MST_KONSUMENBusinessFacade).RetrieveListPaging(-1, 0, "custid ASC", (New DomainObjects.ObjectTransporter(New MST_KONSUMEN, (New UserIdentification).GetUserCredential)), Nothing, 0)

        For index = 0 To oResult.Count - 1
            oArray.Add(oResult(index))
        Next

        Return oArray
    End Function

    Private Sub BindData()
        GridRow.DataSource = ses_orderDtl

        GridRow.DataBind()
    End Sub

    Private Function LoadData() As List(Of ORDER_DTL)
        Dim oArray As New List(Of ORDER_DTL)
        Dim oResult As ArrayList

        oResult = (New ORDER_DTLBusinessFacade).RetrieveListPaging(-1, 0, "ORDER_ID ASC", (New DomainObjects.ObjectTransporter(New ORDER_DTL, (New UserIdentification).GetUserCredential)), Nothing, 0)

        For index = 0 To oResult.Count - 1
            oArray.Add(oResult(index))
        Next

        Return oArray

    End Function
#End Region


    Private Sub GridRow_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles GridRow.CustomColumnDisplayText
        If e.Column.FieldName = "ProductID" Then

            Try
                Dim oResult As ArrayList

                oResult = (New MST_PRODUCTBusinessFacade).RetrieveListPaging(-1, 0, "ProductID ASC", (New DomainObjects.ObjectTransporter(New MST_PRODUCT, (New UserIdentification).GetUserCredential)), "ProductID='" & e.Value & "'", 0)

                e.DisplayText = CType(oResult(0), MST_PRODUCT).ProductName

            Catch ex As Exception

            End Try
        End If
    End Sub



    Private Sub SetControlVisible()

        DataNavigationControl1.ShowClearButton = False
        DataNavigationControl1.ShowUnapproveButton = False
        DataNavigationControl1.ShowApproveButton = False
        DataNavigationControl1.ShowReviseButton = False
        DataNavigationControl1.ShowRejectButton = False
        DataNavigationControl1.ShowUnsubmitButton = False
        DataNavigationControl1.ShowSubmitButton = False
        DataNavigationControl1.ShowSaveButton = True
        DataNavigationControl1.ShowDeleteButton = False
        DataNavigationControl1.ShowSubmitButton = False
        DataNavigationControl1.ShowNewButton = False
        DataNavigationControl1.ShowBackButton = True
    End Sub
    Private Sub DataNavigationControl1_BackButton(sender As Object, e As System.EventArgs) Handles DataNavigationControl1.BackButton
        ClearPageGroupSession()
        Response.Redirect("frmOrder.aspx")
    End Sub

    Private Sub DataNavigationControl1_ClearButton(sender As Object, e As System.EventArgs) Handles DataNavigationControl1.ClearButton


    End Sub

    Private Sub DataNavigationControl1_SaveAddButton(Sender As Object, e As ExecuteEventArgs) Handles DataNavigationControl1.SaveAddButton
        Dim oResult As Object
        Dim oOrderHDR As New ORDER_HDR
        oOrderHDR.ORDER_DATE = TGL_ORDER.Value
        oOrderHDR.CustId = CUST_ID.Value
        oOrderHDR.ALASAN_ORDER = KETERANGAN.Value

        oResult = (New ORDER_HDRBusinessFacade).Create(New ObjectTransporter(oOrderHDR, New UserIdentification().GetUserCredential()))

        LoadData()
        BindData()

    End Sub
End Class