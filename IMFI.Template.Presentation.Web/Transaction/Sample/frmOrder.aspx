﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/IMFI.Master"
    CodeBehind="frmOrder.aspx.vb" Inherits="IMFI.Template.Presentation.Web.frmOrder" %>

<%@ Register Src="~/UserControl/AddControl.ascx" TagName="AddControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadColumnContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainColumnContent" runat="server">
    <asp:Button ID="Button2" runat="server" Text="Add" Style="background-color: Teal;
        color: White;" />
    &nbsp;
    <asp:Button ID="Button1" runat="server" Text="EDIT" Style="background-color: Teal;
        color: White;" />
    &nbsp;
    <asp:Button ID="Button3" runat="server" Text="DELETE" Style="background-color: Teal;
        color: White;" />
    <br />
    <script language="JavaScript" type="text/javascript">
    </script>
    <style type="text/css">
    
    .frmEdit
    {
        border: 1px SOLID #c0c0c0;
        border-radius: 4px 4px 4px 4px;
        background-color: White;
        width: 35%;
        margin: auto;
        padding 0px;
    }
    
    
    .frm-item-group
    {
        padding: 0px;
    }
    
    .Header-Form
    {
        background-color: #015989;
    }
    .Button-Custom
        {
            cursor: pointer;
            color: #FCFCFC;
            height: 30px;
            width: 90px;
        }
        .cst-button
        {
            margin-right: 3px;
        }
    
    
    
    </style>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td class="Header" align="center">
                List Order
            </td>
            <%-- <dx:ASPxCalendar runat="server" ID="calendar">
   
    </dx:ASPxCalendar>--%>
            <br />
            <br />
            <dx:ASPxComboBox ID="ddltest" runat="server" EnableCallbackMode="true" CallbackPageSize="10"
                CssClass="combobox" ValueType="System.Int32" ValueField="custid" TextFormatString="{0} {1}"
                Width="287px" DropDownStyle="DropDown" Caption="Nama konsumen">
                <Columns>
                    <dx:ListBoxColumn FieldName="custid" />
                    <dx:ListBoxColumn FieldName="custname" />
                </Columns>
            </dx:ASPxComboBox>
        </tr>
        <tr>
            <td>
                <dx:ASPxGridView ID="GridRow" runat="server" Width="100%" AutoGenerateColumns="False"
                    KeyFieldName="ORDER_ID">
                    <Columns>
                        <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowEditButton="true" ShowDeleteButton="true"
                            VisibleIndex="0" />
                        <dx:GridViewDataTextColumn FieldName="ORDER_ID" Caption="Order ID" VisibleIndex="1" Name="id_order">
                            <CellStyle HorizontalAlign="center">
                            </CellStyle>
                            <HeaderStyle HorizontalAlign="center"></HeaderStyle>
                             <DataItemTemplate>
                                <dx:ASPxHyperLink ID="linkOrder" runat="server" Text="">
                                </dx:ASPxHyperLink>
                            </DataItemTemplate> 

                            <EditFormSettings VisibleIndex="0" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn FieldName="ORDER_DATE" Caption="Tanggal Order" VisibleIndex="1" DisplayFormatString = "D">
                            <CellStyle HorizontalAlign="center">
                            </CellStyle>
                            <HeaderStyle HorizontalAlign="center"></HeaderStyle>
                            <EditFormSettings VisibleIndex="1" />
                            
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn FieldName="ALASAN_ORDER" Caption="Alasan Order" VisibleIndex="1">
                            <CellStyle HorizontalAlign="center">
                            </CellStyle>
                            <HeaderStyle HorizontalAlign="center"></HeaderStyle>
                            <EditFormSettings VisibleIndex="2" />
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataComboBoxColumn FieldName="CustId" Caption="Customer ID" VisibleIndex="3">
                          
                            <EditFormSettings VisibleIndex="3" />
                            <PropertiesComboBox>
                                
                                <ReadOnlyStyle BackColor="Transparent" ForeColor="Black">
                                    <BorderTop BorderStyle="None" />
                                    <BorderBottom BorderStyle="None" />
                                    <BorderLeft BorderStyle="None" />
                                    <BorderRight BorderStyle="None" />
                                </ReadOnlyStyle>
                            </PropertiesComboBox>
                          
                        </dx:GridViewDataComboBoxColumn>
                       
                        <dx:GridViewDataTextColumn FieldName="STATUS_ORDER" VisibleIndex="3" Caption="STATUS"
                            Width="10%">
                            <CellStyle HorizontalAlign="center">
                            </CellStyle>
                            <HeaderStyle HorizontalAlign="center"></HeaderStyle>
                            <Settings AutoFilterCondition="Contains" FilterMode="DisplayText"></Settings>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Templates>
                        <PagerBar>
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td>
                                        <dx:ASPxGridViewTemplateReplacement ID="MainPager" runat="server" ReplacementType="Pager" />
                                    </td>
                                    <td width="60px">
                                        <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" AutoPostBack="false" RenderMode="Link"
                                            BackColor="Black" ForeColor="White">
                                            <%--            <Image IconID="print_printer_16x16" ToolTip="Print"></Image>--%>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </PagerBar>
                    </Templates>
                    <Settings ShowFilterRow="True" ShowFooter="true" />
                    
                    <SettingsPager PageSize="10" Position="Bottom" />
                    <Styles>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                    <StylesPager Button-Width="50">
                        <CurrentPageNumber BackColor="#57452d" ForeColor="White" CssClass="MyPageNumber">
                            <Paddings PaddingTop="20px" PaddingBottom="20px" PaddingLeft="60px" PaddingRight="60px" />
                        </CurrentPageNumber>
                    </StylesPager>
                    <EditFormLayoutProperties ColCount="1" Styles-LayoutItem-Caption-Font-Bold="true"
                        Styles-LayoutGroup-CssClass="frmEdit">
                        <Styles LayoutGroup-Cell-CssClass="frm-item-group" />
                        <Items>
                            <dx:EmptyLayoutItem ParentContainerStyle-Paddings-PaddingBottom="10px" CssClass="Header-Form">
                            </dx:EmptyLayoutItem>
                            <dx:GridViewColumnLayoutItem ColumnName="ORDER_DATE" />
                            <dx:GridViewColumnLayoutItem ColumnName="ALASAN_ORDER" />
                            <dx:GridViewColumnLayoutItem ColumnName="CustId" />
                            <dx:EditModeCommandLayoutItem Width="100%" HorizontalAlign="Center" Paddings-PaddingTop="10px"
                                Paddings-PaddingBottom="15px" />
                        </Items>
                    </EditFormLayoutProperties>
                    <SettingsCommandButton>
 
                        <UpdateButton Text="Save" RenderMode="Button">
                            <Styles CssPostfix="Button">
                                <Style CssClass="Button-Custom True cst-button"></Style>
                            </Styles>
                        </UpdateButton>
                        <CancelButton RenderMode="Button">
                            <Styles CssPostfix="Button">
                                <Style CssClass="Button-Custom False"></Style>
                            </Styles>
                        </CancelButton>
                    </SettingsCommandButton>
                    <SettingsBehavior ConfirmDelete="true"></SettingsBehavior>
                    <SettingsText ConfirmDelete="Delete data ini?" />
                    <EditFormLayoutProperties>
                        <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="600" />
                    </EditFormLayoutProperties>
                </dx:ASPxGridView>
                <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="GridRow">
                </dx:ASPxGridViewExporter>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:AddControl ID="AddControl" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
