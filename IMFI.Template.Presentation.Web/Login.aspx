﻿<%@ Page Language="vb" EnableTheming="false" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="IMFI.Template.Presentation.Web.Login" %>

<%@ Register Assembly="DevExpress.Web.v16.2, Version=16.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Title</title>
    <link rel="Shortcut Icon" href="~/Images/icon.ico" />

    <link rel="stylesheet" type="text/css" href="~/Styles/IMFI.css" />

    <script language="JavaScript" type="text/javascript">
        function HideLoadingPanel() {
            gridLoadingPanel.Hide();
        }

        function IfLoadingPanel(s, e) {
            if ((e.processOnServer) && s.GetValue() != null)
                gridLoadingPanel.Show();
            else
                gridLoadingPanel.Hide();
        }

        var isPostbackInitiated = false;

        function OnClick(s, e) {
            if (!ASPxClientEdit.ValidateEditorsInContainer(null))
                return;

            e.processOnServer = !isPostbackInitiated;
            isPostbackInitiated = true;

            if (e.processOnServer)
                gridLoadingPanel.Show();
        }

        function OnClickTab(s, e) {
            e.processOnServer = !isPostbackInitiated;
            isPostbackInitiated = true;

            if (e.processOnServer)
                gridLoadingPanel.Show();
        }
    </script>
</head>
<body class="ThemeColor">
    <form id="form1" runat="server" onkeypress="checkKey()" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <dx:ASPxLoadingPanel ID="gridLoadingPanel" ClientInstanceName="gridLoadingPanel" runat="server" Modal="true" CssClass="triangle-custom" Theme="Default">
            <LoadingDivStyle BackColor="Black" Opacity="35"></LoadingDivStyle>
            <Border BorderStyle="None" />
        </dx:ASPxLoadingPanel>
        
        <div style="position: relative; top: 300px;">
            <div id="WhiteContainer" class="WhiteContainer RadiusAll">
                <table cellpadding="1px" cellspacing="0px" border="0" width="300px">
                    <tr>
                        <td align="center" valign="middle" colspan="2">
                            <dx:ASPxImage ID="imgLogo" runat="server" ShowLoadingImage="true" ImageUrl="~/Images/logo.png">
                            </dx:ASPxImage>
                        </td>
                    </tr>
                    <tr>
                        <td class="TextBoxPaddingLeft RadiusTopLeft">
                            <dx:ASPxLabel ID="txtNoteID" runat="server" Text="ID">
                            </dx:ASPxLabel>
                        </td>
                        <td class="TextBoxPaddingRight RadiusTopRight">                            
                            <dx:ASPxTextBox ID="txtUserID" runat="server" Width="100%" MaxLength="8" EnableTheming="true" AutoPostBack="false">
                                <Border BorderStyle="None" />
                                <DisabledStyle BackColor="Transparent" ForeColor="ControlDarkDark" Border-BorderStyle="None">
                                </DisabledStyle>
                                <ValidationSettings CausesValidation="false" Display="Static" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="true" ErrorText="Username tidak boleh dikosongkan."></RequiredField>
                                </ValidationSettings>
                                <%--<ClientSideEvents Init="HideLoadingPanel" ValueChanged="IfLoadingPanel"></ClientSideEvents>--%>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="TextBoxPaddingLeft">
                            <dx:ASPxLabel ID="txtNoteBranch" runat="server" Text="Branch">
                            </dx:ASPxLabel>
                        </td>
                        <td class="TextBoxPaddingRight" align="center">
                            <dx:ASPxComboBox ID="txtBranch" runat="server" ValueType="System.String"
                                IncrementalFilteringMode="Contains" DropDownStyle="DropDownList" Width="100%" EnableTheming="true" ClientEnabled="true">
                                <Border BorderStyle="None" />
                                <DisabledStyle BackColor="Transparent" ForeColor="Black" Border-BorderStyle="None">
                                </DisabledStyle>
                                <ValidationSettings CausesValidation="false" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="true" ErrorText="Cabang tidak boleh dikosongkan." />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="TextBoxPaddingLeft RadiusBottomLeft">
                            <dx:ASPxLabel ID="txtNotePassword" runat="server" Text="Password">
                            </dx:ASPxLabel>
                        </td>
                        <td class="TextBoxPaddingRight RadiusBottomRight">
                            <dx:ASPxTextBox ID="txtPassword" runat="server" Width="100%" MaxLength="20" Password="true">
                                <Border BorderStyle="None" />
                                <DisabledStyle BackColor="Transparent" ForeColor="ControlDarkDark" Border-BorderStyle="None">
                                </DisabledStyle>
                                <ValidationSettings CausesValidation="false" Display="Static" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="true" ErrorText="Password tidak boleh dikosongkan."></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <br />
                            <dx:ASPxButton ID="btLogin" Runat="server" CausesValidation="true" Text="LOGIN" tabIndex="100" CssClass="ButtonLogin True" Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Tahoma"
                                AutoPostBack="false">
                                <ClientSideEvents Click="OnClick"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="height: 100px;">
                                <table id="tableNotification" visible="false" cellspacing="20" border="0" width="100%" runat="server">
                                    <tr>
                                        <td align="center" valign="top" class="triangle-right">
                                            <dx:ASPxLabel ID="txtExpiredSession" runat="server" Text="SOMETHING'S WRONG!" Font-Bold="true" ForeColor="#DD0000">
                                            </dx:ASPxLabel>
                                            
                                            <br /><br />
                                            
                                            <dx:ASPxLabel ID="txtNotification" runat="server" Text="" EncodeHtml="false">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <dx:ASPxImage ID="imgWarning" runat="server" ShowLoadingImage="true" ImageUrl="~/Images/userSmaller.png" Height="50px">
                                            </dx:ASPxImage>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    
        <div id="Footer" class="Footer">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <dx:ASPxLabel ID="txtCopyright" runat="server" Text="Copyright &copy 2017 PT. Indomobil Finance Indonesia" ForeColor="#FCFCFC" EncodeHtml="false">
                        </dx:ASPxLabel>
                    </td>
                    <td align="right">
                        <dx:ASPxLabel ID="txtBestViewed" runat="server" Text="Best viewed in Mozilla Firefox" ForeColor="#FCFCFC">
                        </dx:ASPxLabel>
                    </td>
                    <td width="20px">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>

    </form>
</body>
</html>