﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'															 
'*	             Copyright © 2022 IMFI						*
'															 
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*                                                          *
'*   Author      : IMFI Development Team                    *
'*   Purpose     : MST_SUPPLIER Objects Mapper.              *
'*   Called By   :                                          *
'*                                                          *
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/04/2022 - 15:37:09 PM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Data
Imports System.Collections

#End Region

#Region "Custom Namespace Imports"

Imports Microsoft.Practices.EnterpriseLibrary.Data
'Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
'Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Framework.Persistance.Mapper
Imports IMFI.Template.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.DataMapper
    Public Class MST_SUPPLIERMapper
        Inherits AbstractMapper2

#Region "Constructors/Destructors/Finalizers"

        Public Sub New()
            Db = DatabaseFactory.CreateDatabase()
        End Sub

#End Region

#Region "Private Variables"

        Private m_InsertStatement As String = "USP_MST_SUPPLIERInsert"
        Private m_UpdateStatement As String = "USP_MST_SUPPLIERUpdate"
        Private m_RetrieveStatement As String = "USP_MST_SUPPLIERRetrieve"
        Private m_RetrieveListStatement As String = "USP_MST_SUPPLIERRetrieveList"
        Private m_DeleteStatement As String = "USP_MST_SUPPLIERDelete"
        Private m_RetrieveListPagingStatement As String = "USP_MST_SUPPLIERRetrievePagingList"
        Private m_RetrieveWithConditionStatement As String = "USP_MST_SUPPLIERRetrieveWithCondition"
        Private m_RetrieveListPagingRowCountStatement As String = "USP_MST_SUPPLIERRetrievePagingRowCount"

#End Region

#Region "Protected Methods"


        Protected Overrides Function GetNewID(ByVal oDBCommandWrapper As System.Data.Common.DbCommand) As Object
            Return Db.GetParameterValue(DBCommand, "@Supplier_ID")
        End Function

        Protected Overrides Function GetInsertParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oMST_SUPPLIER As MST_SUPPLIER = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_SUPPLIER)

            DBCommand = Db.GetStoredProcCommand(Me.m_InsertStatement)

            Db.AddInParameter(DBCommand, "@Supplier_ID", DbType.AnsiString, oMST_SUPPLIER.Supplier_ID)
            Db.AddInParameter(DBCommand, "@Supplier_Name", DbType.AnsiString, oMST_SUPPLIER.Supplier_Name)

            Return DBCommand
        End Function

        Protected Overrides Function GetUpdateParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim objUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oMST_SUPPLIER As MST_SUPPLIER = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_SUPPLIER)

            DBCommand = Db.GetStoredProcCommand(Me.m_UpdateStatement)

            Db.AddInParameter(DBCommand, "@Supplier_ID", DbType.AnsiString, oMST_SUPPLIER.Supplier_ID)
            Db.AddInParameter(DBCommand, "@Supplier_Name", DbType.AnsiString, oMST_SUPPLIER.Supplier_Name)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oMST_SUPPLIER As MST_SUPPLIER

            If Not IsNothing(oObjectTransporter) Then
                oMST_SUPPLIER = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_SUPPLIER)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveStatement)

            Db.AddInParameter(DBCommand, "@Supplier_ID", DbType.AnsiString, oMST_SUPPLIER.Supplier_ID)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oMST_SUPPLIER As MST_SUPPLIER

            If Not IsNothing(oObjectTransporter) Then
                oMST_SUPPLIER = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_SUPPLIER)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListStatement)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingParameter(ByVal PageSize As Integer, ByVal CurrentPage As Integer, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oMST_SUPPLIER As MST_SUPPLIER

            If Not IsNothing(oObjectTransporter) Then
                oMST_SUPPLIER = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_SUPPLIER)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingStatement)

            Db.AddInParameter(DBCommand, "@PageSize", DbType.Int32, PageSize)
            Db.AddInParameter(DBCommand, "@CurrentPage", DbType.Int32, CurrentPage)
            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy.Split(" "c)(0))
            Db.AddInParameter(DBCommand, "@Order", DbType.String, OrderBy.Split(" "c)(1))
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveWithConditionParameter(ByVal SearchCondition As String, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oMST_SUPPLIER As MST_SUPPLIER

            If Not IsNothing(oObjectTransporter) Then
                oMST_SUPPLIER = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_SUPPLIER)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveWithConditionStatement)

            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy)
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingRowCountParameter(ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oMST_SUPPLIER As MST_SUPPLIER

            If Not IsNothing(oObjectTransporter) Then
                oMST_SUPPLIER = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_SUPPLIER)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingRowCountStatement)

            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetDeleteParameter(oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oMST_SUPPLIER As MST_SUPPLIER = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_SUPPLIER)
            DBCommand = Db.GetStoredProcCommand(Me.m_DeleteStatement)

            Db.AddInParameter(DBCommand, "@Supplier_ID", DbType.AnsiString, oMST_SUPPLIER.Supplier_ID)

            Return DBCommand
        End Function

        Protected Overrides Function DoRetrieve(ByVal dr As IDataReader) As Object
            Dim oMST_SUPPLIER As MST_SUPPLIER = Nothing

            While (dr.Read())
                oMST_SUPPLIER = Me.CreateObject(dr)
            End While

            Return oMST_SUPPLIER
        End Function

        Protected Overrides Function DoRetrieveList(ByVal dr As IDataReader) As ArrayList
            Dim oMST_SUPPLIERList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oMST_SUPPLIER As MST_SUPPLIER = Me.CreateObject(dr)
                oMST_SUPPLIERList.Add(oMST_SUPPLIER)
            End While

            Return oMST_SUPPLIERList
        End Function

        Protected Overrides Function DoRetrieveWithCondition(ByVal dr As IDataReader) As ArrayList
            Dim oMST_SUPPLIERList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oMST_SUPPLIER As MST_SUPPLIER = Me.CreateObject(dr)
                oMST_SUPPLIERList.Add(oMST_SUPPLIER)
            End While

            Return oMST_SUPPLIERList
        End Function

#End Region

#Region "Private Methods"

        Private Function CreateObject(ByVal dr As IDataReader) As MST_SUPPLIER
            Dim oMST_SUPPLIER As MST_SUPPLIER = New MST_SUPPLIER()

            oMST_SUPPLIER.Supplier_ID = dr("Supplier_ID").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("Supplier_Name"))) Then oMST_SUPPLIER.Supplier_Name = dr("Supplier_Name").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("DibuatOleh"))) Then oMST_SUPPLIER.DibuatOleh = dr("DibuatOleh").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDibuat"))) Then oMST_SUPPLIER.WaktuDibuat = CType(dr("WaktuDibuat"), DateTime)
            If (Not dr.IsDBNull(dr.GetOrdinal("DiubahOleh"))) Then oMST_SUPPLIER.DiubahOleh = dr("DiubahOleh").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDiubah"))) Then oMST_SUPPLIER.WaktuDiubah = CType(dr("WaktuDiubah"), DateTime)

            Return oMST_SUPPLIER
        End Function

#End Region
    End Class
End Namespace
