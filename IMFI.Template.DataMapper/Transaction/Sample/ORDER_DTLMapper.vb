﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'															 
'*	             Copyright © 2022 IMFI						*
'															 
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*                                                          *
'*   Author      : IMFI Development Team                    *
'*   Purpose     : ORDER_DTL Objects Mapper.              *
'*   Called By   :                                          *
'*                                                          *
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/04/2022 - 15:38:09 PM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Data
Imports System.Collections

#End Region

#Region "Custom Namespace Imports"

Imports Microsoft.Practices.EnterpriseLibrary.Data
'Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
'Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Framework.Persistance.Mapper
Imports IMFI.Template.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.DataMapper
    Public Class ORDER_DTLMapper
        Inherits AbstractMapper2

#Region "Constructors/Destructors/Finalizers"

        Public Sub New()
            Db = DatabaseFactory.CreateDatabase()
        End Sub

#End Region

#Region "Private Variables"

        Private m_InsertStatement As String = "USP_ORDER_DTLInsert"
        Private m_UpdateStatement As String = "USP_ORDER_DTLUpdate"
        Private m_RetrieveStatement As String = "USP_ORDER_DTLRetrieve"
        Private m_RetrieveListStatement As String = "USP_ORDER_DTLRetrieveList"
        Private m_DeleteStatement As String = "USP_ORDER_DTLDelete"
        Private m_RetrieveListPagingStatement As String = "USP_ORDER_DTLRetrievePagingList"
        Private m_RetrieveWithConditionStatement As String = "USP_ORDER_DTLRetrieveWithCondition"
        Private m_RetrieveListPagingRowCountStatement As String = "USP_ORDER_DTLRetrievePagingRowCount"

#End Region

#Region "Protected Methods"


        Protected Overrides Function GetNewID(ByVal oDBCommandWrapper As System.Data.Common.DbCommand) As Object
            Return Db.GetParameterValue(DBCommand, "@")
        End Function

        Protected Overrides Function GetInsertParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oORDER_DTL As ORDER_DTL = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_DTL)

            DBCommand = Db.GetStoredProcCommand(Me.m_InsertStatement)

            Db.AddInParameter(DBCommand, "@ORDER_ID", DbType.AnsiString, oORDER_DTL.ORDER_ID)
            Db.AddInParameter(DBCommand, "@ProductID", DbType.AnsiString, oORDER_DTL.ProductID)
            Db.AddInParameter(DBCommand, "@QTY", DbType.Int32, oORDER_DTL.QTY)
            Db.AddInParameter(DBCommand, "@PRICE", DbType.Decimal, oORDER_DTL.PRICE)

            Return DBCommand
        End Function

        Protected Overrides Function GetUpdateParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim objUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oORDER_DTL As ORDER_DTL = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_DTL)

            DBCommand = Db.GetStoredProcCommand(Me.m_UpdateStatement)

            Db.AddInParameter(DBCommand, "@ORDER_ID", DbType.AnsiString, oORDER_DTL.ORDER_ID)
            Db.AddInParameter(DBCommand, "@ProductID", DbType.AnsiString, oORDER_DTL.ProductID)
            Db.AddInParameter(DBCommand, "@QTY", DbType.Int32, oORDER_DTL.QTY)
            Db.AddInParameter(DBCommand, "@PRICE", DbType.Decimal, oORDER_DTL.PRICE)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oORDER_DTL As ORDER_DTL

            If Not IsNothing(oObjectTransporter) Then
                oORDER_DTL = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_DTL)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveStatement)



            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oORDER_DTL As ORDER_DTL

            If Not IsNothing(oObjectTransporter) Then
                oORDER_DTL = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_DTL)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListStatement)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingParameter(ByVal PageSize As Integer, ByVal CurrentPage As Integer, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oORDER_DTL As ORDER_DTL

            If Not IsNothing(oObjectTransporter) Then
                oORDER_DTL = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_DTL)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingStatement)

            Db.AddInParameter(DBCommand, "@PageSize", DbType.Int32, PageSize)
            Db.AddInParameter(DBCommand, "@CurrentPage", DbType.Int32, CurrentPage)
            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy.Split(" "c)(0))
            Db.AddInParameter(DBCommand, "@Order", DbType.String, OrderBy.Split(" "c)(1))
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveWithConditionParameter(ByVal SearchCondition As String, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oORDER_DTL As ORDER_DTL

            If Not IsNothing(oObjectTransporter) Then
                oORDER_DTL = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_DTL)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveWithConditionStatement)

            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy)
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingRowCountParameter(ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oORDER_DTL As ORDER_DTL

            If Not IsNothing(oObjectTransporter) Then
                oORDER_DTL = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_DTL)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingRowCountStatement)

            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetDeleteParameter(oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oORDER_DTL As ORDER_DTL = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_DTL)
            DBCommand = Db.GetStoredProcCommand(Me.m_DeleteStatement)



            Return DBCommand
        End Function

        Protected Overrides Function DoRetrieve(ByVal dr As IDataReader) As Object
            Dim oORDER_DTL As ORDER_DTL = Nothing

            While (dr.Read())
                oORDER_DTL = Me.CreateObject(dr)
            End While

            Return oORDER_DTL
        End Function

        Protected Overrides Function DoRetrieveList(ByVal dr As IDataReader) As ArrayList
            Dim oORDER_DTLList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oORDER_DTL As ORDER_DTL = Me.CreateObject(dr)
                oORDER_DTLList.Add(oORDER_DTL)
            End While

            Return oORDER_DTLList
        End Function

        Protected Overrides Function DoRetrieveWithCondition(ByVal dr As IDataReader) As ArrayList
            Dim oORDER_DTLList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oORDER_DTL As ORDER_DTL = Me.CreateObject(dr)
                oORDER_DTLList.Add(oORDER_DTL)
            End While

            Return oORDER_DTLList
        End Function

#End Region

#Region "Private Methods"

        Private Function CreateObject(ByVal dr As IDataReader) As ORDER_DTL
            Dim oORDER_DTL As ORDER_DTL = New ORDER_DTL()


            If (Not dr.IsDBNull(dr.GetOrdinal("ORDER_ID"))) Then oORDER_DTL.ORDER_ID = dr("ORDER_ID").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("ProductID"))) Then oORDER_DTL.ProductID = dr("ProductID").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("QTY"))) Then oORDER_DTL.QTY = CType(dr("QTY"), Integer)
            If (Not dr.IsDBNull(dr.GetOrdinal("PRICE"))) Then oORDER_DTL.PRICE = CType(dr("PRICE"), Decimal)
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDibuat"))) Then oORDER_DTL.WaktuDibuat = CType(dr("WaktuDibuat"), DateTime)
            If (Not dr.IsDBNull(dr.GetOrdinal("DiubahOleh"))) Then oORDER_DTL.DiubahOleh = dr("DiubahOleh").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDiubah"))) Then oORDER_DTL.WaktuDiubah = CType(dr("WaktuDiubah"), DateTime)
            If (Not dr.IsDBNull(dr.GetOrdinal("DibuatOleh"))) Then oORDER_DTL.DibuatOleh = dr("DibuatOleh").ToString()

            Return oORDER_DTL
        End Function

#End Region
    End Class
End Namespace
