﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'															 
'*	             Copyright © 2022 IMFI						*
'															 
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*                                                          *
'*   Author      : IMFI Development Team                    *
'*   Purpose     : MST_PRODUCT Objects Mapper.              *
'*   Called By   :                                          *
'*                                                          *
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/04/2022 - 15:33:44 PM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Data
Imports System.Collections

#End Region

#Region "Custom Namespace Imports"

Imports Microsoft.Practices.EnterpriseLibrary.Data
'Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
'Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Framework.Persistance.Mapper
Imports IMFI.Template.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.DataMapper
    Public Class MST_PRODUCTMapper
        Inherits AbstractMapper2

#Region "Constructors/Destructors/Finalizers"

        Public Sub New()
            Db = DatabaseFactory.CreateDatabase()
        End Sub

#End Region

#Region "Private Variables"

        Private m_InsertStatement As String = "USP_MST_PRODUCTInsert"
        Private m_UpdateStatement As String = "USP_MST_PRODUCTUpdate"
        Private m_RetrieveStatement As String = "USP_MST_PRODUCTRetrieve"
        Private m_RetrieveListStatement As String = "USP_MST_PRODUCTRetrieveList"
        Private m_DeleteStatement As String = "USP_MST_PRODUCTDelete"
        Private m_RetrieveListPagingStatement As String = "USP_MST_PRODUCTRetrievePagingList"
        Private m_RetrieveWithConditionStatement As String = "USP_MST_PRODUCTRetrieveWithCondition"
        Private m_RetrieveListPagingRowCountStatement As String = "USP_MST_PRODUCTRetrievePagingRowCount"

#End Region

#Region "Protected Methods"


        Protected Overrides Function GetNewID(ByVal oDBCommandWrapper As System.Data.Common.DbCommand) As Object
            Return Db.GetParameterValue(DBCommand, "@ProductID")
        End Function

        Protected Overrides Function GetInsertParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oMST_PRODUCT As MST_PRODUCT = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_PRODUCT)

            DBCommand = Db.GetStoredProcCommand(Me.m_InsertStatement)

            Db.AddInParameter(DBCommand, "@ProductID", DbType.AnsiString, oMST_PRODUCT.ProductID)
            Db.AddInParameter(DBCommand, "@ProductName", DbType.AnsiString, oMST_PRODUCT.ProductName)
            Db.AddInParameter(DBCommand, "@HARGA_SATUAN", DbType.Decimal, oMST_PRODUCT.HARGA_SATUAN)
            Db.AddInParameter(DBCommand, "@Supplier_ID", DbType.AnsiString, oMST_PRODUCT.Supplier_ID)

            Return DBCommand
        End Function

        Protected Overrides Function GetUpdateParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim objUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oMST_PRODUCT As MST_PRODUCT = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_PRODUCT)

            DBCommand = Db.GetStoredProcCommand(Me.m_UpdateStatement)

            Db.AddInParameter(DBCommand, "@ProductID", DbType.AnsiString, oMST_PRODUCT.ProductID)
            Db.AddInParameter(DBCommand, "@ProductName", DbType.AnsiString, oMST_PRODUCT.ProductName)
            Db.AddInParameter(DBCommand, "@HARGA_SATUAN", DbType.Decimal, oMST_PRODUCT.HARGA_SATUAN)
            Db.AddInParameter(DBCommand, "@Supplier_ID", DbType.AnsiString, oMST_PRODUCT.Supplier_ID)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oMST_PRODUCT As MST_PRODUCT

            If Not IsNothing(oObjectTransporter) Then
                oMST_PRODUCT = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_PRODUCT)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveStatement)

            Db.AddInParameter(DBCommand, "@ProductID", DbType.AnsiString, oMST_PRODUCT.ProductID)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oMST_PRODUCT As MST_PRODUCT

            If Not IsNothing(oObjectTransporter) Then
                oMST_PRODUCT = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_PRODUCT)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListStatement)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingParameter(ByVal PageSize As Integer, ByVal CurrentPage As Integer, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oMST_PRODUCT As MST_PRODUCT

            If Not IsNothing(oObjectTransporter) Then
                oMST_PRODUCT = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_PRODUCT)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingStatement)

            Db.AddInParameter(DBCommand, "@PageSize", DbType.Int32, PageSize)
            Db.AddInParameter(DBCommand, "@CurrentPage", DbType.Int32, CurrentPage)
            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy.Split(" "c)(0))
            Db.AddInParameter(DBCommand, "@Order", DbType.String, OrderBy.Split(" "c)(1))
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveWithConditionParameter(ByVal SearchCondition As String, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oMST_PRODUCT As MST_PRODUCT

            If Not IsNothing(oObjectTransporter) Then
                oMST_PRODUCT = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_PRODUCT)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveWithConditionStatement)

            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy)
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingRowCountParameter(ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oMST_PRODUCT As MST_PRODUCT

            If Not IsNothing(oObjectTransporter) Then
                oMST_PRODUCT = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_PRODUCT)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingRowCountStatement)

            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetDeleteParameter(oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oMST_PRODUCT As MST_PRODUCT = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_PRODUCT)
            DBCommand = Db.GetStoredProcCommand(Me.m_DeleteStatement)

            Db.AddInParameter(DBCommand, "@ProductID", DbType.AnsiString, oMST_PRODUCT.ProductID)

            Return DBCommand
        End Function

        Protected Overrides Function DoRetrieve(ByVal dr As IDataReader) As Object
            Dim oMST_PRODUCT As MST_PRODUCT = Nothing

            While (dr.Read())
                oMST_PRODUCT = Me.CreateObject(dr)
            End While

            Return oMST_PRODUCT
        End Function

        Protected Overrides Function DoRetrieveList(ByVal dr As IDataReader) As ArrayList
            Dim oMST_PRODUCTList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oMST_PRODUCT As MST_PRODUCT = Me.CreateObject(dr)
                oMST_PRODUCTList.Add(oMST_PRODUCT)
            End While

            Return oMST_PRODUCTList
        End Function

        Protected Overrides Function DoRetrieveWithCondition(ByVal dr As IDataReader) As ArrayList
            Dim oMST_PRODUCTList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oMST_PRODUCT As MST_PRODUCT = Me.CreateObject(dr)
                oMST_PRODUCTList.Add(oMST_PRODUCT)
            End While

            Return oMST_PRODUCTList
        End Function

#End Region

#Region "Private Methods"

        Private Function CreateObject(ByVal dr As IDataReader) As MST_PRODUCT
            Dim oMST_PRODUCT As MST_PRODUCT = New MST_PRODUCT()

            oMST_PRODUCT.ProductID = dr("ProductID").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("ProductName"))) Then oMST_PRODUCT.ProductName = dr("ProductName").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("DibuatOleh"))) Then oMST_PRODUCT.DibuatOleh = dr("DibuatOleh").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDibuat"))) Then oMST_PRODUCT.WaktuDibuat = CType(dr("WaktuDibuat"), DateTime)
            If (Not dr.IsDBNull(dr.GetOrdinal("DiubahOleh"))) Then oMST_PRODUCT.DiubahOleh = dr("DiubahOleh").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDiubah"))) Then oMST_PRODUCT.WaktuDiubah = CType(dr("WaktuDiubah"), DateTime)
            If (Not dr.IsDBNull(dr.GetOrdinal("HARGA_SATUAN"))) Then oMST_PRODUCT.HARGA_SATUAN = CType(dr("HARGA_SATUAN"), Decimal)
            If (Not dr.IsDBNull(dr.GetOrdinal("Supplier_ID"))) Then oMST_PRODUCT.Supplier_ID = dr("Supplier_ID").ToString()

            Return oMST_PRODUCT
        End Function

#End Region
    End Class
End Namespace
