﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'															 
'*	             Copyright © 2022 IMFI						*
'															 
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*                                                          *
'*   Author      : IMFI Development Team                    *
'*   Purpose     : ORDER_HDR Objects Mapper.              *
'*   Called By   :                                          *
'*                                                          *
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/10/2022 - 10:55:38 AM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Data
Imports System.Collections

#End Region

#Region "Custom Namespace Imports"

Imports Microsoft.Practices.EnterpriseLibrary.Data
'Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
'Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Framework.Persistance.Mapper
Imports IMFI.Template.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.DataMapper
    Public Class ORDER_HDRMapper
        Inherits AbstractMapper2

#Region "Constructors/Destructors/Finalizers"

        Public Sub New()
            Db = DatabaseFactory.CreateDatabase()
        End Sub

#End Region

#Region "Private Variables"

        Private m_InsertStatement As String = "USP_ORDER_HDRInsert"
        Private m_UpdateStatement As String = "USP_ORDER_HDRUpdate"
        Private m_RetrieveStatement As String = "USP_ORDER_HDRRetrieve"
        Private m_RetrieveListStatement As String = "USP_ORDER_HDRRetrieveList"
        Private m_DeleteStatement As String = "USP_ORDER_HDRDelete"
        Private m_RetrieveListPagingStatement As String = "USP_ORDER_HDRRetrievePagingList"
        Private m_RetrieveWithConditionStatement As String = "USP_ORDER_HDRRetrieveWithCondition"
        Private m_RetrieveListPagingRowCountStatement As String = "USP_ORDER_HDRRetrievePagingRowCount"

#End Region

#Region "Protected Methods"


        Protected Overrides Function GetNewID(ByVal oDBCommandWrapper As System.Data.Common.DbCommand) As Object
            Return Db.GetParameterValue(DBCommand, "@ORDER_ID")
        End Function

        Protected Overrides Function GetInsertParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oORDER_HDR As ORDER_HDR = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_HDR)

            DBCommand = Db.GetStoredProcCommand(Me.m_InsertStatement)

            Db.AddOutParameter(DBCommand, "@ORDER_ID", DbType.Int32, oORDER_HDR.ORDER_ID)
            Db.AddInParameter(DBCommand, "@ORDER_DATE", DbType.StringFixedLength, oORDER_HDR.ORDER_DATE)
            Db.AddInParameter(DBCommand, "@CustId", DbType.Int32, oORDER_HDR.CustId)
            Db.AddInParameter(DBCommand, "@STATUS_ORDER", DbType.Int32, oORDER_HDR.STATUS_ORDER)
            Db.AddInParameter(DBCommand, "@ALASAN_ORDER", DbType.AnsiString, oORDER_HDR.ALASAN_ORDER)

            Return DBCommand
        End Function

        Protected Overrides Function GetUpdateParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim objUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oORDER_HDR As ORDER_HDR = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_HDR)

            DBCommand = Db.GetStoredProcCommand(Me.m_UpdateStatement)

            Db.AddInParameter(DBCommand, "@ORDER_ID", DbType.Int32, oORDER_HDR.ORDER_ID)
            Db.AddInParameter(DBCommand, "@ORDER_DATE", DbType.StringFixedLength, oORDER_HDR.ORDER_DATE)
            Db.AddInParameter(DBCommand, "@CustId", DbType.Int32, oORDER_HDR.CustId)
            Db.AddInParameter(DBCommand, "@STATUS_ORDER", DbType.Int32, oORDER_HDR.STATUS_ORDER)
            Db.AddInParameter(DBCommand, "@ALASAN_ORDER", DbType.AnsiString, oORDER_HDR.ALASAN_ORDER)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oORDER_HDR As ORDER_HDR

            If Not IsNothing(oObjectTransporter) Then
                oORDER_HDR = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_HDR)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveStatement)

            Db.AddInParameter(DBCommand, "@ORDER_ID", DbType.Int32, oORDER_HDR.ORDER_ID)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oORDER_HDR As ORDER_HDR

            If Not IsNothing(oObjectTransporter) Then
                oORDER_HDR = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_HDR)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListStatement)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingParameter(ByVal PageSize As Integer, ByVal CurrentPage As Integer, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oORDER_HDR As ORDER_HDR

            If Not IsNothing(oObjectTransporter) Then
                oORDER_HDR = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_HDR)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingStatement)

            Db.AddInParameter(DBCommand, "@PageSize", DbType.Int32, PageSize)
            Db.AddInParameter(DBCommand, "@CurrentPage", DbType.Int32, CurrentPage)
            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy.Split(" "c)(0))
            Db.AddInParameter(DBCommand, "@Order", DbType.String, OrderBy.Split(" "c)(1))
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveWithConditionParameter(ByVal SearchCondition As String, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oORDER_HDR As ORDER_HDR

            If Not IsNothing(oObjectTransporter) Then
                oORDER_HDR = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_HDR)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveWithConditionStatement)

            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy)
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingRowCountParameter(ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oORDER_HDR As ORDER_HDR

            If Not IsNothing(oObjectTransporter) Then
                oORDER_HDR = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_HDR)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingRowCountStatement)

            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetDeleteParameter(oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oORDER_HDR As ORDER_HDR = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, ORDER_HDR)
            DBCommand = Db.GetStoredProcCommand(Me.m_DeleteStatement)

            Db.AddInParameter(DBCommand, "@ORDER_ID", DbType.Int32, oORDER_HDR.ORDER_ID)

            Return DBCommand
        End Function

        Protected Overrides Function DoRetrieve(ByVal dr As IDataReader) As Object
            Dim oORDER_HDR As ORDER_HDR = Nothing

            While (dr.Read())
                oORDER_HDR = Me.CreateObject(dr)
            End While

            Return oORDER_HDR
        End Function

        Protected Overrides Function DoRetrieveList(ByVal dr As IDataReader) As ArrayList
            Dim oORDER_HDRList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oORDER_HDR As ORDER_HDR = Me.CreateObject(dr)
                oORDER_HDRList.Add(oORDER_HDR)
            End While

            Return oORDER_HDRList
        End Function

        Protected Overrides Function DoRetrieveWithCondition(ByVal dr As IDataReader) As ArrayList
            Dim oORDER_HDRList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oORDER_HDR As ORDER_HDR = Me.CreateObject(dr)
                oORDER_HDRList.Add(oORDER_HDR)
            End While

            Return oORDER_HDRList
        End Function

#End Region

#Region "Private Methods"

        Private Function CreateObject(ByVal dr As IDataReader) As ORDER_HDR
            Dim oORDER_HDR As ORDER_HDR = New ORDER_HDR()

            oORDER_HDR.ORDER_ID = CType(dr("ORDER_ID"), Integer)
            If (Not dr.IsDBNull(dr.GetOrdinal("ORDER_DATE"))) Then oORDER_HDR.ORDER_DATE = dr("ORDER_DATE").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("CustId"))) Then oORDER_HDR.CustId = CType(dr("CustId"), Integer)
            If (Not dr.IsDBNull(dr.GetOrdinal("STATUS_ORDER"))) Then oORDER_HDR.STATUS_ORDER = CType(dr("STATUS_ORDER"), Integer)
            If (Not dr.IsDBNull(dr.GetOrdinal("ALASAN_ORDER"))) Then oORDER_HDR.ALASAN_ORDER = dr("ALASAN_ORDER").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDibuat"))) Then oORDER_HDR.WaktuDibuat = CType(dr("WaktuDibuat"), DateTime)
            If (Not dr.IsDBNull(dr.GetOrdinal("DiubahOleh"))) Then oORDER_HDR.DiubahOleh = dr("DiubahOleh").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDiubah"))) Then oORDER_HDR.WaktuDiubah = CType(dr("WaktuDiubah"), DateTime)
            If (Not dr.IsDBNull(dr.GetOrdinal("DibuatOleh"))) Then oORDER_HDR.DibuatOleh = dr("DibuatOleh").ToString()

            Return oORDER_HDR
        End Function

#End Region
    End Class
End Namespace
