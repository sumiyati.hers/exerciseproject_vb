﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'															 
'*	             Copyright © 2022 IMFI						*
'															 
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*                                                          *
'*   Author      : IMFI Development Team                    *
'*   Purpose     : MST_KONSUMEN Objects Mapper.              *
'*   Called By   :                                          *
'*                                                          *
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/04/2022 - 15:12:17 PM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Data
Imports System.Collections

#End Region

#Region "Custom Namespace Imports"

Imports Microsoft.Practices.EnterpriseLibrary.Data
'Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
'Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Framework.Persistance.Mapper
Imports IMFI.Template.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.DataMapper
    Public Class MST_KONSUMENMapper
        Inherits AbstractMapper2

#Region "Constructors/Destructors/Finalizers"

        Public Sub New()
            Db = DatabaseFactory.CreateDatabase()
        End Sub

#End Region

#Region "Private Variables"

        Private m_InsertStatement As String = "USP_MST_KONSUMENInsert"
        Private m_UpdateStatement As String = "USP_MST_KONSUMENUpdate"
        Private m_RetrieveStatement As String = "USP_MST_KONSUMENRetrieve"
        Private m_RetrieveListStatement As String = "USP_MST_KONSUMENRetrieveList"
        Private m_DeleteStatement As String = "USP_MST_KONSUMENDelete"
        Private m_RetrieveListPagingStatement As String = "USP_MST_KONSUMENRetrievePagingList"
        Private m_RetrieveWithConditionStatement As String = "USP_MST_KONSUMENRetrieveWithCondition"
        Private m_RetrieveListPagingRowCountStatement As String = "USP_MST_KONSUMENRetrievePagingRowCount"

#End Region

#Region "Protected Methods"


        Protected Overrides Function GetNewID(ByVal oDBCommandWrapper As System.Data.Common.DbCommand) As Object
            Return Db.GetParameterValue(DBCommand, "@CustId")
        End Function

        Protected Overrides Function GetInsertParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oMST_KONSUMEN As MST_KONSUMEN = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_KONSUMEN)

            DBCommand = Db.GetStoredProcCommand(Me.m_InsertStatement)

            Db.AddOutParameter(DBCommand, "@CustId", DbType.Int32, oMST_KONSUMEN.CustId)
            Db.AddInParameter(DBCommand, "@CustName", DbType.AnsiString, oMST_KONSUMEN.CustName)

            Return DBCommand
        End Function

        Protected Overrides Function GetUpdateParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim objUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oMST_KONSUMEN As MST_KONSUMEN = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_KONSUMEN)

            DBCommand = Db.GetStoredProcCommand(Me.m_UpdateStatement)

            Db.AddInParameter(DBCommand, "@CustId", DbType.Int32, oMST_KONSUMEN.CustId)
            Db.AddInParameter(DBCommand, "@CustName", DbType.AnsiString, oMST_KONSUMEN.CustName)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oMST_KONSUMEN As MST_KONSUMEN

            If Not IsNothing(oObjectTransporter) Then
                oMST_KONSUMEN = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_KONSUMEN)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveStatement)

            Db.AddInParameter(DBCommand, "@CustId", DbType.Int32, oMST_KONSUMEN.CustId)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListParameter(ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oMST_KONSUMEN As MST_KONSUMEN

            If Not IsNothing(oObjectTransporter) Then
                oMST_KONSUMEN = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_KONSUMEN)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListStatement)

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingParameter(ByVal PageSize As Integer, ByVal CurrentPage As Integer, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oMST_KONSUMEN As MST_KONSUMEN

            If Not IsNothing(oObjectTransporter) Then
                oMST_KONSUMEN = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_KONSUMEN)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingStatement)

            Db.AddInParameter(DBCommand, "@PageSize", DbType.Int32, PageSize)
            Db.AddInParameter(DBCommand, "@CurrentPage", DbType.Int32, CurrentPage)
            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy.Split(" "c)(0))
            Db.AddInParameter(DBCommand, "@Order", DbType.String, OrderBy.Split(" "c)(1))
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveWithConditionParameter(ByVal SearchCondition As String, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oMST_KONSUMEN As MST_KONSUMEN

            If Not IsNothing(oObjectTransporter) Then
                oMST_KONSUMEN = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_KONSUMEN)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveWithConditionStatement)

            Db.AddInParameter(DBCommand, "@OrderBy", DbType.String, OrderBy)
            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetRetrieveListPagingRowCountParameter(ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand
            Dim oMST_KONSUMEN As MST_KONSUMEN

            If Not IsNothing(oObjectTransporter) Then
                oMST_KONSUMEN = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_KONSUMEN)
            End If
            DBCommand = Db.GetStoredProcCommand(Me.m_RetrieveListPagingRowCountStatement)

            Db.AddInParameter(DBCommand, "@SearchCondition", DbType.String, IIf(IsNothing(SearchCondition), "", SearchCondition))

            Return DBCommand
        End Function

        Protected Overrides Function GetDeleteParameter(oObjectTransporter As IObjectTransporter) As System.Data.Common.DbCommand
            Dim oUserCredential As IUserCredential = CType(oObjectTransporter, IObjectTransporter).UserCredential
            Dim oMST_KONSUMEN As MST_KONSUMEN = CType(CType(oObjectTransporter, IObjectTransporter).DomainObject, MST_KONSUMEN)
            DBCommand = Db.GetStoredProcCommand(Me.m_DeleteStatement)

            Db.AddInParameter(DBCommand, "@CustId", DbType.Int32, oMST_KONSUMEN.CustId)

            Return DBCommand
        End Function

        Protected Overrides Function DoRetrieve(ByVal dr As IDataReader) As Object
            Dim oMST_KONSUMEN As MST_KONSUMEN = Nothing

            While (dr.Read())
                oMST_KONSUMEN = Me.CreateObject(dr)
            End While

            Return oMST_KONSUMEN
        End Function

        Protected Overrides Function DoRetrieveList(ByVal dr As IDataReader) As ArrayList
            Dim oMST_KONSUMENList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oMST_KONSUMEN As MST_KONSUMEN = Me.CreateObject(dr)
                oMST_KONSUMENList.Add(oMST_KONSUMEN)
            End While

            Return oMST_KONSUMENList
        End Function

        Protected Overrides Function DoRetrieveWithCondition(ByVal dr As IDataReader) As ArrayList
            Dim oMST_KONSUMENList As ArrayList = New ArrayList()

            While (dr.Read())
                Dim oMST_KONSUMEN As MST_KONSUMEN = Me.CreateObject(dr)
                oMST_KONSUMENList.Add(oMST_KONSUMEN)
            End While

            Return oMST_KONSUMENList
        End Function

#End Region

#Region "Private Methods"

        Private Function CreateObject(ByVal dr As IDataReader) As MST_KONSUMEN
            Dim oMST_KONSUMEN As MST_KONSUMEN = New MST_KONSUMEN()

            oMST_KONSUMEN.CustId = CType(dr("CustId"), Integer)
            If (Not dr.IsDBNull(dr.GetOrdinal("CustName"))) Then oMST_KONSUMEN.CustName = dr("CustName").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("DibuatOleh"))) Then oMST_KONSUMEN.DibuatOleh = dr("DibuatOleh").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDibuat"))) Then oMST_KONSUMEN.WaktuDibuat = CType(dr("WaktuDibuat"), DateTime)
            If (Not dr.IsDBNull(dr.GetOrdinal("DiubahOleh"))) Then oMST_KONSUMEN.DiubahOleh = dr("DiubahOleh").ToString()
            If (Not dr.IsDBNull(dr.GetOrdinal("WaktuDiubah"))) Then oMST_KONSUMEN.WaktuDiubah = CType(dr("WaktuDiubah"), DateTime)

            Return oMST_KONSUMEN
        End Function

#End Region
    End Class
End Namespace
