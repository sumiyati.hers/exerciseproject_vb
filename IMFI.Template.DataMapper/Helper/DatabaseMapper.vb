﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports IMFI.Framework.Persistance.Mapper

Namespace IMFI.Template.DataMapper

    Public Class DatabaseMapper
        Inherits AbstractMapper

        Public Sub New()
            Db = DatabaseFactory.CreateDatabase("DBData")
        End Sub

        Public Function CurrentConectionStringDB() As String
            Return Db.ConnectionStringWithoutCredentials
        End Function

        Protected Overrides Function DoRetrieve(ByVal dr As System.Data.IDataReader) As Object

        End Function

        Protected Overrides Function DoRetrieveList(ByVal dr As System.Data.IDataReader) As System.Collections.ArrayList

        End Function

        Protected Overrides Function GetDeleteParameter(ByVal oObjectTransporter As IMFI.Framework.Persistance.DomainObjects.Core.IObjectTransporter) As System.Data.Common.DbCommand

        End Function

        Protected Overrides Function GetInsertParameter(ByVal oObjectTransporter As IMFI.Framework.Persistance.DomainObjects.Core.IObjectTransporter) As System.Data.Common.DbCommand

        End Function

        Protected Overrides Function GetNewID(ByVal DBCommand As System.Data.Common.DbCommand) As Object

        End Function

        Protected Overrides Function GetRetrieveListPagingParameter(ByVal PageSize As Integer, ByVal CurrentPage As Integer, ByVal OrderBy As String, ByVal oObjectTransporter As IMFI.Framework.Persistance.DomainObjects.Core.IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand

        End Function

        Protected Overrides Function GetRetrieveListPagingRowCountParameter(ByVal oObjectTransporter As IMFI.Framework.Persistance.DomainObjects.Core.IObjectTransporter, ByVal SearchCondition As String) As System.Data.Common.DbCommand

        End Function

        Protected Overrides Function GetRetrieveListParameter(ByVal oObjectTransporter As IMFI.Framework.Persistance.DomainObjects.Core.IObjectTransporter) As System.Data.Common.DbCommand

        End Function

        Protected Overrides Function GetRetrieveParameter(ByVal oObjectTransporter As IMFI.Framework.Persistance.DomainObjects.Core.IObjectTransporter) As System.Data.Common.DbCommand

        End Function

        Protected Overrides Function GetUpdateParameter(ByVal oObjectTransporter As IMFI.Framework.Persistance.DomainObjects.Core.IObjectTransporter) As System.Data.Common.DbCommand

        End Function

    End Class

End Namespace