﻿Public Class DayFormat

    Public Function DayToIna(ByVal Hari As String) As String
        Select Case Trim(UCase(Hari))
            Case "SUNDAY"
                DayToIna = "Minggu"
            Case "MONDAY"
                DayToIna = "Senin"
            Case "TUESDAY"
                DayToIna = "Selasa"
            Case "WEDNESDAY"
                DayToIna = "Rabu"
            Case "THURSDAY"
                DayToIna = "Kamis"
            Case "FRIDAY"
                DayToIna = "Jumat"
            Case "SATURDAY"
                DayToIna = "Sabtu"
            Case Else
                DayToIna = ""
        End Select

        Return DayToIna
    End Function

End Class
