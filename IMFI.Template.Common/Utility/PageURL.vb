﻿#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.HttpRequest
Imports System.Web.SessionState
Imports System.Web.HttpContext

#End Region

#Region "Custom Namespace Imports"

#End Region

#End Region

Namespace IMFI.Template.Common

    Public Class PageURL

        Public Shared Function GetPageURL() As String
            Return HttpContext.Current.Request.Url.ToString
        End Function

        Public Shared Function GetPageName() As String
            Dim meASPX As String
            Dim URLPage As String = HttpContext.Current.Request.Url.ToString

            If URLPage.IndexOf("?") > -1 Then
                meASPX = URLPage.Split("?")(0) ' QueryString
            Else
                meASPX = URLPage
            End If

            meASPX = meASPX.Substring(meASPX.IndexOfAny("/", 1) + 1)   'zzzz/xxxx.aspx

            Do While meASPX.IndexOfAny("/", 1) > 0
                meASPX = meASPX.Substring(meASPX.IndexOfAny("/", 1) + 1)   'xxxx.aspx
            Loop
            meASPX = Left(meASPX, meASPX.IndexOf(".aspx", 1))  'xxxxx

            Return meASPX
        End Function

        Public Shared Function GetPageName(ByVal URL As String) As String
            Dim meASPX As String
            Dim URLPage As String = URL

            If URLPage.IndexOf("?") > -1 Then
                meASPX = URLPage.Split("?")(0) ' QueryString
            Else
                meASPX = URLPage
            End If

            meASPX = meASPX.Substring(meASPX.IndexOfAny("/", 1) + 1)   'zzzz/xxxx.aspx

            Do While meASPX.IndexOfAny("/", 1) > 0
                meASPX = meASPX.Substring(meASPX.IndexOfAny("/", 1) + 1)   'xxxx.aspx
            Loop
            meASPX = Left(meASPX, meASPX.IndexOf(".aspx", 1))  'xxxxx

            Return meASPX
        End Function

    End Class

End Namespace
