﻿Public Class BranchFormat

    Public Function BranchNameShort(ByVal value As String) As String
        Return Replace(Replace(value, "PT. IMFI - ", ""), "PT. INDOMOBIL FIN - ", "")
    End Function

    Public Function BranchNameOnly(ByVal value As String) As String
        Return Replace(Replace(Replace(Replace(Replace(value, "PT. IMFI - ", ""), "PT. INDOMOBIL FIN - ", ""), "PT. IMFI-", ""), "CAB. ", ""), "CAB.", "")
    End Function

End Class
