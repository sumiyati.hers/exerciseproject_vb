﻿Namespace IMFI.Template.Common

    Public Class CurrencyConverter

#Region "Public Method"

        Public Function CurrencyToString(ByVal value As Decimal) As String
            Return value.ToString("###,###,##0", New System.Globalization.CultureInfo("en-US"))
        End Function

        Public Function CurrencyToString(ByVal value As Decimal, ByVal digit As Int16) As String
            Return value.ToString("###,###,##0." + Left("00000", digit), New System.Globalization.CultureInfo("en-US"))
        End Function

        Public Function StringToCurrency(ByVal value As String) As Decimal
            If IsNothing(value) OrElse value.Length = 0 Then
                value = "0.0"
            End If
            Return Convert.ChangeType(value, TypeCode.Decimal, New System.Globalization.CultureInfo("en-US"))
        End Function

        Public Function StringToCurrency(ByVal value As String, ByVal digit As Int16) As Decimal
            If value.Length = 0 Then
                value = "0." + Left("00000", digit)
            End If
            Return Convert.ChangeType(value, TypeCode.Decimal, New System.Globalization.CultureInfo("en-US"))
        End Function

        Public Function IsCurrency(ByVal value As String) As Boolean
            Try
                Convert.ChangeType(value, TypeCode.Decimal, New System.Globalization.CultureInfo("en-US"))
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function FormatStringCurrency(ByVal value As String) As String
            If IsCurrency(value) Then
                If CurrencyToString(StringToCurrency(value)) = ".00" Then
                    Return 0.0
                Else
                    Return CurrencyToString(StringToCurrency(value))
                End If
            Else
                Return "0.00" ' CurrencyToString(0.0)
            End If
        End Function

        Public Function FormatStringCurrency(ByVal value As String, ByVal digit As Int16) As String
            If IsCurrency(value) Then
                If CurrencyToString(StringToCurrency(value)) = ".00" Then
                    Return 0.0
                Else
                    Return CurrencyToString(StringToCurrency(value, digit), digit)
                End If
            Else
                Return "0.00" ' CurrencyToString(0.0)
            End If
        End Function

#End Region

    End Class

End Namespace
