﻿Imports System.Text

Public Class PopUpHelper
    Private _width As Integer
    Private _height As Integer
    Private _url As String
    Private _page As System.Web.UI.Page

    Public Property TargetURL() As String
        Get
            Return _url
        End Get
        Set(ByVal Value As String)
            _url = Value
        End Set
    End Property

    Public Sub New(ByVal oPage As System.Web.UI.Page)
        _width = 1024
        _height = 768
        _page = oPage
    End Sub

    Public Sub ShowPopUp()
        'RemovePopUp()
        Dim windowBuilder As New StringBuilder

        windowBuilder.Append("<script language=JavaScript> window.open(")
        windowBuilder.AppendFormat("'{0}' ", _url)
        windowBuilder.Append(", 'Report'")
        windowBuilder.Append(",'")
        windowBuilder.AppendFormat("toolbar=1")
        windowBuilder.AppendFormat(", scrollbars=1")
        windowBuilder.AppendFormat(", location=0")
        windowBuilder.AppendFormat(", statusBar=0")
        windowBuilder.AppendFormat(", menubar=0")
        windowBuilder.AppendFormat(", resizable=1")
        'windowBuilder.AppendFormat(", width={0}", _width)
        'windowBuilder.AppendFormat(", height={0}", _height)
        windowBuilder.AppendFormat(", fullscreen=yes")
        'windowBuilder.AppendFormat(", left=112")
        'windowBuilder.Append(", top=84")
        windowBuilder.Append("'")
        windowBuilder.Append(");")
        windowBuilder.Append("</script>")

        If (Not _page.ClientScript.IsStartupScriptRegistered("Startup")) Then
            _page.ClientScript.RegisterStartupScript(_page.GetType, "Startup", windowBuilder.ToString())

        End If
    End Sub

    Public Sub RemovePopUp()
        If (Not _page.ClientScript.IsStartupScriptRegistered("Startup")) Then
            _page.ClientScript.RegisterStartupScript(_page.GetType, "Startup", String.Empty)
        End If
    End Sub

End Class
