﻿Namespace IMFI.Template.Common

    Public Class StatusDescription
        Public Function ToDescription(ByVal ToCode As String) As String
            Select Case ToCode
                Case "-"
                    ToDescription = "-"
                Case "N"
                    ToDescription = "New"
                Case "S"
                    ToDescription = "Submit"
                Case "P"
                    ToDescription = "Process"
                Case "A"
                    ToDescription = "Approve"
                Case "R"
                    ToDescription = "Reject"
                Case "V"
                    ToDescription = "Revise"
                Case "H"
                    ToDescription = "History"
                Case "L"
                    ToDescription = "Resolve"
                Case "C"
                    ToDescription = "Closed"
                Case Else
                    ToDescription = ""
            End Select

            Return ToDescription
        End Function

        Public Function ToCode(ByVal ToDescription As String) As String
            Select Case Trim(ToDescription.ToUpper)
                Case "-"
                    ToCode = "-"
                Case "NEW"
                    ToCode = "N"
                Case "SUBMIT"
                    ToCode = "S"
                Case "PROCESS"
                    ToCode = "P"
                Case "APPROVE"
                    ToCode = "A"
                Case "REJECT"
                    ToCode = "R"
                Case "REVISE"
                    ToCode = "V"
                Case "HISTORY"
                    ToCode = "H"
                Case "RESOLVE"
                    ToCode = "L"
                Case "CLOSED"
                    ToCode = "C"
                Case Else
                    ToCode = ""
            End Select

            Return ToCode
        End Function

    End Class
End Namespace