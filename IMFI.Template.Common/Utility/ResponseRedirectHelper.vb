﻿#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports System.Web.HttpRequest
Imports System.Web.SessionState
Imports System.Web.HttpContext
Imports AjaxControlToolkit

#End Region

#Region "Custom Namespace Imports"

#End Region

#End Region

Namespace IMFI.Template.Common

    Public NotInheritable Class ResponseRedirectHelper
        Private Sub New()
        End Sub

        Public Shared Sub Redirect(ByVal url As String, ByVal target As String, ByVal windowFeatures As String)
            Dim page As Page = DirectCast(HttpContext.Current.Handler, Page)

            If page Is Nothing Then
                Throw New InvalidOperationException("Cannot redirect to new window outside Page context.")
            End If

            url = page.ResolveClientUrl(url)

            Dim script As String

            If Not [String].IsNullOrEmpty(windowFeatures) Then
                script = "window.open(""{0}"", ""{1}"", ""{2}"");"
            Else
                script = "window.open(""{0}"", ""{1}"");"
            End If

            script = [String].Format(script, url, target, windowFeatures)
            AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(page, GetType(Page), "Redirect", script, True)
            'ScriptManager.RegisterStartupScript(page, GetType(Page), "Redirect", script, True)
        End Sub

    End Class

End Namespace