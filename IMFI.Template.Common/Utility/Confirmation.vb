﻿#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System.Web
Imports System.Web.SessionState
Imports System.Web.HttpContext
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports System.Reflection

#End Region

#Region "Custom Namespace Imports"

#End Region

#End Region

Namespace IMFI.Template.Common

    Public Class Confirmation

        Shared Function JSEscape(ByVal text As String) As String
            Return text.Replace("\", "\\"). _
            Replace("'", "\'").Replace("<br>", "\n"). _
            Replace(vbCrLf, "\n"). _
            Replace(Chr(13), "\n"). _
            Replace(Chr(10), "\n")
        End Function

        Public Shared Sub AddOnLoadJSAction(ByVal aPage As System.Web.UI.Page, ByVal jsAction As String)
            Static id As Integer = 0
            aPage.ClientScript.RegisterClientScriptBlock(aPage.GetType, "AlertScript", _
                "<script>var onloadActions = new Array(); " & _
                "window.onload = function ()" & _
                "{ for(var i = 0; i < onloadActions.length; ++i) " & _
                "(onloadActions[i])(); }</script>")

            aPage.ClientScript.RegisterClientScriptBlock(aPage.GetType, id, _
                "<script>onloadActions[onloadActions.length] = function() {" & jsAction & "}</script>")

            'aPage.RegisterClientScriptBlock("AlertScript", _
            '    "<script>var onloadActions = new Array(); " & _
            '    "window.onload = function ()" & _
            '    "{ for(var i = 0; i < onloadActions.length; ++i) " & _
            '    "(onloadActions[i])(); }</script>")
            'aPage.RegisterClientScriptBlock(id, _
            '    "<script>onloadActions[onloadActions.length] = function() {" & jsAction & "}</script>")
            
            
            id += 1
        End Sub

        Public Shared Function GetMetaControl(ByVal parent As Control, ByVal ID As String, ByVal controlType As Type) As Control
            Dim res As Control
            Dim ctl As Control
            For Each ctl In parent.Controls
                If TypeOf (ctl) Is HtmlForm Then
                    res = ctl.FindControl(ID)
                    If res Is Nothing Then
                        res = Activator.CreateInstance(controlType)
                        res.ID = ID
                        ctl.Controls.Add(res)
                        Return res
                    Else
                        Return res
                    End If
                End If
            Next
            Throw New SystemException("Unable to locate HtmlForm on the page")
        End Function

        Public Shared Function ConfirmationDone() As Boolean
            Return Not System.Web.HttpContext.Current.Request("XXActionConfirmed") Is Nothing
        End Function

        Public Shared Function Confirmed() As Boolean
            Dim result As String = HttpContext.Current.Request("XXActionConfirmed")
            Return Not result Is Nothing AndAlso result = "yes"
        End Function

        Public Shared Sub ConfirmAction(ByVal postbackControl As Control, ByVal text As String)
            Dim confirmator As HtmlInputHidden = GetMetaControl(postbackControl.Page, "XXActionConfirmed", GetType(HtmlInputHidden))
            confirmator.Value = "no"
            AddOnLoadJSAction(postbackControl.Page, _
                String.Format( _
                    "if(confirm('{0}')) " & _
                    "{{document.getElementById('XXActionConfirmed').value = 'yes'}} " & _
                    "document.getElementById('{1}').click()", JSEscape(text), JSEscape(postbackControl.ClientID)))
        End Sub
    End Class

End Namespace
