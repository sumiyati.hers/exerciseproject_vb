﻿#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Web
Imports System.Web.SessionState

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects.Core

#End Region

#End Region

Namespace IMFI.Template.Common

    Public NotInheritable Class UserIdentification
        Public Const USER_AUTORIZATION_SESSION_NAME As String = "UserAuthorization"
        Public Const EMPLOYEE_INFO As String = "EmployeeInfo"

#Region "Private Properties"

        Private ReadOnly Property CurrentUserAuthorization() As IUserCredential
            Get
                If Not IsAlive() Then
                    'HttpContext.Current.Session("BACKURL") = HttpContext.Current.Request.Url.ToString()
                    'HttpContext.Current.Server.Transfer("..\Login_PD.aspx")
                    'HttpContext.Current.Server.Transfer("temp.aspx")

                    'HttpContext.Current.Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/temp.aspx")

                    HttpContext.Current.Response.Write("<base target='_parent'>")
                    HttpContext.Current.Response.Redirect("~\NotAuthorized\NotAuthorized\SessionExpired.aspx")

                    'HttpContext.Current.Response.Redirect(HttpContext.Current.Request.ApplicationPath + "/Login.aspx")
                End If
                Return CType(HttpContext.Current.Session(USER_AUTORIZATION_SESSION_NAME), IUserCredential)
            End Get
        End Property

        Private ReadOnly Property CurrentEmployee() As IEmployeeInfo
            Get
                If Not IsAlive() Then
                    HttpContext.Current.Response.Write("<base target='_parent'>")
                    HttpContext.Current.Response.Redirect("~\NotAuthorized\NotAuthorized\SessionExpired.aspx")
                End If
                Return CType(HttpContext.Current.Session(EMPLOYEE_INFO), IEmployeeInfo)
            End Get
        End Property

#End Region

#Region "Constructor"

        Public Sub New()

        End Sub

#End Region

#Region "Public Method"

        Public Function GetUserCredential() As IUserCredential
            Return CurrentUserAuthorization
        End Function

        Public Function CurrentUserInfo() As IMFI.Framework.Persistance.DomainObjects.Core.IUserLogin
            Return CurrentUserAuthorization.UserLoginInfo
        End Function

        Public Function CurrentEmployeeInfo() As IEmployeeInfo
            Return CurrentEmployee
        End Function

        Public Function CurrentBranchPusatInfo() As IBranchInfo
            Return CurrentUserAuthorization.BranchPusatLoginInfo
        End Function

        Public Function CurrentBranchInfo() As IBranchInfo
            Return CurrentUserAuthorization.BranchLoginInfo
        End Function

        Public Function IsAlive() As Boolean
            Return Not IsNothing(HttpContext.Current.Session(USER_AUTORIZATION_SESSION_NAME))
        End Function

        Public Sub Register(ByVal value As IUserCredential)
            Dim Session As HttpSessionState = HttpContext.Current.Session
            Session(USER_AUTORIZATION_SESSION_NAME) = value
        End Sub

        Public Sub Register(ByVal value As IUserCredential, ByVal employeeInfo As IEmployeeInfo)
            Dim Session As HttpSessionState = HttpContext.Current.Session
            Session(USER_AUTORIZATION_SESSION_NAME) = value
            Session(EMPLOYEE_INFO) = employeeInfo
        End Sub

        Public Function CurrentBranchCode() As Decimal
            Return CurrentUserAuthorization.BranchLoginInfo.BRANCH_CODE
        End Function

        Public Function CurrentBranchPusatCode() As Decimal
            Return CurrentUserAuthorization.BranchPusatLoginInfo.BRANCH_CODE
        End Function
        Public Function CurrentBranchRep() As Decimal
            Return CurrentUserAuthorization.BranchPusatLoginInfo.BRANCH_CODE_REP
        End Function

        Public Function NoDataEmployee() As Int32
            Return CurrentEmployeeInfo.NoDataEmployee
        End Function

#End Region
    End Class

End Namespace