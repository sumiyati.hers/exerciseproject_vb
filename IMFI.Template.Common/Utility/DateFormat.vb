﻿
#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Collections
Imports System.Reflection

#End Region

#End Region

Namespace IMFI.Template.Common

    Public Class DateFormat
        Public Shared Function ToIndonesianDate(ByVal DateValue As Object) As DateTime
            Return Convert.ToDateTime(DateValue, New System.Globalization.CultureInfo("id-ID"))
        End Function

        Public Shared Function ToWesternDate(ByVal DateValue As Object) As DateTime
            Return Convert.ToDateTime(DateValue, New System.Globalization.CultureInfo("en-US"))
        End Function

        Public Shared Function ToReportDateFormat(ByVal DateValue As Object) As String
            Return Convert.ToDateTime(DateValue, New System.Globalization.CultureInfo("id-ID")).Year.ToString() + Right("00" + Convert.ToDateTime(DateValue, New System.Globalization.CultureInfo("id-ID")).Month.ToString, 2) + Right("00" + Convert.ToDateTime(DateValue, New System.Globalization.CultureInfo("id-ID")).Day.ToString, 2)
        End Function

        Public Shared Function IsSQLDateTimeMinValue(ByVal DateValue As DateTime) As Boolean
            Return DateValue = CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)
        End Function

        Public Shared Function SQLDateTimeMinValue() As DateTime
            Return CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)
        End Function

        Public Shared Function GetLastDayOfTheMonth(ByVal Year As Int16, ByVal Month As Int16) As String
            Dim oDate As String = ""
            If Date.DaysInMonth(Year, Month) = 30 Then
                oDate = "30"
            ElseIf Date.DaysInMonth(Year, Month) = 31 Then
                oDate = "31"
            ElseIf Date.DaysInMonth(Year, Month) = 28 Then
                oDate = "28"
            ElseIf Date.DaysInMonth(Year, Month) = 29 Then
                oDate = "29"
            End If
            Return oDate & "/" & Date.Now.Month & "/" & Date.Now.Year
        End Function

        Public Function StringToPeriod(ByVal PeriodDate As String) As String
            Return Date.Parse(PeriodDate).ToString("MMyyyy")
        End Function

    End Class
End Namespace
