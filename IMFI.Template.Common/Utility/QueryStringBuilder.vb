﻿#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Collections
Imports System.Configuration
Imports System.Reflection
Imports System.Text
Imports System.Security.Cryptography
Imports System.IO

#End Region

#Region "Custom Namespace Imports"

#End Region

#End Region

Namespace IMFI.Template.Common

    Public Class QueryStringBuilder

#Region "Public Constant"


#End Region

#Region "Private Constant"

        Private Const VALUE_SEPARATOR As String = ":"
        Private Const QUERY_SEPARATOR As String = ";"
        Private Const REPLACE_VALUE_SEPARATOR As String = "###"
        Private Const REPLACE_QUERY_SEPARATOR As String = "$$$"
        Private Const ILEGAL_CHAR As String = "+"
        Private Const LEGAL_CHAR As String = "@@"
        Private Const FORM_MODE As String = "FORM_MODE"

#End Region

#Region "Private Variable"

        Private m_QSHash As Hashtable
        Private m_SourceObjectType As Type
        Private _SourcePropertyInfos As PropertyInfo() = Nothing

#End Region

#Region "Constructor"

        Public Sub New()
            m_QSHash = New Hashtable
        End Sub

#End Region

#Region "Private Property"

        Private Function SessionID() As String
            Return System.Web.HttpContext.Current.Session.SessionID
        End Function

        Private ReadOnly Property SourcePropertyInfos() As PropertyInfo()
            Get
                If IsNothing(_SourcePropertyInfos) Then
                    _SourcePropertyInfos = m_SourceObjectType.GetProperties()
                End If
                Return _SourcePropertyInfos
            End Get
        End Property

#End Region

#Region "Private Method"

        Private Function EncryptQS(ByVal QueryString As String) As String
            If IsNothing(QueryString) Then
                Return Nothing
            End If

            If ConfigurationManager.AppSettings("ENCRYPT_QUERY_STRING") = "1" Then
                Dim ToEncryptQS As Byte() = (New UTF8Encoding).GetBytes(QueryString)
                Dim oPasswordDeriveBytes As PasswordDeriveBytes = New PasswordDeriveBytes(SessionID, Nothing)
                Dim oMemoryStream As MemoryStream = New MemoryStream
                Dim oCryptoStream As CryptoStream = New CryptoStream(oMemoryStream, (New RC2CryptoServiceProvider).CreateEncryptor(oPasswordDeriveBytes.GetBytes(8), oPasswordDeriveBytes.GetBytes(8)), CryptoStreamMode.Write)
                Dim bufferLength(4) As Byte

                bufferLength(0) = CType((QueryString.Length And 255), Byte)
                bufferLength(1) = CType(((QueryString.Length >> 8) And 255), Byte)
                bufferLength(2) = CType(((QueryString.Length >> 16) And 255), Byte)
                bufferLength(3) = CType(((QueryString.Length >> 24) And 255), Byte)

                oCryptoStream.Write(bufferLength, 0, 4)
                oCryptoStream.Write(ToEncryptQS, 0, ToEncryptQS.Length)
                oCryptoStream.FlushFinalBlock()

                Return System.Web.HttpContext.Current.Server.UrlEncode(Convert.ToBase64String(oMemoryStream.ToArray).Replace(ILEGAL_CHAR, LEGAL_CHAR))

            Else
                Return QueryString
            End If
        End Function

        Private Function DecryptQS(ByVal QueryString As String) As String
            If IsNothing(QueryString) Then
                Return Nothing
            End If

            If ConfigurationManager.AppSettings("ENCRYPT_QUERY_STRING") = "1" Then
                Dim EncryptedQueryString As Byte() = Convert.FromBase64String(System.Web.HttpContext.Current.Server.UrlDecode(QueryString).Replace(LEGAL_CHAR, ILEGAL_CHAR))
                Dim oKeyGenerated As PasswordDeriveBytes = New PasswordDeriveBytes(SessionID, Nothing)
                Dim oMemoryStreamt As MemoryStream = New MemoryStream(EncryptedQueryString)
                Dim oCryptoStream As CryptoStream = New CryptoStream(oMemoryStreamt, (New RC2CryptoServiceProvider).CreateDecryptor(oKeyGenerated.GetBytes(8), oKeyGenerated.GetBytes(8)), CryptoStreamMode.Read)
                Dim fromEncrypt(EncryptedQueryString.Length - 4) As Byte
                Dim bufferLength(4) As Byte

                oCryptoStream.Read(bufferLength, 0, 4)
                oCryptoStream.Read(fromEncrypt, 0, fromEncrypt.Length)

                Dim LengthString As Int32 = CType(bufferLength(0), Int32) Or (bufferLength(1) << 8) Or (bufferLength(2) << 16) Or (bufferLength(3) << 24)

                Return (New UTF8Encoding).GetString(fromEncrypt).Substring(0, LengthString)
            Else
                Return QueryString
            End If
        End Function

        Private Function EnumerateQueryString() As String
            Dim oStringBuilder As StringBuilder = New StringBuilder
            Dim oIEnumerator As IDictionaryEnumerator

            oIEnumerator = m_QSHash.GetEnumerator()

            While oIEnumerator.MoveNext
                oStringBuilder.Append(oIEnumerator.Key)
                oStringBuilder.Append(VALUE_SEPARATOR)
                oStringBuilder.Append(Convert.ToString(oIEnumerator.Value).Replace(VALUE_SEPARATOR, REPLACE_VALUE_SEPARATOR).Replace(QUERY_SEPARATOR, REPLACE_QUERY_SEPARATOR))
                oStringBuilder.Append(QUERY_SEPARATOR)
            End While

            Return oStringBuilder.ToString(0, oStringBuilder.Length - 1)
        End Function

        Private Function GenerateQueryString() As String
            Dim oStringBuilder As StringBuilder = New StringBuilder

            oStringBuilder.Append("?")
            oStringBuilder.Append(SessionID.Substring(0, 8))
            oStringBuilder.Append("=")
            oStringBuilder.Append(EncryptQS(EnumerateQueryString()))

            Return oStringBuilder.ToString()
        End Function

        Private Function SplitQueryString(ByVal QueryString As String) As Array
            If IsNothing(QueryString) Then
                Return Nothing
            End If
            Return QueryString.Split(QUERY_SEPARATOR)
        End Function

        Private Function SplitQueryStringValue(ByVal QueryStringValue As String) As Array
            If IsNothing(QueryStringValue) Then
                Return Nothing
            End If
            Return QueryStringValue.Split(VALUE_SEPARATOR)
        End Function

#End Region

#Region "Public Method"

        Public Sub AddQSFromObject(ByVal SourceObject As Object)
            m_SourceObjectType = SourceObject.GetType
            Me.ClearQS()
            For Each oProperty As PropertyInfo In SourcePropertyInfos
                Me.AddQS(oProperty.Name.ToString().Trim(), CType(oProperty.GetValue(SourceObject, Nothing), String))
            Next
        End Sub

        Public Function QSCount() As Int32
            Return m_QSHash.Count
        End Function

        Public Sub AddQS(ByVal Key As String, ByVal Value As String)
            If m_QSHash.ContainsKey(Key) Then
                Throw New Exception("Key already exists.")
            End If
            If Not IsNothing(Value) Then
                Value = Value.Replace(":", "xttk2")
            End If
            m_QSHash.Add(Key, Value)
        End Sub

        Public Sub RemoveQS(ByVal Key As String)
            If m_QSHash.ContainsKey(Key) Then
                m_QSHash.Remove(Key)
            End If
        End Sub

        Public Sub ClearQS()
            m_QSHash.Clear()
        End Sub

        Public Function GetQSItem(ByVal Key As String) As String
            If m_QSHash.ContainsKey(Key) Then
                Return CType(m_QSHash.Item(Key), String).Replace("xttk2", ":")
            End If
            Return Nothing
        End Function

        Public Function ToQueryString() As String
            Return GenerateQueryString()
        End Function

        Public Function ToQueryString(ByVal PageURL As String)
            If (PageURL.Trim.Length > 0) Then
                Return PageURL + ToQueryString()
            End If
            Return ToQueryString()
        End Function

        Public Function ReadQueryString() As Hashtable
            Dim oArray As Array

            oArray = SplitQueryString(DecryptQS(System.Web.HttpContext.Current.Request.QueryString(SessionID.Substring(0, 8))))

            If IsNothing(oArray) Then
                Return Nothing
            End If

            For i As Int16 = 0 To oArray.Length - 1
                Dim oArrayValue As Array

                oArrayValue = SplitQueryStringValue(Convert.ToString(oArray(i)))
                If oArrayValue.Length = 2 Then
                    Me.AddQS(CType(oArrayValue(0), String), CType(oArrayValue(1), String).Replace(REPLACE_VALUE_SEPARATOR, VALUE_SEPARATOR).Replace(REPLACE_QUERY_SEPARATOR, QUERY_SEPARATOR))
                Else
                    Throw New Exception("Query String isn't valid.")
                End If
            Next

            Return m_QSHash
        End Function

        Public Function ReadQueryString(ByVal SourceObject As Object) As Object
            ReadQueryString()
            m_SourceObjectType = SourceObject.GetType

            For Each oProperty As PropertyInfo In SourcePropertyInfos
                If m_QSHash.ContainsKey(oProperty.Name.ToString().Trim()) Then
                    oProperty.SetValue(SourceObject, Convert.ChangeType(m_QSHash(oProperty.Name.ToString().Trim()), oProperty.GetValue(SourceObject, Nothing).GetType()), Nothing)
                End If
            Next

            Return SourceObject
        End Function

        Public Sub AddQSFormMode(ByVal mode As FormBaseEnum.FormMode)
            If m_QSHash.ContainsKey(FORM_MODE) Then
                Throw New Exception("Key already exists.")
            End If
            m_QSHash.Add(FORM_MODE, mode.ToString)
        End Sub

        Public Function GetQSFormModeItem() As String
            If IsNothing(m_QSHash) Or m_QSHash.Count = 0 Then
                Return Nothing
            End If

            If m_QSHash.ContainsKey(FORM_MODE) Then
                Return CType(m_QSHash.Item(FORM_MODE), String)
            End If
            Return Nothing
        End Function

#End Region

    End Class

End Namespace