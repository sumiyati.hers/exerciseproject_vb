﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'															   
'	             Copyright © 2022 IMFI							   
'															   
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'															   
'   Author      : IMFI IT Development Team				   
'   Purpose     : MST_SUPPLIER Business Facade.			   
'   Called By   :											   
'															   
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/04/2022 - 15:42:46 PM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System
Imports System.Collections

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.Mapper
Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.Framework.Persistance.BusinessFacade
Imports IMFI.Template.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.BusinessFacade

    Public Class MST_SUPPLIERBusinessFacade
#Region "Private Variables"

        Private MST_SUPPLIERMapper As IMapper2
        Private _Transaction As IDbTransaction

#End Region

#Region "Constructors/Destructors/Finalizers"

        Public Sub New()
            MST_SUPPLIERMapper = MapperFactory2.GetInstance().GetMapper(GetType(MST_SUPPLIER).ToString())
        End Sub
#End Region

#Region "Public Methods"

        Public Function Create(ByVal oObjectTransporter As IObjectTransporter) As Object
            Return MST_SUPPLIERMapper.Insert(oObjectTransporter)
        End Function

        Public Function Retrieve(ByVal oObjectTransporter As IObjectTransporter) As MST_SUPPLIER
            Return CType(MST_SUPPLIERMapper.Retrieve(oObjectTransporter), MST_SUPPLIER)
        End Function

        Public Function RetrieveListPaging(ByVal PageSize As Int32, ByVal CurrentPage As Int32, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String, ByRef RowCount As Int32) As ArrayList
            Return MST_SUPPLIERMapper.RetrieveListPaging(PageSize, CurrentPage, OrderBy, oObjectTransporter, SearchCondition, RowCount)
        End Function

        Public Function RetrieveWithCondition(ByVal SearchCondition As String, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter) As ArrayList
            Return MST_SUPPLIERMapper.RetrieveWithCondition(SearchCondition, OrderBy, oObjectTransporter)
        End Function

        Public Function RetrieveList(ByVal oObjectTransporter As IObjectTransporter) As ArrayList
            Return MST_SUPPLIERMapper.RetrieveList(oObjectTransporter)
        End Function

        Public Function Update(ByVal oObjectTransporter As IObjectTransporter) As Int32
            Return MST_SUPPLIERMapper.Update(oObjectTransporter)
        End Function

        'public Function Delete(ByVal oObjectTransporter as IObjectTransporter) as Int32
        '	return MST_SUPPLIERMapper.Delete(oObjectTransporter)
        'End Function

        Public Function Delete(ByVal oObjectTransporter As IObjectTransporter) As Result
            Dim oResult As Result

            Try
                oResult = New Result(MST_SUPPLIERMapper.Delete(oObjectTransporter), "")
            Catch ex As Exception
                If ex.GetType().Name = "ConstraintException" Then
                    oResult = New Result(-1, "Data sudah digunakan.")
                Else
                    Throw ex
                End If
            End Try

            Return oResult
        End Function

        Public Sub UseTransaction(ByVal oTransaction As IDbTransaction)
            MST_SUPPLIERMapper.UseTransaction(oTransaction)
            _Transaction = oTransaction
        End Sub

#End Region

    End Class

End Namespace
