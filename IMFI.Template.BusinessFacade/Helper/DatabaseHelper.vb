﻿Public Class DatabaseHelper

    Public Function CurrentConnectionString() As String
        Return New IMFI.Template.DataMapper.DatabaseMapper().CurrentConectionStringDB
    End Function

    Public Function CurrentDB() As String
        Return CurrentConnectionString.Split(";"c)(0).Replace("database=", "")
    End Function

End Class
