﻿Imports IMFI.Framework.Configuration

Namespace IMFI.Template.BusinessFacade

    Public Class FrameworkConfigurationWrapper

        Public Shared Sub Configure()
            Dim oDomainDataMapConfiguration As DomainDataMapConfiguration = New DomainDataMapConfiguration

            oDomainDataMapConfiguration.InitializeConfiguration(System.Reflection.Assembly.GetExecutingAssembly(), "DomainDataMap.xml")
            IMFI.SC.BusinessFacade.FrameworkConfigurationWrapper.Configure()
        End Sub

    End Class

End Namespace
