﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'*															*
'*	             Copyright © 2015 IMFI                      *
'*															*
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*															*
'*   Author      : IMFI Development Team		            *
'*   Purpose     : Security Data Helper                     *
'*   Called By   :                                          *
'*															*
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects
Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.SC.BusinessFacade

#End Region

#End Region

Namespace IMFI.Template.BusinessFacade

    Public Class BranchDataHelper
        Public Function NewObjectBranch() As IBranchInfo
            Return New SC.DomainObjects.DS_COMPANY_REP
        End Function

        Public Function RetrievePagingList(ByVal PageSize As Int32, ByVal CurrentPage As Int32, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String, ByRef RowCount As Int32) As ArrayList
            Return (New IMFI.SC.BusinessFacade.DS_COMPANY_REPBusinessFacade).RetrieveListPaging(PageSize, CurrentPage, OrderBy, oObjectTransporter, SearchCondition, RowCount)
        End Function

        Public Function Retrieve(ByVal oObjectTransporter As IObjectTransporter) As IBranchInfo
            Return (New IMFI.SC.BusinessFacade.DS_COMPANY_REPBusinessFacade).Retrieve(oObjectTransporter)
        End Function

        Public Function RetrieveByBranchRep(ByVal oObjectTransporter As IObjectTransporter) As IBranchInfo
            Return (New IMFI.SC.BusinessFacade.SC_COMPANYBusinessFacade).GetBranchPusatLoginInfo(oObjectTransporter)
        End Function

        Public Function RetrieveByBranchRep(ByVal oBranchCodeRep As Decimal) As IBranchInfo
            Dim oBranchInfo As IBranchInfo = NewObjectBranch()
            oBranchInfo.BRANCH_CODE = oBranchCodeRep

            Return (New IMFI.SC.BusinessFacade.SC_COMPANYBusinessFacade).GetBranchPusatLoginInfo(New IMFI.Template.DomainObjects.ObjectTransporter(oBranchInfo, (New IMFI.Template.Common.UserIdentification).GetUserCredential))
        End Function
    End Class

End Namespace