﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'*															*
'*	             Copyright © 2015 IMFI                      *
'*															*
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*															*
'*   Author      : IMFI Development Team		            *
'*   Purpose     : Security Wrapper                         *
'*   Called By   :                                          *
'*															*
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects
Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.SC.BusinessFacade

#End Region

#End Region

Namespace IMFI.Template.BusinessFacade

    Public Class SecurityWrapper

        Public Function AuthenticateLogin(ByVal userId As String, ByVal password As String) As IUserCredential
            Return (New IMFI.SC.BusinessFacade.SecurityHelper).AuthenticateLogin(userId, password)
        End Function

        Public Function AuthenticateLogin(ByVal userId As String, ByVal password As String, ByVal branchCode As String, ByVal prefixProgId As String, ByVal prefixGroupId As String) As IUserCredential
            Return (New IMFI.SC.BusinessFacade.SecurityHelper).AuthenticateLogin(userId, password, branchCode, prefixProgId, prefixGroupId, True)
        End Function

        Public Function DecodePassword(ByVal password As String) As String
            Return (New IMFI.SC.BusinessFacade.SecurityHelper).DecodePw(password)
        End Function

        Public Function EncodePassword(ByVal password As String) As String
            Return (New IMFI.SC.BusinessFacade.SecurityHelper).EncodePw(password)
        End Function

        Public Function ChangePassword(ByVal userCredential As IUserCredential, ByVal newPassword As String) As Object
            Return (New IMFI.SC.BusinessFacade.SecurityHelper).ChangePassword(userCredential, newPassword)
        End Function

        Public Function GetAllAuthoriedUser(ByVal PREFIX_PROG_ID As String, ByVal userCredential As IUserCredential) As Object
            Return (New IMFI.SC.BusinessFacade.SecurityHelper).GetAuthorizedUser(PREFIX_PROG_ID, userCredential)
        End Function
    End Class

End Namespace




