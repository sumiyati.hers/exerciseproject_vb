﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'*															*
'*	             Copyright © 2015 IMFI                      *
'*															*
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*															*
'*   Author      : IMFI Development Team		            *
'*   Purpose     : Security Data Helper                     *
'*   Called By   :                                          *
'*															*
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects
Imports IMFI.Framework.Persistance.DomainObjects.Core
Imports IMFI.SC.BusinessFacade

#End Region

#End Region

Namespace IMFI.Template.BusinessFacade

    Public Class SecurityDataHelper
        Public Function RetrieveBranchPagingList(ByVal PageSize As Int32, ByVal CurrentPage As Int32, ByVal OrderBy As String, ByVal oObjectTransporter As IObjectTransporter, ByVal SearchCondition As String, ByRef RowCount As Int32) As ArrayList
            Return (New IMFI.SC.BusinessFacade.SC_COMPANYBusinessFacade).RetrieveListPaging(PageSize, CurrentPage, OrderBy, oObjectTransporter, SearchCondition, RowCount)
        End Function

        Public Function RetrieveBranch(ByVal oObjectTransporter As IObjectTransporter) As IBranchInfo
            Return (New IMFI.SC.BusinessFacade.SC_COMPANYBusinessFacade).Retrieve(oObjectTransporter)
        End Function
    End Class

End Namespace
