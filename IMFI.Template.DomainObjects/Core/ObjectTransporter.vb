﻿#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects.Core

#End Region

#End Region

Namespace IMFI.Template.DomainObjects

    <Serializable()> _
    Public Class ObjectTransporter
        Implements IObjectTransporter

#Region "Constructor"

        Public Sub New()
        End Sub

        Public Sub New(ByVal DomainObject As Object)
            _DomainObject = DomainObject
        End Sub

        Public Sub New(ByVal DomainObject As Object, ByVal UserCredential As IUserCredential)
            _DomainObject = DomainObject
            _UserCredential = UserCredential
        End Sub

#End Region

#Region "Private Declaration"

        Private _DomainObject As Object
        Private _UserCredential As IUserCredential

#End Region

#Region "IObjectTransporter Members"


        Public Property DomainObject() As Object Implements Framework.Persistance.DomainObjects.Core.IObjectTransporter.DomainObject
            Get
                Return _DomainObject
            End Get
            Set(ByVal Value As Object)
                _DomainObject = Value
            End Set
        End Property

        Public Property UserCredential() As Framework.Persistance.DomainObjects.Core.IUserCredential Implements Framework.Persistance.DomainObjects.Core.IObjectTransporter.UserCredential
            Get
                Return _UserCredential
            End Get
            Set(ByVal Value As Framework.Persistance.DomainObjects.Core.IUserCredential)
                _UserCredential = Value
            End Set
        End Property
#End Region

    End Class

End Namespace