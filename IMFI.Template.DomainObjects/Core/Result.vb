﻿
#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects.Core

#End Region

#End Region

Namespace IMFI.Template.DomainObjects

    <Serializable()> _
    Public Class Result

#Region "Constructor"

        Public Sub New()
        End Sub


        Public Sub New(ByVal Tag As Object, ByVal ErrorMessage As String)
            _Tag = Tag
            _ErrorMessage = ErrorMessage
        End Sub

#End Region

#Region "Private Declaration"

        Private _Tag As Object
        Private _ErrorMessage As String

#End Region

#Region "IObjectTransporter Members"


        Public Property Tag() As Object
            Get
                Return _Tag
            End Get
            Set(ByVal Value As Object)
                _Tag = Value
            End Set
        End Property

        Public Property ErrorMessage() As String

            Get
                Return _ErrorMessage
            End Get
            Set(ByVal Value As String)
                _ErrorMessage = Value
            End Set
        End Property
#End Region

    End Class

End Namespace