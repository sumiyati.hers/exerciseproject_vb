﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'*															*
'*	             Copyright © 2022 IMFI						*
'*															*
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*                                                          *
'*   Author      : IMFI Development Team                    *
'*   Purpose     : ORDER_HDR Domain Object.               *
'*   Called By   :                                          *
'*                                                          *
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/10/2022 - 10:53:34 AM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.DomainObjects
    <Serializable()> _
    Public Class ORDER_HDR
        Inherits AbstractDomainObject

#Region "Constructors/Destructors/Finalizers"
        Public Sub New()
        End Sub

        Public Sub New(ORDER_ID As Integer)
            _ORDER_ID = ORDER_ID
        End Sub
#End Region

#Region "Private Variables"
        Private _ORDER_ID As Integer
        Private _ORDER_DATE As String = String.Empty
        Private _CustId As Integer
        Private _STATUS_ORDER As Integer
        Private _ALASAN_ORDER As String = String.Empty
        Private _WaktuDibuat As DateTime = CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)
        Private _DiubahOleh As String = String.Empty
        Private _WaktuDiubah As DateTime = CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)
        Private _DibuatOleh As String = String.Empty

#End Region

#Region "Public Properties"
        Public Property ORDER_ID As Integer
            Get
                Return _ORDER_ID
            End Get
            Set(value As Integer)
                _ORDER_ID = value
            End Set
        End Property

        Public Property ORDER_DATE As String
            Get
                Return _ORDER_DATE
            End Get
            Set(value As String)
                _ORDER_DATE = value
            End Set
        End Property

        Public Property CustId As Integer
            Get
                Return _CustId
            End Get
            Set(value As Integer)
                _CustId = value
            End Set
        End Property

        Public Property STATUS_ORDER As Integer
            Get
                Return _STATUS_ORDER
            End Get
            Set(value As Integer)
                _STATUS_ORDER = value
            End Set
        End Property

        Public Property ALASAN_ORDER As String
            Get
                Return _ALASAN_ORDER
            End Get
            Set(value As String)
                _ALASAN_ORDER = value
            End Set
        End Property

        Public Property WaktuDibuat As DateTime
            Get
                Return _WaktuDibuat
            End Get
            Set(value As DateTime)
                _WaktuDibuat = value
            End Set
        End Property

        Public Property DiubahOleh As String
            Get
                Return _DiubahOleh
            End Get
            Set(value As String)
                _DiubahOleh = value
            End Set
        End Property

        Public Property WaktuDiubah As DateTime
            Get
                Return _WaktuDiubah
            End Get
            Set(value As DateTime)
                _WaktuDiubah = value
            End Set
        End Property

        Public Property DibuatOleh As String
            Get
                Return _DibuatOleh
            End Get
            Set(value As String)
                _DibuatOleh = value
            End Set
        End Property


#End Region
    End Class
End Namespace
