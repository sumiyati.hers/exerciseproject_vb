﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'*															*
'*	             Copyright © 2022 IMFI						*
'*															*
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*                                                          *
'*   Author      : IMFI Development Team                    *
'*   Purpose     : MST_PRODUCT Domain Object.               *
'*   Called By   :                                          *
'*                                                          *
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/04/2022 - 03:18:54 PM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.DomainObjects
    <Serializable()> _
    Public Class MST_PRODUCT
        Inherits AbstractDomainObject

#Region "Constructors/Destructors/Finalizers"
        Public Sub New()
        End Sub

        Public Sub New(ProductID As String)
            _ProductID = ProductID
        End Sub
#End Region

#Region "Private Variables"
        Private _ProductID As String = String.Empty
        Private _ProductName As String = String.Empty
        Private _DibuatOleh As String = String.Empty
        Private _WaktuDibuat As DateTime = CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)
        Private _DiubahOleh As String = String.Empty
        Private _WaktuDiubah As DateTime = CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)
        Private _HARGA_SATUAN As Decimal
        Private _Supplier_ID As String = String.Empty

#End Region

#Region "Public Properties"
        Public Property ProductID As String
            Get
                Return _ProductID
            End Get
            Set(value As String)
                _ProductID = value
            End Set
        End Property

        Public Property ProductName As String
            Get
                Return _ProductName
            End Get
            Set(value As String)
                _ProductName = value
            End Set
        End Property

        Public Property DibuatOleh As String
            Get
                Return _DibuatOleh
            End Get
            Set(value As String)
                _DibuatOleh = value
            End Set
        End Property

        Public Property WaktuDibuat As DateTime
            Get
                Return _WaktuDibuat
            End Get
            Set(value As DateTime)
                _WaktuDibuat = value
            End Set
        End Property

        Public Property DiubahOleh As String
            Get
                Return _DiubahOleh
            End Get
            Set(value As String)
                _DiubahOleh = value
            End Set
        End Property

        Public Property WaktuDiubah As DateTime
            Get
                Return _WaktuDiubah
            End Get
            Set(value As DateTime)
                _WaktuDiubah = value
            End Set
        End Property

        Public Property HARGA_SATUAN As Decimal
            Get
                Return _HARGA_SATUAN
            End Get
            Set(value As Decimal)
                _HARGA_SATUAN = value
            End Set
        End Property

        Public Property Supplier_ID As String
            Get
                Return _Supplier_ID
            End Get
            Set(value As String)
                _Supplier_ID = value
            End Set
        End Property


#End Region
    End Class
End Namespace
