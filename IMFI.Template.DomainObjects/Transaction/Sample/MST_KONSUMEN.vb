﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'*															*
'*	             Copyright © 2022 IMFI						*
'*															*
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*                                                          *
'*   Author      : IMFI Development Team                    *
'*   Purpose     : MST_KONSUMEN Domain Object.               *
'*   Called By   :                                          *
'*                                                          *
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/04/2022 - 03:13:02 PM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.DomainObjects
    <Serializable()> _
    Public Class MST_KONSUMEN
        Inherits AbstractDomainObject
        Implements IViewDomainObject
#Region "Constructors/Destructors/Finalizers"
        Public Sub New()
        End Sub

        Public Sub New(CustId As Integer)
            _CustId = CustId
        End Sub
#End Region

#Region "Private Variables"
        Private _CustId As Integer
        Private _CustName As String = String.Empty
        Private _DibuatOleh As String = String.Empty
        Private _WaktuDibuat As DateTime = CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)
        Private _DiubahOleh As String = String.Empty
        Private _WaktuDiubah As DateTime = CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)

#End Region

#Region "Public Properties"
        Public Property CustId As Integer
            Get
                Return _CustId
            End Get
            Set(value As Integer)
                _CustId = value
            End Set
        End Property

        Public Property CustName As String
            Get
                Return _CustName
            End Get
            Set(value As String)
                _CustName = value
            End Set
        End Property

        Public Property DibuatOleh As String
            Get
                Return _DibuatOleh
            End Get
            Set(value As String)
                _DibuatOleh = value
            End Set
        End Property

        Public Property WaktuDibuat As DateTime
            Get
                Return _WaktuDibuat
            End Get
            Set(value As DateTime)
                _WaktuDibuat = value
            End Set
        End Property

        Public Property DiubahOleh As String
            Get
                Return _DiubahOleh
            End Get
            Set(value As String)
                _DiubahOleh = value
            End Set
        End Property

        Public Property WaktuDiubah As DateTime
            Get
                Return _WaktuDiubah
            End Get
            Set(value As DateTime)
                _WaktuDiubah = value
            End Set
        End Property


#End Region
    End Class
End Namespace
