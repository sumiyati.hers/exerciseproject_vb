﻿#Region "Header Info"

#Region "Code Disclaimer"

'************************************************************
'*															*
'*	             Copyright © 2022 IMFI						*
'*															*
'************************************************************

#End Region

#Region "Summary"

'************************************************************
'*                                                          *
'*   Author      : IMFI Development Team                    *
'*   Purpose     : ORDER_DTL Domain Object.               *
'*   Called By   :                                          *
'*                                                          *
'************************************************************

#End Region

#Region "History"

'************************************************************
'*                                                          *
'*   Created On  : 10/04/2022 - 03:23:47 PM                 *
'*                                                          *
'*   Modify By   : {Name} - {Date}                          *
'*       - {Description Here}                               *
'*                                                          *
'************************************************************

#End Region

#End Region

#Region "Namespace Imports"

#Region ".NET Base Class Namespace Imports"

Imports System

#End Region

#Region "Custom Namespace Imports"

Imports IMFI.Framework.Persistance.DomainObjects

#End Region

#End Region

Namespace IMFI.Template.DomainObjects
    <Serializable()> _
    Public Class ORDER_DTL
        Inherits AbstractDomainObject

#Region "Constructors/Destructors/Finalizers"
        Public Sub New()

        End Sub
#End Region

#Region "Private Variables"
        Private _ORDER_ID As String = String.Empty
        Private _ProductID As String = String.Empty
        Private _QTY As Integer
        Private _PRICE As Decimal
        Private _WaktuDibuat As DateTime = CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)
        Private _DiubahOleh As String = String.Empty
        Private _WaktuDiubah As DateTime = CType(System.Data.SqlTypes.SqlDateTime.MinValue.Value, DateTime)
        Private _DibuatOleh As String = String.Empty

#End Region

#Region "Public Properties"
        Public Property ORDER_ID As String
            Get
                Return _ORDER_ID
            End Get
            Set(value As String)
                _ORDER_ID = value
            End Set
        End Property

        Public Property ProductID As String
            Get
                Return _ProductID
            End Get
            Set(value As String)
                _ProductID = value
            End Set
        End Property

        Public Property QTY As Integer
            Get
                Return _QTY
            End Get
            Set(value As Integer)
                _QTY = value
            End Set
        End Property

        Public Property PRICE As Decimal
            Get
                Return _PRICE
            End Get
            Set(value As Decimal)
                _PRICE = value
            End Set
        End Property

        Public Property WaktuDibuat As DateTime
            Get
                Return _WaktuDibuat
            End Get
            Set(value As DateTime)
                _WaktuDibuat = value
            End Set
        End Property

        Public Property DiubahOleh As String
            Get
                Return _DiubahOleh
            End Get
            Set(value As String)
                _DiubahOleh = value
            End Set
        End Property

        Public Property WaktuDiubah As DateTime
            Get
                Return _WaktuDiubah
            End Get
            Set(value As DateTime)
                _WaktuDiubah = value
            End Set
        End Property

        Public Property DibuatOleh As String
            Get
                Return _DibuatOleh
            End Get
            Set(value As String)
                _DibuatOleh = value
            End Set
        End Property


#End Region
    End Class
End Namespace
